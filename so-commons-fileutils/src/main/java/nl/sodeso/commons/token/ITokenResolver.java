package nl.sodeso.commons.token;

import java.io.IOException;

/**
 * @author Ronald Mathies
 */
public interface ITokenResolver {

    String resolveToken(String tokenName) throws IOException;

}
