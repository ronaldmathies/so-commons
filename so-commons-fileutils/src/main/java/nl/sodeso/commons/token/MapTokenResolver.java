package nl.sodeso.commons.token;

import java.io.IOException;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class MapTokenResolver implements ITokenResolver {

    private Map<String, String> map = null;

    public MapTokenResolver(Map<String, String> map) {
        this.map = map;
    }

    @Override
    public String resolveToken(String tokenName) throws IOException {
        if (map.containsKey(tokenName)) {
            return map.get(tokenName);
        }

        return null;
    }
}
