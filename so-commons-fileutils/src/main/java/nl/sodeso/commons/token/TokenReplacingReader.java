package nl.sodeso.commons.token;

import java.io.IOException;
import java.io.PushbackReader;
import java.io.Reader;
import java.nio.CharBuffer;

/**
 * A reader which will replace any found token ( ${token} ) with the values as resolved by the
 * class implementing the ITokenResolver interface.
 */
public class TokenReplacingReader extends Reader {

    protected PushbackReader pushbackReader   = null;
    protected ITokenResolver tokenResolver    = null;
    protected StringBuilder  tokenNameBuffer  = new StringBuilder();
    protected String         tokenValue       = null;
    protected int            tokenValueIndex  = 0;

    /**
     * Constructs a new TokenReplacingReader wich encapsulates a reader.
     *
     * @param source the reader to encapsulate
     * @param resolver the token resolver which will be called during the read action when a token is found.
     */
    public TokenReplacingReader(Reader source, ITokenResolver resolver) {
        this.pushbackReader = new PushbackReader(source, 2);
        this.tokenResolver  = resolver;
    }

    /**
     * {@inheritDoc}
     */
    public int read(CharBuffer target) throws IOException {
        throw new RuntimeException("Operation Not Supported");
    }

    /**
     * {@inheritDoc}
     */
    public int read() throws IOException {
        if(this.tokenValue != null){
            if(this.tokenValueIndex < this.tokenValue.length()){
                return this.tokenValue.charAt(this.tokenValueIndex++);
            }
            if(this.tokenValueIndex == this.tokenValue.length()){
                this.tokenValue = null;
                this.tokenValueIndex = 0;
            }
        }

        int data = this.pushbackReader.read();
        if(data != '$') return data;

        data = this.pushbackReader.read();
        if(data != '{'){
            this.pushbackReader.unread(data);
            return '$';
        }
        this.tokenNameBuffer.delete(0, this.tokenNameBuffer.length());

        data = this.pushbackReader.read();
        while(data != '}'){
            this.tokenNameBuffer.append((char) data);
            data = this.pushbackReader.read();
        }

        this.tokenValue = this.tokenResolver
                .resolveToken(this.tokenNameBuffer.toString());

        if(this.tokenValue == null){
            this.tokenValue = "${"+ this.tokenNameBuffer.toString() + "}";
        }
        if(this.tokenValue.length() == 0){
            return read();
        }
        return this.tokenValue.charAt(this.tokenValueIndex++);


    }

    /**
     * {@inheritDoc}
     */
    public int read(char cbuf[]) throws IOException {
        return read(cbuf, 0, cbuf.length);
    }

    /**
     * {@inheritDoc}
     */
    public int read(char cbuf[], int off, int len) throws IOException {
        int charsRead = 0;
        for(int i=0; i<len; i++){
            int nextChar = read();
            if(nextChar == -1) {
                if(charsRead == 0){
                    charsRead = -1;
                }
                break;
            }
            charsRead = i + 1;
            cbuf[off + i] = (char) nextChar;
        }
        return charsRead;
    }

    /**
     * {@inheritDoc}
     */
    public void close() throws IOException {
        this.pushbackReader.close();
    }

    /**
     * {@inheritDoc}
     */
    public long skip(long n) throws IOException {
        throw new RuntimeException("Operation Not Supported");
    }

    /**
     * {@inheritDoc}
     */
    public boolean ready() throws IOException {
        return this.pushbackReader.ready();
    }

    /**
     * {@inheritDoc}
     */
    public boolean markSupported() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public void mark(int readAheadLimit) throws IOException {
        throw new RuntimeException("Operation Not Supported");
    }

    /**
     * {@inheritDoc}
     */
    public void reset() throws IOException {
        throw new RuntimeException("Operation Not Supported");
    }
}