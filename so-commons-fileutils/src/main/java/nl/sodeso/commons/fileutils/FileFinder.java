package nl.sodeso.commons.fileutils;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Recursively searches the user.dir path for the given filename and calls
 * the FileHandler when the file is found.
 *
 * @author Ronald Mathies
 */
public final class FileFinder {

    private enum Type {
        FILE,
        FOLDER
    }

    public static final String PROP_USER_DIR = "user.dir";

    private FileFinder() {}

    public static void findFilesMatchingExpression(final String expression, final FileHandler handler) {
        findFilesMatchingExpression(System.getProperty(PROP_USER_DIR), expression, handler);
    }

    public static void findFilesMatchingExpression(String directory, final String expression, final FileHandler handler) {
        final Path startPath = Paths.get(new File(directory).toURI());

        try {
            Files.walkFileTree(startPath, new SimpleFileVisitor<Path>() {

                @Override
                public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
                    File file = path.toFile();

                    if (file.getName().matches(expression)) {
                        handler.handle(file);
                    }

                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            throw new IllegalArgumentException("FileFinder: error finding files matching '" + expression + "'.");
        }
    }

    /**
     * Recursively searches the given directory for the given filename and calls
     * the FileHandler when the file is found.
     *
     * @param path the starting path.
     * @param filename the filename to search for.
     * @param handler the handler which will be called when the file is found.
     *
     * @throws IllegalArgumentException when the file cannot be located.
     */
    public static void findFileInPath(final Path path, final String filename, final FileHandler handler) {
        findObjectInPath(Type.FILE, path, filename, handler);
    }

    /**
     * Recursively searches the given path for the given folder name and calls
     * the FileHandler when the folder is found.
     *
     * @param path the starting path.
     * @param folder the folder to search for.
     * @param handler the handler which will be called when the file is found.
     *
     * @throws IllegalArgumentException when the file cannot be located.
     */
    public static void findFolderInPath(final Path path, final String folder, final FileHandler handler) {
        findObjectInPath(Type.FOLDER, path, folder, handler);
    }

    /**
     * Recursively searches the user.dir path for the given filename and calls
     * the FileHandler when the file is found.
     *
     * @param filename the filename to search for.
     * @param handler the handler which will be called when the file is found.
     *
     * @throws IllegalArgumentException when the file cannot be located.
     */
    public static void findFileInUserPath(final String filename, final FileHandler handler)  {
        findObjectInPath(Type.FILE, getPathToFolder(System.getProperty(PROP_USER_DIR)), filename, handler);
    }

    /**
     * Recursively searches the user.dir path for the given directory name and calls
     * the FileHandler when the file is found.
     *
     * @param folder the directory to search for.
     * @param handler the handler which will be called when the file is found.
     *
     * @throws IllegalArgumentException when the file cannot be located.
     */
    public static void findFolderInUserPath(final String folder, final FileHandler handler)  {
        findObjectInPath(Type.FOLDER, getPathToFolder(System.getProperty(PROP_USER_DIR)), folder, handler);
    }

    public static void findObjectInPath(final Type type, Path startPath, final String name, final FileHandler handler) {
        final boolean[] result = {false};
        try {
            Files.walkFileTree(startPath, new SimpleFileVisitor<Path>() {

                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    if (type == Type.FOLDER) {
                        if (attrs.isDirectory() && dir.getFileName().toString().equalsIgnoreCase(name)) {
                            result[0] = true;
                        }

                        if (result[0]) {
                            handler.handle(dir.toFile());
                        }
                    }

                    return result[0] ? FileVisitResult.TERMINATE : FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
                    File file = path.toFile();
                    if (type == Type.FILE) {
                        if (attrs.isRegularFile() && file.getName().equalsIgnoreCase(name)) {
                            result[0] = true;
                        }
                    }

                    if (result[0]) {
                        handler.handle(file);
                    }

                    return result[0] ? FileVisitResult.TERMINATE : FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            throw new IllegalArgumentException("FileFinder: error finding " + (type == Type.FOLDER ? "directory" : "file") + " with name '" + name + "'.");
        }

        if (!result[0]) {
            throw new IllegalArgumentException("FileFinder: error finding " + (type == Type.FOLDER ? "directory" : "file") + " with name '" + name + "'.");
        }
    }

    private static Path getPathToFolder(String directory) {
        return Paths.get(new File(directory).toURI());
    }

}
