package nl.sodeso.commons.fileutils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Ronald Mathies
 */
public class StreamUtil {

    /**
     * Writes the inputstream to the outputstream.
     *
     * @param is the input stream.
     * @param os the output stream.
     *
     * @throws IOException when a problem occured while writing to the output stream or reading from the input stream.
     */
    public static void writeToOutputStream(InputStream is, OutputStream os) throws IOException {
        int length = 0;

        byte[] buffer = new byte[2048];
        while ((length = is.read(buffer)) != -1) {
            os.write(buffer, 0, length);
        }

        os.flush();
    }
}
