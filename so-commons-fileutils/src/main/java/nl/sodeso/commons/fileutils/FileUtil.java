package nl.sodeso.commons.fileutils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Ronald Mathies
 */
public class FileUtil {

    public static final String SYSTEM_PROPERTY_TEMP_FOLDER = "java.io.tmpdir";

    /**
     * Returns the system defined temporarily folder (defined by a stystem property java.io.tmpdir),
     * if the temp folder doesn't end with a File.seperator then it will be added.
     *
     * @return the system temporarily folder.
     */
    public static String getSystemTempFolder() {
        String property = System.getProperty(SYSTEM_PROPERTY_TEMP_FOLDER);
        return property.endsWith("/") ? property : property + File.separator;
    }

    /**
     * Returns the system defined temporarily folder encapsulated in a <code>File</code> object.
     *
     * @return the system temporarily folder.
     */
    public static File getSystemTempFolderAsFile() {
        return new File(getSystemTempFolder());
    }

    /**
     * Removes all illegal characters from the filename.
     *
     * @param filename the filename to clean-up.
     * @return the cleaned-up filename.
     */
    public static String removeIllegalChars(String filename) {
        return filename.replaceAll("[^a-zA-Z0-9.-]", "_");
    }

}
