package nl.sodeso.commons.token;

import org.junit.Test;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public class TokenReplacingReaderTest {

    @Test
    public void test() throws IOException {
        String sourceString = "this is a ${token1} and ${token2}";
        Reader reader = new StringReader(sourceString);

        TokenReplacingReader tokenReplacingReader = new TokenReplacingReader(reader, new TokenResolverMock());

        assertEquals('t', (char) tokenReplacingReader.read());
        assertEquals('h', (char) tokenReplacingReader.read());
        assertEquals('i', (char) tokenReplacingReader.read());
        assertEquals('s', (char) tokenReplacingReader.read());
        assertEquals(' ', (char) tokenReplacingReader.read());
        assertEquals('i', (char) tokenReplacingReader.read());
        assertEquals('s', (char) tokenReplacingReader.read());
        assertEquals(' ', (char) tokenReplacingReader.read());
        assertEquals('a', (char) tokenReplacingReader.read());
        assertEquals(' ', (char) tokenReplacingReader.read());
        assertEquals('1', (char) tokenReplacingReader.read());
        assertEquals('2', (char) tokenReplacingReader.read());
        assertEquals('3', (char) tokenReplacingReader.read());
        assertEquals(' ', (char) tokenReplacingReader.read());
        assertEquals('a', (char) tokenReplacingReader.read());
        assertEquals('n', (char) tokenReplacingReader.read());
        assertEquals('d', (char) tokenReplacingReader.read());
        assertEquals(' ', (char) tokenReplacingReader.read());
        assertEquals(-1, tokenReplacingReader.read());
    }

}