package nl.sodeso.commons.token;

import java.io.IOException;

/**
 * @author Ronald Mathies
 */
public class TokenResolverMock implements ITokenResolver {

    @Override
    public String resolveToken(String tokenName) throws IOException {
        if("token1".equals(tokenName)) {
            return "123";
        }
        if("token2".equals(tokenName)) {
            return "";
        }

        return "";
    }
}