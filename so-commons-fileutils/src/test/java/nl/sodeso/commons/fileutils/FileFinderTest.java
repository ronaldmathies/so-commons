package nl.sodeso.commons.fileutils;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertTrue;

/**
 * @author Ronald Mathies
 */
public class FileFinderTest {

    @Test
    public void testFindFileInUserDirectory() throws Exception {
        FileFinder.findFileInUserPath("FileAA.txt", new FileHandler() {
            @Override
            public void handle(File file) {
                assertTrue(file.exists() && file.isFile() &&  file.getName().equals("FileAA.txt"));
            }
        });

        FileFinder.findFileInUserPath("FileBA.txt", new FileHandler() {
            @Override
            public void handle(File file) {
                assertTrue(file.exists() && file.isFile() && file.getName().equals("FileBA.txt"));
            }
        });
    }

    @Test
    public void testFindDirectoryInUserDirectory() throws Exception {
        FileFinder.findFolderInUserPath("FolderBA", new FileHandler() {
            @Override
            public void handle(File file) {
                assertTrue(file.exists() && file.isDirectory() && file.getName().equals("FolderBA"));
            }
        });

        FileFinder.findFolderInUserPath("FolderAA", new FileHandler() {
            @Override
            public void handle(File file) {
                assertTrue(file.exists() && file.isDirectory() && file.getName().equals("FolderAA"));
            }
        });
    }
}