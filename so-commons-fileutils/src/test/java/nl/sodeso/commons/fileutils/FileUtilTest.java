package nl.sodeso.commons.fileutils;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public class FileUtilTest {

    @Before
    public void before() {
        System.setProperty(FileUtil.SYSTEM_PROPERTY_TEMP_FOLDER, "/usr/local/tmp");
    }

    @Test
    public void testGetSystemTempFolder() throws Exception {
        assertEquals("Temp folder not as expected.", "/usr/local/tmp/", FileUtil.getSystemTempFolder());
    }

    @Test
    public void testGetSystemTempFolderAsFile() throws Exception {
        assertEquals("Temp folder not as expected.", "/usr/local/tmp", FileUtil.getSystemTempFolderAsFile().getAbsolutePath());
    }

    @Test
    public void testRemoveIllegalChars() throws Exception {
        assertEquals("Illegal characters not removed properly.", "abcd-_-_-1-_-2-_.tst", FileUtil.removeIllegalChars("abcd-è-#-1-)-2-}.tst"));
    }
}