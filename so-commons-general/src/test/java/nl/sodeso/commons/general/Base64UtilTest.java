package nl.sodeso.commons.general;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;

import static org.junit.Assert.*;

/**
 * @author Ronald Mathies
 */
public class Base64UtilTest {

    @Test
    public void testEncode() throws Exception {
        String dataToEncode = "data encode encode";
        StringWriter encoded = Base64Util.encode(new ByteArrayInputStream(dataToEncode.getBytes()));
        assertEquals("Encoded result is not as excepcted.", "ZGF0YSBlbmNvZGUgZW5jb2Rl", encoded.toString());
    }

//    @Test
//    public void testDecode() throws Exception {
//        String dataToDecode = "ZGF0YSBlbmNvZGUgZW5jb2Rl";
//        StringWriter decoded = Base64Util.decode(new ByteArrayInputStream(dataToDecode.getBytes()));
//        assertEquals("Encoded result is not as excepcted.", "data encode encode", decoded.toString());
//    }
}