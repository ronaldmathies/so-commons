package nl.sodeso.commons.general;

import java.io.*;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class CloneUtil {

    private static final Logger log = Logger.getLogger(CloneUtil.class.getName());

    @SuppressWarnings("unchecked")
    public static <T extends Serializable> T clone(T t)  {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            try (ObjectOutputStream oos = new ObjectOutputStream(bos)) {
                oos.writeObject(t);
                oos.flush();

                try (ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bos.toByteArray()))) {
                    return (T) ois.readObject();
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            log.severe(String.format("Failed to clone object of type %s, %s", t.getClass().getName(), e.getMessage()));
        }

        return null;
    }

}
