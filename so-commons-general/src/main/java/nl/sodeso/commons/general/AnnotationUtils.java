package nl.sodeso.commons.general;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.util.Iterator;
import java.util.Set;

/**
 * @author Ronald Mathies
 */
public class AnnotationUtils {

    @SuppressWarnings("unchecked")
    public static <T> Set<Class<? extends T>> findSubTypesWithAnnotation(Class<T> superType, Class annotation) {
        Reflections reflections =
                new Reflections(
                        new ConfigurationBuilder()
                            .setScanners(new TypeAnnotationsScanner(), new SubTypesScanner())
                            .setUrls(ClasspathHelper.forPackage("nl.sodeso.webservice.mock.service")));

        Set<Class<? extends T>> subTypesOf = reflections.getSubTypesOf(superType);
        Iterator<Class<? extends T>> subTypesIter = subTypesOf.iterator();
        while (subTypesIter.hasNext()) {
            Class<? extends T> filter = subTypesIter.next();
            if (!filter.isAnnotationPresent(annotation)) {
                subTypesIter.remove();
            }
        }

        return subTypesOf;
    }

}
