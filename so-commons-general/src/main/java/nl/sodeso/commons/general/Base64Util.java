package nl.sodeso.commons.general;

import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.charset.Charset;

/**
 * @author Ronald Mathies
 */
public class Base64Util {

    private static final int BUFFER_SIZE = 32768;

    public static StringWriter encode(InputStream inputStream) {
        StringWriter writer = new StringWriter(BUFFER_SIZE);
        Base64InputStream base64InputStream = new Base64InputStream(inputStream, true, 0, null);
        try {
            IOUtils.copy(base64InputStream, writer, Charset.forName("UTF-8"));
        } catch (IOException ignored) {

        }

        return writer;
    }

    public static ByteArrayOutputStream decode(InputStream inputStream) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(BUFFER_SIZE);
        Base64InputStream base64InputStream = new Base64InputStream(inputStream, false, 0, null);
        try {
            IOUtils.copy(base64InputStream, baos);
        } catch (IOException ignored) {

        }

        return baos;
    }

}
