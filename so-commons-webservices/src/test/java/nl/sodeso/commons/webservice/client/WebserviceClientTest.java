package nl.sodeso.commons.webservice.client;

import com.sun.net.httpserver.HttpServer;
import nl.sodeso.commons.webservice.util.SoapUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;

public class WebserviceClientTest {

    private static final int HTTP_SERVER_PORT = 6666;
    private static final int CONNECTION_TIMEOUT = 10000;
    private static final String CONTEXT_ROOT = "/context/webservice";

    private static final String SOAP_REQUEST =
            "<?xml version=\"1.0\"?>\n" +
            "   <soap:Envelope xmlns:soap=\"http://www.w3.org/2001/12/soap-envelope\" soap:encodingStyle=\"http://www.w3.org/2001/12/soap-encoding\">\n" +
            "       <soap:Body xmlns:s=\"http://www.sodeso.nl/stock\">\n" +
            "           <s:GetStockPriceRequest>\n" +
            "               <s:StockName>IBM</s:StockName>\n" +
            "           </s:GetStockPriceRequest>\n" +
            "       </soap:Body>\n" +
            "</soap:Envelope>";

    private static final String SOAP_RESPONSE =
            "<?xml version=\"1.0\"?>\n" +
            "   <soap:Envelope xmlns:soap=\"http://www.w3.org/2001/12/soap-envelope\" soap:encodingStyle=\"http://www.w3.org/2001/12/soap-encoding\">\n" +
            "       <soap:Body xmlns:s=\"http://www.sodeso.nl/stock\">\n" +
            "           <s:GetStockPriceResponse>\n" +
            "               <s:Price>IBM</s:Price>\n" +
            "           </s:GetStockPriceResponse>\n" +
            "       </soap:Body>\n" +
            "</soap:Envelope>";

    private HttpServer server = null;

    @Before
    public void setUp() throws IOException {
        server = HttpServer.create(new InetSocketAddress(HTTP_SERVER_PORT), 0);
        server.createContext(CONTEXT_ROOT, exchange -> {
            OutputStream outputStream = exchange.getResponseBody();
            exchange.sendResponseHeaders(200, SOAP_RESPONSE.length());

            outputStream.write(SOAP_RESPONSE.getBytes());
            outputStream.flush();
        });
        server.setExecutor(Executors.newFixedThreadPool(1));
        server.start();
    }

    @After
    public void tearDown() {
        if (server != null) {
            server.stop(1);
        }
    }

    @Test
    public void checkPerformSoapRequest() {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(SOAP_REQUEST.getBytes());

        String localname = WebserviceClient.performSoapRequest("http://127.0.0.1:" + HTTP_SERVER_PORT + CONTEXT_ROOT, inputStream, CONNECTION_TIMEOUT, new WebserviceResultHandler<String>() {
            @Override
            public String onSuccess(InputStream inputStream) {
                Node node = SoapUtil.getSoapBody(inputStream);
                if (node == null) {
                    return "NODE NOT FOUND";
                }

                return node.getLocalName();
            }

            @Override
            public void onFailure(int statusCode, InputStream inputStream) {

            }
        });

        assertEquals("Localname should be the same.", "GetStockPriceResponse", localname);
    }


}