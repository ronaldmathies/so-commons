package nl.sodeso.commons.webservice.client;

/**
 * This exception is thrown when a call to the SOAP endpoint failed or when the process of resolving
 * the SOAP body failed.
 *
 * @author Ronald Mathies
 */
public class WebserviceClientException extends RuntimeException {

    public WebserviceClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
