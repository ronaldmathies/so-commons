package nl.sodeso.commons.webservice.util;

import nl.sodeso.commons.webservice.client.WebserviceClientException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Ronald Mathies
 */
public class SoapUtil {

    private static final String BODY_LOCAL_NAME = "Body";

    private SoapUtil() {}

    /**
     * Strips the complete soap response down to the body and removes
     * and empty child nodes within the body until the first real
     * node.
     *
     * Details about the empty nodes:
     *
     * http://www.w3.org/DOM/faq.html#emptytext
     */
    public static Node getSoapBody(InputStream inputStream) {

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);

            // Parse the input stream.
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            Document document = documentBuilder.parse(inputStream, "UTF-8");

            // Loop through all the nodes until we encounter the body node.
            NodeList childNodes = document.getDocumentElement().getChildNodes();
            for(int i = 0; i < childNodes.getLength(); i++) {
                Node node  = childNodes.item(i);
                if (BODY_LOCAL_NAME.equalsIgnoreCase(node.getLocalName())) {

                    // When the body node is found, loop through it's children
                    // until a node with a real local name is encountered (white space stripping)
                    NodeList bodyChildNodes = node.getChildNodes();
                    for (int index = 0; index < bodyChildNodes.getLength(); index++) {
                        if (bodyChildNodes.item(index).getLocalName() != null) {
                            return bodyChildNodes.item(index);
                        }
                    }
                }
            }
            
            return null;
        } catch (ParserConfigurationException e) {
            throw new WebserviceClientException("Error creating a new document builder.", e);
        } catch (IOException | SAXException e) {
            throw new WebserviceClientException("Error during document parsing of the soap message.", e);
        }
    }

}
