package nl.sodeso.commons.webservice.client;

import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author Ronald Mathies
 */
public final class WebserviceClient {

    private static final Logger LOG = Logger.getLogger(WebserviceClient.class.getName());

    /**
     * Constructor.
     */
    private WebserviceClient() {
    }

    /**
     * Performs a SOAP request.
     *
     * @param url the SOAP endpoint to call.
     * @param inputStream the stream containing the SOAP call.
     * @param connectionTimeout timeout after which the client will break off the connection.
     * @param handler the handler to handle the result stream.
     * @param <T> the result of the SOAP call.
     * @return the result of the SOAP call.
     */
    public static <T> T performSoapRequest(String url, InputStream inputStream, int connectionTimeout, WebserviceResultHandler<T> handler) {
        // Convert the stream to an entity stream.
        InputStreamEntity inputStreamEntity = new InputStreamEntity(inputStream);
        inputStreamEntity.setContentType("text/xml; charset=UTF-8");
        inputStreamEntity.setChunked(true);
        inputStreamEntity.setContentEncoding("UTF-8");

        // Create request configuration
        RequestConfig.Builder requestConfigBuilder = RequestConfig.custom();
        requestConfigBuilder.setConnectTimeout(connectionTimeout);
        requestConfigBuilder.setSocketTimeout(connectionTimeout);

        // Create the HTTP Client that will perform the webservice call.
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        httpClientBuilder.setDefaultRequestConfig(requestConfigBuilder.build());
        CloseableHttpClient closeableHttpClient = httpClientBuilder.build();

        try {
            LOG.info("Performing webservice call to: " + url);

            // Create the HTTP post method.
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(inputStreamEntity);

            // Execute the HTTP post method.
            CloseableHttpResponse closeableHttpResponse = closeableHttpClient.execute(httpPost);

            LOG.info("Finished performing webservice call to: " + url);

            // Check the HTTP Response status code.
            int statusCode = closeableHttpResponse.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                LOG.info("Failed performing webservice call to: '" + url + "', status code: " + statusCode);

                handler.onFailure(statusCode, inputStream);
            } else {
                LOG.info("Succesfully performing webservice call to: '" + url + "', status code: " + statusCode);
            }

            return handler.onSuccess(closeableHttpResponse.getEntity().getContent());
        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Error performing webservice call to: " + url, e);
            throw new WebserviceClientException("Error performing webservice call to: " + url, e);
        } finally {
            try {
                closeableHttpClient.close();
            } catch (Exception e) {
                LOG.info("Error closing the HTTP client.");
            }
        }
    }

}
