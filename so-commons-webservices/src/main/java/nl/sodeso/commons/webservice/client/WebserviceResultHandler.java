package nl.sodeso.commons.webservice.client;

import java.io.InputStream;

/**
 * @author Ronald Mathies
 *
 * @param <T>
 */
public interface WebserviceResultHandler<T> {

    /**
     * Called by the webservice client when the call to the SOAP endpoint was completed successfully without any
     * errors.
     *
     * @param inputStream the input stream containing the result from the webservice call.
     * @return the processed result.
     */
    T onSuccess(InputStream inputStream);

    /**
     * Called by the webservice client when the call to the SOAP endpoint failed.
     *
     * @param statusCode the HTTP status code.
     * @param inputStream the input stream containing the result from the webservice call.
     */
    void onFailure(int statusCode, InputStream inputStream);

}
