package nl.sodeso.commons.storage.rules;

import nl.sodeso.commons.storage.FileEntry;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * @author Ronald Mathies
 */
public class NeverExpirationRule implements ExpirationRule {

    public NeverExpirationRule() {
    }

    public boolean isExpired(FileEntry entry) {
        return false;
    }

}
