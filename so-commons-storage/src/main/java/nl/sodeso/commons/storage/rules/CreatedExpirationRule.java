package nl.sodeso.commons.storage.rules;

import nl.sodeso.commons.storage.FileEntry;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * @author Ronald Mathies
 */
public class CreatedExpirationRule implements ExpirationRule {

    private long expiration = 0l;

    public CreatedExpirationRule(long expiration) {
        this.expiration = expiration;
    }

    public boolean isExpired(FileEntry entry) {
        try {
            BasicFileAttributes attributes = Files.readAttributes(entry.getFile().toPath(), BasicFileAttributes.class);
            return System.currentTimeMillis() - attributes.creationTime().toMillis() > expiration;
        } catch (IOException e) {
            return false;
        }
    }

}
