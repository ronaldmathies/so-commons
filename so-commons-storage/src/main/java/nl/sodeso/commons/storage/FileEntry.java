package nl.sodeso.commons.storage;

import nl.sodeso.commons.fileutils.FileUtil;
import nl.sodeso.commons.storage.rules.ExpirationRule;
import nl.sodeso.commons.storage.rules.NeverExpirationRule;

import java.io.File;
import java.util.Objects;

/**
 * @author Ronald Mathies
 */
public class FileEntry {

    private String uuid;
    private File file;
    private ExpirationRule rule;

    public FileEntry(String uuid) {
        this(uuid, new NeverExpirationRule());
    }

    public FileEntry(String uuid, ExpirationRule rule) {
        this(uuid, new File(FileUtil.getSystemTempFolderAsFile(), uuid.concat(".upl")), rule);
    }

    public FileEntry(String uuid, File file) {
        this(uuid, file, new NeverExpirationRule());
    }

    public FileEntry(String uuid, File file, ExpirationRule rule) {
        this.uuid = uuid;
        this.file = file;
        this.rule = rule;
    }

    public String getUuid() {
        return uuid;
    }

    public File getFile() {
        return file;
    }

    public boolean isExpired() {
        return rule.isExpired(this);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        return Objects.equals(uuid, ((FileEntry)other).uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
