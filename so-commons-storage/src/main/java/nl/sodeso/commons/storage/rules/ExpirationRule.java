package nl.sodeso.commons.storage.rules;

import nl.sodeso.commons.storage.FileEntry;

/**
 * @author Ronald Mathies
 */
public interface ExpirationRule {

    long THIRTY_MINUTES = 1000 * 60 * 30;
    long HOUR_MINUTES = 1000 * 60 * 30;

    boolean isExpired(FileEntry entry);

}
