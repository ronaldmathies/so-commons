package nl.sodeso.commons.storage;

import nl.sodeso.commons.quartz.annotation.QuartzJob;
import org.quartz.*;

/**
 * @author Ronald Mathies
 */
@QuartzJob(
    name="CleanupExpiredFilesJob",
    cron = "0 0/1 * 1/1 * ? *"
)
public class CleanupExpiredFilesJob implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        StorageController.getInstance().deleteExpired();
    }
}
