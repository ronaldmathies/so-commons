package nl.sodeso.commons.storage;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class StorageController {

    private static final Logger log = Logger.getLogger(StorageController.class.getName());

    private List<FileEntry> entries = new ArrayList<>();

    private static final Object lock = new Object();
    private static StorageController INSTANCE = null;

    public static StorageController getInstance() {
        if (INSTANCE == null) {
            synchronized (lock) {
                INSTANCE = new StorageController();
            }
        }

        return INSTANCE;
    }

    public void add(FileEntry entry) {
        this.entries.add(entry);
    }

    public void remove(String uuid) {
        entries.removeIf(entry -> {
            if (entry.getUuid().equals(uuid)) {
                deleteFile(entry);
                return true;
            }

            return false;
        });
    }

    public Optional<FileEntry> get(String uuid) {
        return entries.stream().filter(fileEntry -> fileEntry.getUuid().equals(uuid)).findFirst();
    }

    public void destroy() {
        entries.forEach(entry -> deleteFile(entry));
    }

    public void deleteExpired() {
        entries.removeIf(entry -> {
            if (entry.isExpired()) {
                deleteFile(entry);

                return true;
            }

            return false;
        });
    }

    private void deleteFile(FileEntry entry) {
        if (entry.getFile().exists()) {
            log.info(String.format("Auto removed file '%s'.", entry.getFile().getName()));
            if (!entry.getFile().delete()) {
                log.warning(String.format("Could not delete file '%s'.", entry.getFile().getName()));
            }
        }
    }
}
