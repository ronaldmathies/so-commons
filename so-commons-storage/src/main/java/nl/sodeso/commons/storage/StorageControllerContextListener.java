package nl.sodeso.commons.storage;

import nl.sodeso.commons.web.initialization.AbstractWebInitContext;
import nl.sodeso.commons.web.initialization.annotation.WebInitContext;

import javax.servlet.ServletContextEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Abstract implementation of a servlet context initialization for initializing a quartz
 * engine.
 *
 * @author Ronald Mathies
 */
@WebInitContext(priority = 0)
public class StorageControllerContextListener extends AbstractWebInitContext {

    private static final Logger LOG = Logger.getLogger(StorageControllerContextListener.class.getName());

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(ServletContextEvent event) {
        LOG.log(Level.INFO, "Initializing Storage Controller Context Listener.");
        LOG.log(Level.INFO, "Storage Controller Context Listener initialized.");

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy(ServletContextEvent event) {
        LOG.log(Level.INFO, "Destroying Storage Controller Context Listener.");

        StorageController.getInstance().destroy();

        LOG.log(Level.INFO, "Storage Controller Context Listener Destroyed.");

    }

}
