package nl.sodeso.commons.network.discovery.broadcast;

/**
 * @author Ronald Mathies
 */
public abstract class BroadcastServerConfiguration {

    public abstract boolean isEnabled();

    public abstract String getServiceId();

    public abstract int getLocalBindPort();

    public abstract int getExposedPort();

    public int getSocketTimeout() {
        return 5000;
    }

    public int getBroadcastInterval() { return 500; }

}
