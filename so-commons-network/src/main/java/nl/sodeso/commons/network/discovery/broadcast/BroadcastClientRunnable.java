package nl.sodeso.commons.network.discovery.broadcast;

import nl.sodeso.commons.network.discovery.DiscoveredService;
import nl.sodeso.commons.network.discovery.DiscoveredServices;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class BroadcastClientRunnable implements Runnable {

    private static final Logger log = Logger.getLogger(BroadcastClientRunnable.class.getName());

    private static final int BUFFER_SIZE = 256;

    private BroadcastClientConfiguration configuration;

    BroadcastClientRunnable(BroadcastClientConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void run() {
        try {
            log.info(String.format("Creating datagram socker on network address '%s'.", configuration.getLocalBindAddress()));

            DatagramSocket socket = new DatagramSocket(configuration.getLocalBindPort(), InetAddress.getByName(configuration.getLocalBindAddress()));
            socket.setBroadcast(true);

            while (true) {
                byte[] receiveBuf = new byte[BUFFER_SIZE];
                DatagramPacket receivedPacket = new DatagramPacket(receiveBuf, receiveBuf.length);

                socket.receive(receivedPacket);

                log.info(String.format("Incoming broadcast packet from network address '%s'.", receivedPacket.getAddress().getHostAddress()));

                String request = new String(receivedPacket.getData()).trim();
                if (!request.isEmpty()) {

                    String[] parts = request.split(":");
                    if (parts.length == 2) {

                        if (configuration.getServiceId().equals(parts[0])) {
                            if (DiscoveredServices.getInstance().add(new DiscoveredService(Integer.parseInt(parts[1]), receivedPacket.getAddress())) == DiscoveredServices.Status.Added) {
                                log.info("Incoming broadcast packet received and is applicable, send response to sender.");
                            }

                            byte[] sendData = String.format("%s", configuration.getServiceId()).getBytes();
                            socket.send(new DatagramPacket(sendData, sendData.length, receivedPacket.getAddress(), receivedPacket.getPort()));
                        }
                    }
                }
            }
        } catch (IOException e) {
            log.warning("Problem receiving broadcast messages: " + e.getMessage());
        }
    }
}
