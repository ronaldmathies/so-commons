package nl.sodeso.commons.network.discovery.multicast;

import nl.sodeso.commons.network.discovery.DiscoveredService;
import nl.sodeso.commons.network.discovery.DiscoveredServices;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class MulticastClientRunnable implements Runnable {

    private static final Logger log = Logger.getLogger(MulticastClientRunnable.class.getName());

    private static final int BUFFER_SIZE = 256;

    private MulticastClientConfiguration configuration;

    public MulticastClientRunnable(MulticastClientConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void run() {
        try (MulticastSocket multicastSocket = new MulticastSocket(configuration.getMulticastPort())) {

            multicastSocket.joinGroup(InetAddress.getByName(configuration.getMulticastGroup()));

            while (true) {

                byte[] receiveBuf = new byte[BUFFER_SIZE];
                DatagramPacket receivedPacket = new DatagramPacket(receiveBuf, receiveBuf.length);

                multicastSocket.receive(receivedPacket);

                String request = new String(receivedPacket.getData()).trim();
                if (!request.isEmpty()) {

                    String[] parts = request.split(":");
                    if (parts.length == 2) {

                        if (configuration.getServiceId().equals(parts[0])) {
                            if (DiscoveredServices.getInstance().add(new DiscoveredService(Integer.parseInt(parts[1]), receivedPacket.getAddress())) == DiscoveredServices.Status.Added) {
                                log.info("Incoming multicast packet received and is applicable, send response to sender.");
                            }
                        }
                    }
                }
            }

        } catch (IOException e) {
            log.warning("Problem receiving multicast messages: " + e.getMessage());
        }
    }

}
