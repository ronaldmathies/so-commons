package nl.sodeso.commons.network.discovery;

/**
 * @author Ronald Mathies
 */
public interface DiscoveredServicesListener {

    void discovered(DiscoveredService service);

}
