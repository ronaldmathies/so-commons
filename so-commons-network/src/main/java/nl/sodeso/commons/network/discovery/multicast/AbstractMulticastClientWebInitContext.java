package nl.sodeso.commons.network.discovery.multicast;

import nl.sodeso.commons.web.executor.ManagedExecutorServiceFactory;
import nl.sodeso.commons.web.initialization.AbstractWebInitContext;

import javax.enterprise.concurrent.ManagedExecutorService;
import javax.servlet.ServletContextEvent;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public abstract class AbstractMulticastClientWebInitContext extends AbstractWebInitContext {

    private static final Logger log = Logger.getLogger(AbstractMulticastClientWebInitContext.class.getName());

    private MulticastClientRunnable multicastClientRunnable = null;

    @Override
    public void initialize(ServletContextEvent sce) {
        if (getConfiguration().isEnabled()) {

            log.info("Starting multicast discovery service.");

            multicastClientRunnable = new MulticastClientRunnable(getConfiguration());

            ManagedExecutorService managedExecutorService = ManagedExecutorServiceFactory.instance().getService();
            managedExecutorService.execute(multicastClientRunnable);

            log.info(String.format("Multicast discovery service started, listening on port %d.", getConfiguration().getMulticastPort()));
        } else {
            log.info("Multicast service initialization is not enabled.");
        }
    }

    public abstract MulticastClientConfiguration getConfiguration();

    @Override
    public void destroy(ServletContextEvent sce) {
    }
}
