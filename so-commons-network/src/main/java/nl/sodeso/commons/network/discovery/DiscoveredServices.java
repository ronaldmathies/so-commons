package nl.sodeso.commons.network.discovery;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@SuppressWarnings("unused")
public class DiscoveredServices {

    private static DiscoveredServices INSTANCE = null;

    private List<DiscoveredServicesListener> listeners = new ArrayList<>();
    private List<DiscoveredService> discoveredServices = new ArrayList<>();

    public enum Status {
        Added,
        Existed
    }

    /**
     * Returns an instance of the PropertyConfiguration class.
     * @return the instance.
     */
    public static DiscoveredServices getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DiscoveredServices();
        }

        return INSTANCE;
    }

    private DiscoveredServices() {
    }

    public Status add(DiscoveredService service) {
        Status status = Status.Existed;
        if (!this.discoveredServices.contains(service)) {
            this.discoveredServices.add(service);
            status = Status.Added;
        }

        if (status == Status.Added) {
            for (DiscoveredServicesListener listener : listeners) {
                listener.discovered(service);
            }
        }

        return status;
    }

    public void remove(DiscoveredService service) {
        this.discoveredServices.remove(service);
    }

    public List<DiscoveredService> getServices() {
        return this.discoveredServices;
    }

    public void addListener(DiscoveredServicesListener listener) {
        listeners.add(listener);
    }
}
