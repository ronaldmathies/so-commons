package nl.sodeso.commons.network.discovery.multicast;

import nl.sodeso.commons.network.discovery.broadcast.BroadcastClientRunnable;

import java.net.*;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class MulticastServerRunnable implements Runnable{

    private static final Logger log = Logger.getLogger(MulticastServerRunnable.class.getName());

    private MulticastServerConfiguration configuration = null;
    private DatagramSocket datagramSocket = null;

    public MulticastServerRunnable(MulticastServerConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void run() {
        log.info("Starting to send multicast message.");

        while (true) {
            try  {
                datagramSocket = new DatagramSocket();


                final byte[] request = String.format("%s:%d", configuration.getServiceId(), configuration.getExposedPort()).getBytes();
                datagramSocket.send(new DatagramPacket(request, request.length, InetAddress.getByName(configuration.getMulticastGroup()), configuration.getMulticastPort()));

                Thread.sleep(configuration.getMulticastInterval());
            } catch (Exception e) {
                log.warning("An error occured while trying to send multicast packet: " + e.getMessage());
            } finally {
                closeSocket();
            }
        }
    }

    private void closeSocket() {
        try {
            if (datagramSocket != null) {
                datagramSocket.close();
            }
        } catch (Exception e) {
            // Do nothing...
        }
    }
}
