package nl.sodeso.commons.network.discovery.broadcast;

import javax.enterprise.concurrent.ManagedExecutorService;
import java.net.*;
import java.util.*;
import java.util.concurrent.Future;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class BroadcastServerRunnable implements Runnable {

    private static final Logger log = Logger.getLogger(BroadcastClientRunnable.class.getName());

    private ManagedExecutorService managedExecutorService;
    private BroadcastServerConfiguration configuration = null;
    private DatagramSocket datagramSocket = null;

    public BroadcastServerRunnable(ManagedExecutorService managedExecutorService, BroadcastServerConfiguration configuration) {
        this.configuration = configuration;
        this.managedExecutorService = managedExecutorService;
    }

    @Override
    public void run() {
        while (true) {
            try {
                datagramSocket = new DatagramSocket();
                datagramSocket.setSoTimeout(configuration.getSocketTimeout());
                datagramSocket.setBroadcast(true);

                log.info("Collecting all callable network interface.");

                List<BroadcastServerChannelRunnable> callableNetworkInterfaces = collectNetworkInterfaceCallables(configuration);
                if (!callableNetworkInterfaces.isEmpty()) {
                    log.info("Sending broadcast messages to all callable network interfaces.");

                    List<Future<Void>> futures = managedExecutorService.invokeAll(callableNetworkInterfaces);
                    while (futures.stream().anyMatch(future -> !future.isDone())) {
                        Thread.sleep(50);
                    }

                } else {
                    log.info("No viable network interfaces found.");
                }

                Thread.sleep(configuration.getBroadcastInterval());
            } catch (Exception e) {
                log.warning("An error occured while trying to lookup network interfaces: " + e.getMessage());
            } finally {
                closeSocket();
            }
        }
    }

    private List<BroadcastServerChannelRunnable> collectNetworkInterfaceCallables(BroadcastServerConfiguration configuration) throws SocketException {
        List<BroadcastServerChannelRunnable> callables = new ArrayList<>();
        Collections.list(NetworkInterface.getNetworkInterfaces()).forEach(networkInterface -> {
            try {
                if (!networkInterface.isLoopback() && networkInterface.isUp()) {
                    for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) {

                        InetAddress broadcastAddress = interfaceAddress.getBroadcast();
                        if (broadcastAddress == null) {
                            log.info(String.format("Address '%s' on interface '%s' not used, the address has no broadcast address.", interfaceAddress.getAddress().getHostAddress(), networkInterface.getDisplayName()));
                            continue;
                        }

                        log.info(String.format("Preparing broadcast for network interface '%s' on address '%s'.", networkInterface.getDisplayName(), interfaceAddress.getAddress().getHostAddress()));
                        callables.add(new BroadcastServerChannelRunnable(() -> datagramSocket, broadcastAddress, configuration));
                    }
                } else {
                    log.info(String.format("Network interface '%s' not used, network interface %s", networkInterface.getDisplayName(), networkInterface.isLoopback() ? " is a loopback interface." : " is not available."));
                }
            } catch (Exception e) {
                log.warning(String.format("Failed to determin if network interface '%s' is a loopback interface or if the interface is available (%s).", networkInterface.getDisplayName(), e.getMessage()));
            }
        });

        return callables;
    }

    private void closeSocket() {
        try {
            if (datagramSocket != null) {
                datagramSocket.close();
            }
        } catch (Exception e) {
            // Do nothing...
        }
    }
}
