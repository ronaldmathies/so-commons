package nl.sodeso.commons.network.discovery.broadcast;

import nl.sodeso.commons.web.executor.ManagedExecutorServiceFactory;
import nl.sodeso.commons.web.initialization.AbstractWebInitContext;

import javax.enterprise.concurrent.ManagedExecutorService;
import javax.servlet.ServletContextEvent;
import java.util.logging.Logger;

/**
 * Initializes the broadcast server listener.
 *
 * @author Ronald Mathies
 */
public abstract class AbstractBroadcastServerWebInitContext extends AbstractWebInitContext {

    private static final Logger log = Logger.getLogger(AbstractBroadcastServerWebInitContext.class.getName());

    @Override
    public void initialize(ServletContextEvent sce) {
        if (getConfiguration().isEnabled()) {
            log.info("Starting broadcast discovery service.");

            ManagedExecutorService managedExecutorService = ManagedExecutorServiceFactory.instance().getService();
            managedExecutorService.execute(new BroadcastServerRunnable(managedExecutorService, getConfiguration()));

            log.info(String.format("Broadcast discovery service started, listening on port %d.", getConfiguration().getLocalBindPort()));
        } else {
            log.info("Broadcast service is not enabled.");
        }
    }

    public abstract BroadcastServerConfiguration getConfiguration();

    @Override
    public void destroy(ServletContextEvent sce) {
    }
}
