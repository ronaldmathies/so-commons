package nl.sodeso.commons.network.discovery.broadcast;

/**
 * @author Ronald Mathies
 */
public class Constants {

    public final static String DEFAULT_IPV4_ALL_NETWORK_ADDRESSES = "0.0.0.0";
    public final static String DEFAULT_IPV6_ALL_NETWORK_ADDRESSES = "::";

}
