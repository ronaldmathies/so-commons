package nl.sodeso.commons.network.discovery.broadcast;

/**
 * @author Ronald Mathies
 */
public abstract class BroadcastClientConfiguration {

    public abstract boolean isEnabled();

    public abstract String getServiceId();

    public abstract String getLocalBindAddress();

    public abstract int getLocalBindPort();


}
