package nl.sodeso.commons.network.discovery.multicast;

/**
 * @author Ronald Mathies
 */
public abstract class MulticastServerConfiguration {

    public abstract boolean isEnabled();

    public abstract String getServiceId();

    public abstract String getMulticastGroup();

    public abstract int getMulticastPort();

    public abstract int getExposedPort();

    public abstract int getMulticastInterval();

}
