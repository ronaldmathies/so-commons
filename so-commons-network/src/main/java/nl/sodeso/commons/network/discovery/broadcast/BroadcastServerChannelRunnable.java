package nl.sodeso.commons.network.discovery.broadcast;

import javax.enterprise.inject.spi.CDI;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.Callable;
import java.util.function.Supplier;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class BroadcastServerChannelRunnable implements Callable<Void> {

    private static final Logger log = Logger.getLogger(BroadcastServerChannelRunnable.class.getName());

    private static final int BUFFER_SIZE = 256;

    private Supplier<DatagramSocket> datagramSocket = null;
    private InetAddress broadcastAddress = null;
    private BroadcastServerConfiguration configuration = null;

    BroadcastServerChannelRunnable(Supplier<DatagramSocket> datagramSocket, InetAddress broadcastAddress, BroadcastServerConfiguration configuration) {
        this.datagramSocket = datagramSocket;
        this.broadcastAddress = broadcastAddress;
        this.configuration = configuration;
    }

    @Override
    public Void call() {
        try {
            final byte[] request = String.format("%s:%d", configuration.getServiceId(), configuration.getExposedPort()).getBytes();
            datagramSocket.get().send(new DatagramPacket(request, request.length, broadcastAddress, configuration.getLocalBindPort()));

            //Wait for a response
            byte[] receiveBuf = new byte[BUFFER_SIZE];
            DatagramPacket receivePacket = new DatagramPacket(receiveBuf, receiveBuf.length);
            datagramSocket.get().receive(receivePacket);

            String response = new String(receivePacket.getData()).trim();
            if (configuration.getServiceId().equals(response)) {
                log.info("Found new broadcast initialization: " + receivePacket.getAddress());
                return null;
            }
        } catch (IOException e) {
            log.warning(String.format("Failed to get a response on a broadcast for '%s'.", broadcastAddress.getHostAddress()));
        }

        return null;
    }

}