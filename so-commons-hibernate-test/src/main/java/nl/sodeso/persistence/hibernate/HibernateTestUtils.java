package nl.sodeso.persistence.hibernate;

import nl.sodeso.persistence.jdbc.TableUtils;
import org.hibernate.Session;

import javax.annotation.Nonnull;

/**
 * @author Ronald Mathies
 */
public class HibernateTestUtils {

    /**
     * Convenience method to truncate all tables that are accessable using the specified session.
     * @param session the session to use to truncate all tables.
     */
    public static void truncateAllTables(@Nonnull Session session ) {
        session.doWork(TableUtils::truncateAllTables);
    }

}
