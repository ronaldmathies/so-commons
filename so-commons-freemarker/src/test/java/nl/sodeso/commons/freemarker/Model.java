package nl.sodeso.commons.freemarker;

/**
 * @author Ronald Mathies
 */
public class Model {

    private String variable;

    public Model() {
        this.variable = "test";
    }

    public String getVariable() {
        return this.variable;
    }
}
