package nl.sodeso.commons.freemarker;

import nl.sodeso.commons.security.digest.DigestUtil;
import nl.sodeso.commons.security.digest.Algorithm;

import freemarker.template.Template;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

/**
 * @author Ronald Mathies
 */
public class FreemarkerUtilTest {

    @Test
    public void testLoadTemplate() throws Exception {
        Template template = FreemarkerUtil.loadTemplate("freemarker.tpl");
        assertNotNull("Template should be not null.", template);

        boolean exceptionThrown = false;
        try {
            FreemarkerUtil.loadTemplate("freemarker_fail.tpl");
        } catch (FreemarkerException e) {
            exceptionThrown = true;
        }

        assertTrue("Exception should be thrown.", exceptionThrown);
    }

    @Test
    public void testProcess() throws Exception {
        Template template = FreemarkerUtil.loadTemplate("freemarker.tpl");

        File moduleFolder = new File(System.getProperty("user.dir"));
        File outputFile = new File(moduleFolder, "build/tmp/output.txt");
        FreemarkerUtil.process(template, new Model(), outputFile);
        assertEquals("Digest should match.", "964298ff0385e982f2088619204feffd3ada3d8d1deba398e9d9d31692e709ce", DigestUtil.digest(outputFile, Algorithm.SHA256));
    }
}