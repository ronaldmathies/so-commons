package nl.sodeso.commons.freemarker;

import freemarker.template.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Ronald Mathies
 */
public class FreemarkerUtil {

    /**
     * The Freemarker template to load, the file will be loaded from the freemarker/templates folder on the classpath.
     * @param filename the filename of the template.
     *
     * @return the loaded template.
     *
     * @throws FreemarkerException exception will be thrown when the template cannot be loaded.
     */
    public static Template loadTemplate(String filename) throws FreemarkerException {
        try {
            Configuration configuration = new Configuration(Configuration.VERSION_2_3_22);
            configuration.setClassLoaderForTemplateLoading(FreemarkerUtil.class.getClassLoader(), "freemarker/templates");
            configuration.setDefaultEncoding("UTF-8");

            return configuration.getTemplate(filename);
        } catch (IOException e) {
            throw new FreemarkerException("Failed to load template.", e);
        }
    }

    /**
     * Processes the template using the specified model, outputs the result to the specified file.
     *
     * @param template the loaded template to process.
     * @param model the data model to use when processing the template.
     * @param outputFile the file to write the results to.
     *
     * @throws FreemarkerException when an error occurred while processing the template.
     */
    public static void process(Template template, Object model, File outputFile) throws FreemarkerException {
        try (FileWriter fw = new FileWriter(outputFile)) {
            template.process(model, fw);
        } catch (TemplateException | IOException e){
            throw new FreemarkerException("Failed to process template.", e);
        }
    }
}
