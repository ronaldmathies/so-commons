package nl.sodeso.commons.freemarker;

/**
 * @author Ronald Mathies
 */
public class FreemarkerException extends Exception {

    /**
     * {@inheritDoc}
     */
    public FreemarkerException(String message, Throwable cause) {
        super(message, cause);
    }

}
