package nl.sodeso.commons.restful.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

/**
 * Deserializer for reading in RAW json structures somewhere in the middle of a
 * incomming message.
 *
 * @author Ronald Mathies
 */
public class RawJsonDeserializer extends JsonDeserializer<String> {

    @Override
    public String deserialize(JsonParser jp, DeserializationContext context) throws IOException {
        TreeNode tree = jp.getCodec().readTree(jp);
        return tree.toString();
    }
}
