package nl.sodeso.commons.restful.client;

/**
 * @author Ronald Mathies
 */
public class SimpleRestClientConfiguration implements RestClientConfiguration {

    private String host;
    private int port;

    public SimpleRestClientConfiguration(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public int getPort() {
        return port;
    }
}
