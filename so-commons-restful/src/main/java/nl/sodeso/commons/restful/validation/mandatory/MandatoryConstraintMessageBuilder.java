package nl.sodeso.commons.restful.validation.mandatory;

import nl.sodeso.commons.restful.model.Message;
import nl.sodeso.commons.restful.builder.ConstraintMessageBuilder;

import javax.validation.ConstraintViolation;

/**
 * @author Ronald Mathies
 */
public class MandatoryConstraintMessageBuilder implements ConstraintMessageBuilder<MandatoryConstraint> {

    @Override
    public Message parse(MandatoryConstraint constraint, ConstraintViolation violation) {
        return new Message(constraint.code(), constraint.message());
    }
}
