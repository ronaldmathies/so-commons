package nl.sodeso.commons.restful.client;

/**
 * Created by sodeso on 03/05/16.
 */
public class RestClientException extends Exception {

    public RestClientException(String message, Object ... args) {
        super(String.format(message, args));
    }

    public RestClientException(Throwable cause, String message, Object ... args) {
        super(String.format(message, args), cause);
    }
}
