package nl.sodeso.commons.restful.validation;

//import org.glassfish.jersey.server.validation.ValidationConfig;
//import org.glassfish.jersey.server.validation.internal.InjectingConstraintValidatorFactory;

import javax.validation.ParameterNameProvider;
import javax.validation.Validation;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.ContextResolver;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public abstract class AbstractValidationConfigurationContextResolver {}
//implements ContextResolver<ValidationConfig> {
//
//    @Context
//    private ResourceContext resourceContext;
//
//    @Override
//    public ValidationConfig getContext(final Class<?> type) {
//        return new ValidationConfig()
//                .constraintValidatorFactory(resourceContext.getResource(InjectingConstraintValidatorFactory.class))
//                .parameterNameProvider(new CustomParameterNameProvider());
//    }
//
//    /**
//     * @see ParameterNameProvider#getParameterNames(Method)
//     */
//    protected abstract List<String> getParameterNames(Method method);
//
//    protected abstract List<String> getParameterNames(final Constructor<?> constructor);
//
//    /**
//     * Custom parameter name provider which will use the getParameterNames to resolve the names
//     * of the parameters, if no parameters are found it will fall back to the default provider.
//     */
//    private class CustomarameterNameProvider implements ParameterNameProvider {
//
//        private final ParameterNameProvider defaultParameterNameProvider;
//
//        public CustomParameterNameProvider() {
//            defaultParameterNameProvider = Validation.byDefaultProvider().configure().getDefaultParameterNameProvider();
//        }
//
//        @Override
//        public List<String> getParameterNames(final Constructor<?> constructor) {
//            List<String> parameterNames = AbstractValidationConfigurationContextResolver.this.getParameterNames(constructor);
//            if (parameterNames != null && !parameterNames.isEmpty()) {
//                return parameterNames;
//            }
//
//            return defaultParameterNameProvider.getParameterNames(constructor);
//        }
//
//        @Override
//        public List<String> getParameterNames(final Method method) {
//            List<String> parameterNames = AbstractValidationConfigurationContextResolver.this.getParameterNames(method);
//            if (parameterNames != null && !parameterNames.isEmpty()) {
//                return parameterNames;
//            }
//
//            return defaultParameterNameProvider.getParameterNames(method);
//        }
//    }
//}