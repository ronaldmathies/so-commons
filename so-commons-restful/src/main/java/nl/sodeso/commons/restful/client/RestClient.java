package nl.sodeso.commons.restful.client;

import org.jboss.resteasy.spi.ResteasyProviderFactory;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.*;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class RestClient {

    private enum Method {
        GET,
        DELETE,
        POST,
        PUT
    }

    private RestClientConfiguration configuration;

    public RestClient(RestClientConfiguration configuration) {
        this.configuration = configuration;
    }

    public void put(String path, Entity entity, RestClientCallback callback) throws RestClientException {
        put(path, entity, callback, (Object[])null);
    }

    public void put(String path, Entity entity, RestClientCallback callback, Object ... args) throws RestClientException {
        execute(Method.PUT, path, entity, callback, null,  args);
    }

    public void post(String path, Entity entity, RestClientCallback callback) throws RestClientException {
        post(path, entity, callback, null, (Object[])null);
    }

    public void post(String path, Entity entity, RestClientCallback callback, Map<String, Object> queryParams) throws RestClientException {
        post(path, entity, callback, queryParams, (Object[])null);
    }

    public void post(String path, Entity entity, RestClientCallback callback, Object ... args) throws RestClientException {
        post(path, entity, callback, null,  args);
    }

    public void post(String path, Entity entity, RestClientCallback callback, Map<String, Object> queryParams, Object ... args) throws RestClientException {
        execute(Method.POST, path, entity, callback, queryParams,  args);
    }

    public void delete(String path, RestClientCallback callback) throws RestClientException {
        delete(path, callback, null, (Object[])null);
    }

    public void delete(String path, RestClientCallback callback, Map<String, Object> queryParams) throws RestClientException {
        delete(path, callback, queryParams, (Object[])null);
    }

    public void delete(String path, RestClientCallback callback, Map<String, Object> queryParams, Object ... args) throws RestClientException {
        execute(Method.DELETE, path, callback, queryParams,  args);
    }

    public void delete(String path, RestClientCallback callback, Object ... args) throws RestClientException {
        execute(Method.DELETE, path, callback, null,  args);
    }

    public void get(String path, RestClientCallback callback) throws RestClientException {
        get(path, callback, null, (Object[])null);
    }

    public void get(String path, RestClientCallback callback, Map<String, Object> queryParams) throws RestClientException {
        get(path, callback, queryParams, (Object[])null);
    }

    public void get(String path, RestClientCallback callback, Object ... args) throws RestClientException {
        get(path, callback, null, args);
    }

    public void get(String path, RestClientCallback callback, Map<String, Object> queryParams, Object ... args) throws RestClientException {
        execute(Method.GET, path, callback, queryParams, args);
    }

    private void execute(Method method, String path, Entity entity, RestClientCallback callback, Map<String, Object> queryParams, Object ... args) throws RestClientException {
        try {
//            Configuration configuration = ResteasyProviderFactory.getInstance().p
            Client client = ClientBuilder.newClient();

            WebTarget target = client.target(buildUriFrom(path, args));
            if (queryParams != null) {
                for (Map.Entry<String, Object> queryParam : queryParams.entrySet()) {
                    target = target.queryParam(queryParam.getKey(), queryParam.getValue());
                }            }

            Invocation.Builder request = target.request();
            Response response = method.equals(Method.POST) ? request.post(entity) : request.put(entity);

            if (response.getStatus() == Response.Status.OK.getStatusCode()) {
                callback.onSuccess(response);
            } else {
                callback.onFailure(response);
            }

        } catch (UnsupportedEncodingException | MalformedURLException | URISyntaxException e) {
            throw new RestClientException(e, "Failed to post information to the webservice mock due to an exception: %s", e.getMessage());
        }
    }

    private void execute(Method method, String path, RestClientCallback callback, Map<String, Object> queryParams, Object... args) throws RestClientException {
        try {
            Client client = ClientBuilder.newClient();

            WebTarget target = client.target(buildUriFrom(path, args));
            if (queryParams != null) {
                for (Map.Entry<String, Object> queryParam : queryParams.entrySet()) {
                    target = target.queryParam(queryParam.getKey(), queryParam.getValue());
                }
            }

            Invocation.Builder request = target.request();
            Response response = method.equals(Method.GET) ? request.get() : request.delete();

            if (response.getStatus() == Response.Status.OK.getStatusCode()) {
                callback.onSuccess(response);
            } else {
                callback.onFailure(response);
            }
        } catch (ProcessingException | UnsupportedEncodingException | MalformedURLException | URISyntaxException e) {
            throw new RestClientException(e, "Failed to post information to the webservice mock due to an exception: %s", e.getMessage());
        }
    }

    protected String buildUriFrom(String path, Object ... args) throws UnsupportedEncodingException, MalformedURLException, URISyntaxException {
        return new URI("http", null, configuration.getHost(), configuration.getPort(), args != null && args.length > 0 ? String.format(path, args) : path, null, null).toASCIIString();
    }

}
