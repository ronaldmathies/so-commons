package nl.sodeso.commons.restful.validation.range;

import nl.sodeso.commons.restful.builder.ConstraintMessageBuilder;
import nl.sodeso.commons.restful.model.Message;
import nl.sodeso.commons.restful.validation.mandatory.MandatoryConstraint;

import javax.validation.ConstraintViolation;

/**
 * @author Ronald Mathies
 */
public class DoubleRangeConstraintMessageBuilder implements ConstraintMessageBuilder<DoubleRangeConstraint> {

    @Override
    public Message parse(DoubleRangeConstraint constraint, ConstraintViolation violation) {
        return new Message(constraint.code(), constraint.message());
    }
}
