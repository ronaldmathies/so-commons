package nl.sodeso.commons.restful.validation.mandatory;

import nl.sodeso.commons.restful.builder.Builder;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Ronald Mathies
 */
@Target( {PARAMETER} )
@Retention( RUNTIME)
@Constraint(validatedBy = MandatoryConstraintValidator.class)
@Builder(parsedBy = MandatoryConstraintMessageBuilder.class)
@Documented
public @interface MandatoryConstraint {

    /**
     * The code to use when the validation of the constraint leads to an error.
     * @return the code.
     */
    String code();

    /**
     * The message to use when the validation of the constraint leads to an error.
     * @return the message.
     */
    String message();

    /**
     * Are <code>NULL</code> values allowed (so either null or non-empty), when false
     * the value must be non-null and non-empty.
     *
     * @return flag indicating if null values are allowed.
     */
    boolean allowNull() default false;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
