package nl.sodeso.commons.restful.validation.range;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author Ronald Mathies
 */
public class IntegerRangeConstraintValidator implements ConstraintValidator<IntegerRangeConstraint, Integer> {

    private IntegerRangeConstraint constraint;

    /**
     * {@inheritDoc}
     */
    public void initialize(IntegerRangeConstraint constraint) {
        this.constraint = constraint;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isValid(Integer value, ConstraintValidatorContext constraintContext) {
        return !(value != null && value < constraint.minValue()) && !(value != null && value > constraint.maxValue());

    }
}
