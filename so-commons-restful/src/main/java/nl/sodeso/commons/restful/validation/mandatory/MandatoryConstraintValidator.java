package nl.sodeso.commons.restful.validation.mandatory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author Ronald Mathies
 */
public class MandatoryConstraintValidator implements ConstraintValidator<MandatoryConstraint, String> {

    private MandatoryConstraint mandatoryConstraint;

    /**
     * {@inheritDoc}
     */
    public void initialize(MandatoryConstraint mandatoryConstraint) {
        this.mandatoryConstraint = mandatoryConstraint;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isValid(String value, ConstraintValidatorContext constraintContext) {
        if (mandatoryConstraint.allowNull()) {
            return value == null || !value.isEmpty();
        }

        return value != null && !value.isEmpty();

    }
}
