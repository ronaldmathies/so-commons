package nl.sodeso.commons.restful.validation.range;

import nl.sodeso.commons.restful.builder.Builder;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Ronald Mathies
 */
@Target( {PARAMETER} )
@Retention( RUNTIME)
@Constraint(validatedBy = IntegerRangeConstraintValidator.class)
@Builder(parsedBy = IntegerRangeConstraintMessageBuilder.class)
@Documented
public @interface IntegerRangeConstraint {

    /**
     * The code to use when the validation of the constraint leads to an error.
     * @return the code.
     */
    String code();

    /**
     * The message to use when the validation of the constraint leads to an error.
     * @return the message.
     */
    String message();

    /**
     * The minimum allowed value (including)
     *
     * @return the minimum allowed value.
     */
    int minValue() default Integer.MIN_VALUE;

    /**
     * The maximum allowed value (including)
     *
     * @return the maximum allowed value.
     */
    int maxValue() default Integer.MAX_VALUE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}


