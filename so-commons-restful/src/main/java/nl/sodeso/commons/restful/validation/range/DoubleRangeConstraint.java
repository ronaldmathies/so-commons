package nl.sodeso.commons.restful.validation.range;

import nl.sodeso.commons.restful.builder.Builder;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Ronald Mathies
 */
@Target( {PARAMETER} )
@Retention( RUNTIME)
@Constraint(validatedBy = DoubleRangeConstraintValidator.class)
@Builder(parsedBy = DoubleRangeConstraintMessageBuilder.class)
@Documented
public @interface DoubleRangeConstraint {

    /**
     * The code to use when the validation of the constraint leads to an error.
     * @return the code.
     */
    String code();

    /**
     * The message to use when the validation of the constraint leads to an error.
     * @return the message.
     */
    String message();

    /**
     * The minimum allowed value (including)
     *
     * @return the minimum allowed value.
     */
    double minValue() default Double.MIN_VALUE;

    /**
     * The maximum allowed value (including)
     *
     * @return the maximum allowed value.
     */
    double maxValue() default Double.MAX_VALUE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}


