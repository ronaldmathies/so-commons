package nl.sodeso.commons.restful.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Container for holding a number of messages.
 *
 * @author Ronald Mathies
 */
public class Messages {

    private List<Message> messages = new ArrayList<>();

    /**
     * Constructs a new empty messages container.
     */
    public Messages() {}

    /**
     * Adds an additional message to the list of messages.
     * @param message the message to add.
     * @return a reference to this.
     */
    public Messages addMessage(Message message) {
        this.messages.add(message);
        return this;
    }

    /**
     * Returns all messages.
     * @return all messages.
     */
    public List<Message> getMessages() {
        return this.messages;
    }

}
