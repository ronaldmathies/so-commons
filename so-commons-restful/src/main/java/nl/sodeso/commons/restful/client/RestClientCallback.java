package nl.sodeso.commons.restful.client;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

/**
 * @author Ronald Mathies
 */
public interface RestClientCallback {

    void onSuccess(Response response) throws RestClientException;
    void onFailure(Response response) throws RestClientException;
}
