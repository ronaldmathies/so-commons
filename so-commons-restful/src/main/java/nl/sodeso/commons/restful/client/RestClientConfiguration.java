package nl.sodeso.commons.restful.client;

/**
 * @author Ronald Mathies
 */
public interface RestClientConfiguration {

    String getHost();
    int getPort();

}
