package nl.sodeso.commons.restful.validation.range;

import nl.sodeso.commons.restful.builder.ConstraintMessageBuilder;
import nl.sodeso.commons.restful.model.Message;

import javax.validation.ConstraintViolation;

/**
 * @author Ronald Mathies
 */
public class FloatRangeConstraintMessageBuilder implements ConstraintMessageBuilder<FloatRangeConstraint> {

    @Override
    public Message parse(FloatRangeConstraint constraint, ConstraintViolation violation) {
        return new Message(constraint.code(), constraint.message());
    }
}
