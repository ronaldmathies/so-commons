package nl.sodeso.commons.restful.validation;

import nl.sodeso.commons.restful.builder.Builder;
import nl.sodeso.commons.restful.builder.ConstraintMessageBuilder;
import nl.sodeso.commons.restful.model.*;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

/**
 * JAXWS-RS Exception mapper.
 *
 * @author Ronald Mathies
 */
@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    private Map<Class, ConstraintMessageBuilder> builders = new HashMap<>();

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public Response toResponse(ConstraintViolationException cve) {
        Messages messages = new Messages();

        for (ConstraintViolation cv : cve.getConstraintViolations()) {
            Annotation annotation = cv.getConstraintDescriptor().getAnnotation();

            ConstraintMessageBuilder constraintMessageBuilder = lookupBuilder(annotation);
            if (constraintMessageBuilder != null) {
                messages.addMessage(constraintMessageBuilder.parse(annotation, cv));
            }
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(messages).type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }

    /**
     * Looks up an implementation of the ConstraintMessageBuilder that may be registered on the Constraint annotation.
     * @param constraintAnnotation the Constraint annotation.
     * @return the message builder implementation or null when no implementation could be found.
     */
    private ConstraintMessageBuilder<?> lookupBuilder(Annotation constraintAnnotation) {
        Class<? extends Annotation> constraintType = constraintAnnotation.annotationType();
        if (constraintType.isAnnotationPresent(Builder.class)) {
            Class<? extends ConstraintMessageBuilder<?>> messageBuilderClass =
                    constraintType.getAnnotation(Builder.class).parsedBy();

            if (!builders.containsKey(messageBuilderClass)) {
                try {
                    builders.put(messageBuilderClass, messageBuilderClass.newInstance());
                } catch (InstantiationException | IllegalAccessException e) {
                    // Log that we had a problem instantiating the builder.
                }
            }

            return builders.get(messageBuilderClass);
        }

        return null;
    }
}
