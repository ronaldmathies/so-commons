package nl.sodeso.commons.restful.model;

/**
 * Message container for holding the code / description of an error message.
 *
 * @author Ronald Mathies
 */
public class Message {

    private String code;
    private String message;

    /**
     * Constructs a new empty message.
     */
    public Message() {}

    /**
     * Constrcuts a new message.
     *
     * @param code the code.
     * @param message the message.
     */
    public Message(String code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * Returns the code.
     * @return the code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     * @param code the code.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Return the message.
     * @return the message.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message.
     * @param message the message.
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
