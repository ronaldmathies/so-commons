package nl.sodeso.commons.restful.validation.range;

import nl.sodeso.commons.restful.builder.ConstraintMessageBuilder;
import nl.sodeso.commons.restful.model.Message;

import javax.validation.ConstraintViolation;

/**
 * @author Ronald Mathies
 */
public class IntegerRangeConstraintMessageBuilder implements ConstraintMessageBuilder<IntegerRangeConstraint> {

    @Override
    public Message parse(IntegerRangeConstraint constraint, ConstraintViolation violation) {
        return new Message(constraint.code(), constraint.message());
    }
}
