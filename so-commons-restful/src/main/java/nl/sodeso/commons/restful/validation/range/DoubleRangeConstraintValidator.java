package nl.sodeso.commons.restful.validation.range;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author Ronald Mathies
 */
public class DoubleRangeConstraintValidator implements ConstraintValidator<DoubleRangeConstraint, Double> {

    private DoubleRangeConstraint constraint;

    /**
     * {@inheritDoc}
     */
    public void initialize(DoubleRangeConstraint constraint) {
        this.constraint = constraint;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isValid(Double value, ConstraintValidatorContext constraintContext) {
        return !(value != null && value < constraint.minValue()) && !(value != null && value > constraint.maxValue());

    }
}
