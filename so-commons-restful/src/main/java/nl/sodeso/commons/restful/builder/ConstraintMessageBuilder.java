package nl.sodeso.commons.restful.builder;

import nl.sodeso.commons.restful.model.Message;

import javax.validation.ConstraintViolation;
import java.lang.annotation.Annotation;

/**
 * @author Ronald Mathies
 */
public interface ConstraintMessageBuilder<A extends Annotation> {

    Message parse(A constraint, ConstraintViolation violation);

}
