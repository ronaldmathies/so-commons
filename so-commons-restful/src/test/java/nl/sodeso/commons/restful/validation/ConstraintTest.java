package nl.sodeso.commons.restful.validation;

import com.jayway.restassured.http.ContentType;
import nl.sodeso.commons.restful.validation.endpoint.ConstraintEndpoint;
import nl.sodeso.commons.restful.validation.endpoint.ConstraintServiceConfig;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
//import org.glassfish.jersey.server.ServerProperties;
//import org.glassfish.jersey.servlet.ServletContainer;
//import org.glassfish.jersey.servlet.ServletProperties;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.ServerSocket;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import static org.junit.Assert.*;

/**
 * @author Ronald Mathies
 *
 * REST-Assured
 */
public class ConstraintTest {
/*
    private static int HTTP_PORT;

    private static Server server;

    @BeforeClass
    public static void startJetty() throws Exception {
        HTTP_PORT = findFirstAvailableFreePort();

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        server = new Server(HTTP_PORT);
        server.setHandler(context);

        ServletHolder servlet = context.addServlet(ServletContainer.class, "/*");
        servlet.setInitOrder(0);
        servlet.setInitParameter(ServerProperties.PROVIDER_CLASSNAMES, ConstraintEndpoint.class.getCanonicalName());
        servlet.setInitParameter(ServletProperties.JAXRS_APPLICATION_CLASS, ConstraintServiceConfig.class.getCanonicalName());
        server.start();
    }

    @AfterClass
    public static void stopJetty() throws Exception {
        server.stop();
        server.join();
    }

    // TODO: Nice method for in a utility class?
    public static int findFirstAvailableFreePort() {
        int port = -1;

        try (ServerSocket socket = new ServerSocket(0)) {
            port = socket.getLocalPort();
        } catch (Exception e) {
            fail("Failed to : " + e.getMessage());
        }

        return port;
    }


    @Test
    public void mandatoryNullAllowedWithValueOk() throws Exception {
        given().port(HTTP_PORT)
            .queryParam("param", "value")
        .when()
            .get("/constraints/nullAllowedCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("code", equalTo("OK"));
    }

    @Test
    public void mandatoryNullAllowedWithoutValueOk() throws Exception {
        given().port(HTTP_PORT)
        .when()
            .get("/constraints/nullAllowedCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("code", equalTo("OK"));
    }

    @Test
    public void mandatoryNullAllowedEmptyValueNok() throws Exception {
        given().port(HTTP_PORT)
            .queryParam("param", "")
        .when()
            .get("/constraints/nullAllowedCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("messages[0].code", equalTo("ERR01"))
            .body("messages[0].message", equalTo("Error-01"));
    }

    @Test
    public void mandatoryNullNotAllowedWithValueOk() throws Exception {
        given().port(HTTP_PORT)
            .queryParam("param", "value")
        .when()
            .get("/constraints/nullNotAllowedCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("code", equalTo("OK"));
    }

    @Test
    public void mandatoryNullNotAllowedWithoutValueNok() throws Exception {
        given().port(HTTP_PORT)
        .when()
            .get("/constraints/nullNotAllowedCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("messages[0].code", equalTo("ERR02"))
            .body("messages[0].message", equalTo("Error-02"));
    }

    @Test
    public void mandatoryNullNotAllowedEmptyValueNok() throws Exception {
        given().port(HTTP_PORT)
            .queryParam("param", "")
        .when()
            .get("/constraints/nullNotAllowedCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("messages[0].code", equalTo("ERR02"))
            .body("messages[0].message", equalTo("Error-02"));
    }

    @Test
    public void integerMinValueOk() throws Exception {
        given().port(HTTP_PORT)
            .queryParam("param", -100)
        .when()
            .get("/constraints/integerCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("code", equalTo("OK"));
    }

    @Test
    public void integerMaxValueOk() throws Exception {
        given().port(HTTP_PORT)
            .queryParam("param", 100)
        .when()
            .get("/constraints/integerCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("code", equalTo("OK"));
    }

    @Test
    public void integerNullOk() throws Exception {
        given().port(HTTP_PORT)
        .when()
            .get("/constraints/integerCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("code", equalTo("OK"));
    }

    @Test
    public void integerMinValueNok() throws Exception {
        given().port(HTTP_PORT)
            .queryParam("param", -101)
        .when()
            .get("/constraints/integerCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("messages[0].code", equalTo("ERR03"))
            .body("messages[0].message", equalTo("Error-03"));
    }

    @Test
    public void integerMaxValueNok() throws Exception {
        given().port(HTTP_PORT)
            .queryParam("param", 101)
        .when()
            .get("/constraints/integerCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("messages[0].code", equalTo("ERR03"))
            .body("messages[0].message", equalTo("Error-03"));
    }

    @Test
    public void doubleMinValueOk() throws Exception {
        given().port(HTTP_PORT)
            .queryParam("param", -100.5)
        .when()
            .get("/constraints/doubleCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("code", equalTo("OK"));
    }

    @Test
    public void doubleMaxValueOk() throws Exception {
        given().port(HTTP_PORT)
            .queryParam("param", 100.5)
        .when()
            .get("/constraints/doubleCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("code", equalTo("OK"));
    }

    @Test
    public void doubleNullOk() throws Exception {
        given().port(HTTP_PORT)
        .when()
            .get("/constraints/doubleCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("code", equalTo("OK"));
    }

    @Test
    public void doubleMinValueNok() throws Exception {
        given().port(HTTP_PORT)
            .queryParam("param", -100.6)
        .when()
            .get("/constraints/doubleCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("messages[0].code", equalTo("ERR04"))
            .body("messages[0].message", equalTo("Error-04"));
    }

    @Test
    public void doubleMaxValueNok() throws Exception {
        given().port(HTTP_PORT)
            .queryParam("param", 100.6)
        .when()
            .get("/constraints/doubleCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("messages[0].code", equalTo("ERR04"))
            .body("messages[0].message", equalTo("Error-04"));
    }

    @Test
    public void floatMinValueOk() throws Exception {
        given().port(HTTP_PORT)
            .queryParam("param", -100.5f)
        .when()
            .get("/constraints/floatCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("code", equalTo("OK"));
    }

    @Test
    public void floatMaxValueOk() throws Exception {
        given().port(HTTP_PORT)
            .queryParam("param", 100.5f)
        .when()
            .get("/constraints/floatCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("code", equalTo("OK"));
    }

    @Test
    public void floatNullOk() throws Exception {
        given().port(HTTP_PORT)
        .when()
            .get("/constraints/floatCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("code", equalTo("OK"));
    }

    @Test
    public void floatMinValueNok() throws Exception {
        given().port(HTTP_PORT)
            .queryParam("param", -100.6f)
        .when()
            .get("/constraints/floatCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("messages[0].code", equalTo("ERR05"))
            .body("messages[0].message", equalTo("Error-05"));
    }

    @Test
    public void floatMaxValueNok() throws Exception {
        given().port(HTTP_PORT)
            .queryParam("param", 100.6f)
        .when()
            .get("/constraints/floatCheck")
        .then()
            .contentType(ContentType.JSON)
            .body("messages[0].code", equalTo("ERR05"))
            .body("messages[0].message", equalTo("Error-05"));
    }*/
}
