package nl.sodeso.commons.restful.view.endpoint.model;

/**
 * Created by sodeso on 30/06/16.
 */
public class ModelView {

    public static class Summary {}
    public static class Detail extends Summary {}

}
