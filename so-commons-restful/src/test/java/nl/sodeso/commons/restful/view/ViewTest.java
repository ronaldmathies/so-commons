package nl.sodeso.commons.restful.view;

//import com.jayway.restassured.http.ContentType;
//import com.jayway.restassured.response.ValidatableResponse;
//import nl.sodeso.commons.restful.client.RestClient;
//import nl.sodeso.commons.restful.client.RestClientCallback;
//import nl.sodeso.commons.restful.client.RestClientConfiguration;
//import nl.sodeso.commons.restful.validation.endpoint.ConstraintEndpoint;
//import nl.sodeso.commons.restful.view.endpoint.ViewServiceConfig;
//import nl.sodeso.commons.restful.view.endpoint.model.Model;
//import org.eclipse.jetty.server.Server;
//import org.eclipse.jetty.servlet.ServletContextHandler;
//import org.eclipse.jetty.servlet.ServletHolder;
//import org.glassfish.jersey.server.ServerProperties;
//import org.glassfish.jersey.servlet.ServletContainer;
//import org.glassfish.jersey.servlet.ServletProperties;
//import org.junit.AfterClass;
//import org.junit.BeforeClass;
//import org.junit.Test;
//
//import javax.ws.rs.core.GenericType;
//import javax.ws.rs.core.Response;
//import java.net.ServerSocket;
//import java.util.List;
//
//import static com.jayway.restassured.RestAssured.given;
//import static org.hamcrest.Matchers.equalTo;
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNull;
//import static org.junit.Assert.fail;

/**
 * @author Ronald Mathies
 */
public class ViewTest {
/*
    private static int HTTP_PORT;

    private static Server server;

    @BeforeClass
    public static void startJetty() throws Exception {
        HTTP_PORT = findFirstAvailableFreePort();

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        server = new Server(HTTP_PORT);
        server.setHandler(context);

        ServletHolder servlet = context.addServlet(ServletContainer.class, "/*");
        servlet.setInitOrder(0);
        servlet.setInitParameter(ServerProperties.PROVIDER_CLASSNAMES, ConstraintEndpoint.class.getCanonicalName());
        servlet.setInitParameter(ServletProperties.JAXRS_APPLICATION_CLASS, ViewServiceConfig.class.getCanonicalName());
        server.start();
    }

    @AfterClass
    public static void stopJetty() throws Exception {
        server.stop();
        server.join();
    }

    // TODO: Nice method for in a utility class?
    public static int findFirstAvailableFreePort() {
        int port = -1;

        try (ServerSocket socket = new ServerSocket(0)) {
            port = socket.getLocalPort();
        } catch (Exception e) {
            fail("Failed to : " + e.getMessage());
        }

        return port;
    }

    @Test
    public void summaryView() throws Exception {
        ValidatableResponse validatableResponse = given().port(HTTP_PORT)
            .when()
            .get("/views/summary")
            .then()
            .contentType(ContentType.JSON);

        validatableResponse
            .body("summary", equalTo("summary"));

        validatableResponse
            .body("details", equalTo(null));
    }

    @Test
    public void detailView() throws Exception {
        ValidatableResponse validatableResponse = given().port(HTTP_PORT)
            .pathParam("param", "detail")
            .when()
            .get("/views/detail/{param}")
            .then()
            .contentType(ContentType.JSON);

        validatableResponse
            .body("summary", equalTo("summary"));

        validatableResponse
            .body("detail", equalTo("detail"));
    }

    @Test
    public void summaryViewRestClient() throws Exception {
        RestClient restClient = new RestClient(new RestClientConfiguration() {
            @Override
            public String getHost() {
                return "localhost";
            }

            @Override
            public int getPort() {
                return HTTP_PORT;
            }
        });

        restClient.get("/views/summary", new RestClientCallback() {
            @Override
            public void onSuccess(Response response) {
                List<Model> models = response.readEntity(new GenericType<List<Model>>() {});
                assertEquals(1, models.size());

                Model model = models.get(0);
                assertEquals("summary", model.getSummary());
                assertNull(model.getDetail());
            }

            @Override
            public void onFailure(Response response) {
                fail("onFailure should not be called.");
            }
        });
    }

    @Test
    public void detailViewRestClient() throws Exception {
        RestClient restClient = new RestClient(new RestClientConfiguration() {
            @Override
            public String getHost() {
                return "localhost";
            }

            @Override
            public int getPort() {
                return HTTP_PORT;
            }
        });

        restClient.get("/views/detail/%s", new RestClientCallback() {
            @Override
            public void onSuccess(Response response) {
                Model model = response.readEntity(Model.class);
                assertEquals("summary", model.getSummary());
                assertEquals("detail", model.getDetail());
            }

            @Override
            public void onFailure(Response response) {
                fail("onFailure should not be called.");
            }
        }, "detail");
    }
*/
}
