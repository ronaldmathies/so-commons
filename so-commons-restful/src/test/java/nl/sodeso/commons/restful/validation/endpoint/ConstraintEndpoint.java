package nl.sodeso.commons.restful.validation.endpoint;

//import nl.sodeso.commons.restful.model.Message;
//import nl.sodeso.commons.restful.validation.mandatory.MandatoryConstraint;
//import nl.sodeso.commons.restful.validation.range.DoubleRangeConstraint;
//import nl.sodeso.commons.restful.validation.range.FloatRangeConstraint;
//import nl.sodeso.commons.restful.validation.range.IntegerRangeConstraint;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * @author Ronald Mathies
 */
@Path("/constraints")
public class ConstraintEndpoint {/*

    public static final String ENCODING = "UTF-8";

    @Context
    private UriInfo headers;

    @GET
    @Path("nullAllowedCheck")
    @Produces(MediaType.APPLICATION_JSON)
    public Response nullAllowedCheck(
            final @MandatoryConstraint(allowNull = true, code = "ERR01", message = "Error-01") @QueryParam("param") String param) {

        Response.ResponseBuilder response = Response.ok();
        response.encoding(ENCODING);

        return Response.status(Response.Status.OK).entity(new Message("OK", "ok")).type(MediaType.APPLICATION_JSON_TYPE).build();
    }

    @GET
    @Path("nullNotAllowedCheck")
    @Produces(MediaType.APPLICATION_JSON)
    public Response nullNotAllowedCheck(
            final @MandatoryConstraint(allowNull = false, code = "ERR02", message = "Error-02") @QueryParam("param") String param) {

        Response.ResponseBuilder response = Response.ok();
        response.encoding(ENCODING);

        return Response.status(Response.Status.OK).entity(new Message("OK", "ok")).type(MediaType.APPLICATION_JSON_TYPE).build();
    }

    @GET
    @Path("integerCheck")
    @Produces(MediaType.APPLICATION_JSON)
    public Response integerCheck(
            final @IntegerRangeConstraint(minValue = -100, maxValue = 100, code = "ERR03", message = "Error-03") @QueryParam("param") Integer param) {

        Response.ResponseBuilder response = Response.ok();
        response.encoding(ENCODING);

        return Response.status(Response.Status.OK).entity(new Message("OK", "ok")).type(MediaType.APPLICATION_JSON_TYPE).build();
    }

    @GET
    @Path("doubleCheck")
    @Produces(MediaType.APPLICATION_JSON)
    public Response doubleCheck(
            final @DoubleRangeConstraint(minValue = -100.5, maxValue = 100.5, code = "ERR04", message = "Error-04") @QueryParam("param") Double param) {

        Response.ResponseBuilder response = Response.ok();
        response.encoding(ENCODING);

        return Response.status(Response.Status.OK).entity(new Message("OK", "ok")).type(MediaType.APPLICATION_JSON_TYPE).build();
    }

    @GET
    @Path("floatCheck")
    @Produces(MediaType.APPLICATION_JSON)
    public Response doubleCheck(
            final @FloatRangeConstraint(minValue = -100.5f, maxValue = 100.5f, code = "ERR05", message = "Error-05") @QueryParam("param") Float param) {

        Response.ResponseBuilder response = Response.ok();
        response.encoding(ENCODING);

        return Response.status(Response.Status.OK).entity(new Message("OK", "ok")).type(MediaType.APPLICATION_JSON_TYPE).build();
    }
*/
}
