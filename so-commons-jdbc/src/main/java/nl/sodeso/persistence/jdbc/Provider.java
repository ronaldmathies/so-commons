package nl.sodeso.persistence.jdbc;

/**
 * @author Ronald Mathies
 */
public enum Provider {

    H2("H2"),
    MySQL("MySQL"),
    DB2_AIX64("DB2/AIX64");

    private String provider = null;

    Provider(String provider) {
        this.provider = provider;
    }

    public String getProvider() {
        return this.provider;
    }

    /**
     * Returns a provider based on the name of the provider.
     * @param provider the provider to look for.
     * @return the provider found or <code>null</code>
     */
    public static Provider getProvider(String provider) {
        if (H2.getProvider().equalsIgnoreCase(provider)) {
            return H2;
        } else if (MySQL.getProvider().equalsIgnoreCase(provider)) {
            return MySQL;
        } else if (DB2_AIX64.getProvider().equalsIgnoreCase(provider)) {
            return DB2_AIX64;
        }

        return null;
    }

    /**
     * Flag indicating if this is a H2 provider.
     * @return true if this is a H2 provider, false if not.
     */
    public boolean isH2() {
        return this.equals(Provider.H2);
    }

    /**
     * Flag indicating if this is a MySQL provider.
     * @return true if this is a MySQL provider, false if not.
     */
    public boolean isMySql() {
        return this.equals(Provider.MySQL);
    }

    /**
     * Flag indicating if this is a DB2/AIX64 provider.
     * @return true if this is a DB2/AIX64 provider, false if not.
     */
    public boolean isDb2Aix64() {
        return this.equals(Provider.DB2_AIX64);
    }

}
