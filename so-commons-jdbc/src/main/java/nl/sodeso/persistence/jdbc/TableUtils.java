package nl.sodeso.persistence.jdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class TableUtils {

    private static final String DISABLE_REFERENTIAL_INTEGRITY_MYSQL = "SET GLOBAL FOREIGN_KEY_CHECKS=0;";
    private static final String DISABLE_REFERNTIAL_INTEGRITY_H2 = "ALTER TABLE %s SET REFERENTIAL_INTEGRITY FALSE";
    private static final String TRUNCATE_TABLE = "TRUNCATE TABLE %s";
    private static final String ENABLE_REFERENTIAL_INTEGRITY_H2 = "ALTER TABLE %s SET REFERENTIAL_INTEGRITY TRUE";
    private static final String ENABLE_REFERENTIAL_INTEGRITY_MYSQL = "SET GLOBAL FOREIGN_KEY_CHECKS=1;";

    /**
     * Truncates all tables that can be found using the specified connection.
     * @param connection the connection to use to truncate all tables.
     * @throws SQLException when there was a abnormal situation while truncating the tables.
     */
    public static void truncateAllTables(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        DatabaseMetaData metaData = connection.getMetaData();

        Provider provider = Provider.getProvider(metaData.getDatabaseProductName());
        if (provider == null) {
            throw new SQLException(String.format("Unsupported provider '%s'.", metaData.getDatabaseProductName()));
        }

        List<String> catalogs = new ArrayList<>();
        ResultSet catalogsRs = metaData.getCatalogs();
        while (catalogsRs.next()) {
            catalogs.add(catalogsRs.getString(1));
        }
        catalogsRs.close();

        for (String catalog : catalogs) {

            List<String> tables = new ArrayList<>();
            ResultSet tablesRs = metaData.getTables(catalog, null, "%", new String[] {"TABLE"});
            while (tablesRs.next()) {
                tables.add(tablesRs.getString(3));
            }
            tablesRs.close();

            if (provider.isH2()) {
                truncateAllTablesH2(statement, tables);
            } else if (provider.isMySql()) {
                truncateAllTablesMySql(statement, tables);
            }
        }
    }

    /**
     * Truncates all the tables for an H2 database.
     * @param statement the statement to use to perform the SQL executions.
     * @param tables the tables to truncate.
     * @throws SQLException when there was a abnormal situation while truncating the tables.
     */
    private static void truncateAllTablesH2(Statement statement, List<String> tables) throws SQLException {
        for (String table : tables) {
            statement.execute(String.format(DISABLE_REFERNTIAL_INTEGRITY_H2, table));
        }

        for (String table : tables) {
            statement.execute(String.format(TRUNCATE_TABLE, table));
        }

        for (String table : tables) {
            statement.execute(String.format(ENABLE_REFERENTIAL_INTEGRITY_H2, table));
        }
    }

    /**
     * Truncates all the tables for an H2 database.
     * @param statement the statement to use to perform the SQL executions.
     * @param tables the tables to truncate.
     * @throws SQLException when there was a abnormal situation while truncating the tables.
     */
    private static void truncateAllTablesMySql(Statement statement, List<String> tables) throws SQLException {
        statement.execute(DISABLE_REFERENTIAL_INTEGRITY_MYSQL);

        for (String table : tables) {
            statement.execute(String.format(TRUNCATE_TABLE, table));
        }

        statement.execute(ENABLE_REFERENTIAL_INTEGRITY_MYSQL);
    }

}
