package nl.sodeso.persistence.jdbc;

import nl.sodeso.commons.fileutils.FileFinder;
import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.*;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class SqlScriptExecutor {

    private static final Logger LOG = Logger.getLogger(SqlScriptExecutor.class.getName());

    /**
     * Executes the contents of the sql file using the specified connection.
     *
     * @param connection the connection to use.
     * @param sqlFile the sql file to execute.
     */
    public static boolean execute( final Connection connection, String sqlFile) throws IOException {
        final boolean[] success = new boolean[1];
        success[0] = false;

        FileFinder.findFileInUserPath(sqlFile, file -> {
            try {
                execute(connection, file);

                success[0] = true;
            } catch (IOException e) {
                LOG.log(Level.SEVERE, "Failed to import file '%s' due to the following reason '%s'.", new String[] {file.getAbsolutePath(), e.getMessage()});
            }
        });

        return success[0];
    }

    /**
     * Executes the contents of the sql file using the specified connection.
     *
     * @param connection the connection to use.
     * @param inputStream the input stream which contains the sql statements to execute.
     */
    public static void execute(Connection connection, InputStream inputStream) {
        execute(connection, new InputStreamReader(inputStream));
    }

    /**
     * Executes the contents of the sql file using the specified connection.
     *
     * @param connection the connection to use.
     * @param sqlFile the sql file to execute.
     * @throws IOException when an IO exception occurs.
     */
    public static void execute(Connection connection, File sqlFile) throws IOException {
        execute(connection, new BufferedReader(new FileReader(sqlFile)));
    }

    /**
     * Executes the contents of the sql file using the specified connection.
     *
     * @param connection the connection to use.
     * @param reader the reader which contains the sql statements to execute.
     */
    public static void execute(Connection connection, Reader reader) {
        ScriptRunner runner = new ScriptRunner(connection);
        runner.setAutoCommit(true);
        runner.runScript(reader);
    }

}
