package nl.sodeso.commons.properties.containers.jdbc;

import nl.sodeso.commons.properties.AbstractContainer;
import nl.sodeso.commons.properties.annotations.JdbcResource;
import nl.sodeso.commons.properties.annotations.JdbcResources;

/**
 * @author Ronald Mathies
 */
public class JdbcContainer extends AbstractContainer<JdbcEntry> {

    public JdbcContainer() {
        super();

        JdbcResources resources = getClass().getAnnotation(JdbcResources.class);

        for (int idx = 0; idx < resources.jdbc().length; idx++) {
            JdbcResource resource = resources.jdbc()[idx];

            addSource(new JdbcEntry(resource));
        }
    }

}
