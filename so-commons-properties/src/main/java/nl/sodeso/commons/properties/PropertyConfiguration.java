package nl.sodeso.commons.properties;

import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.events.PropertyDomainChangedEvent;
import nl.sodeso.commons.properties.events.PropertyDomainChangedHandler;
import nl.sodeso.commons.web.classpath.ClasspathWebUtil;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * @author Ronald Mathies
 */
public class PropertyConfiguration {

    private static final Logger LOG = Logger.getLogger(PropertyConfiguration.class.getName());

    private static final String QUARTZ_CONFIGURATION = "property-configuration-quartz.properties";
    private static final String QUARTZ_CRON_EVERY_TEN_SECONDS = "0/10 * * * * ? *";
    private static final String QUARTZ_JOB = "PropertyJob";

    private static PropertyConfiguration INSTANCE;

    private List<AbstractContainer> containers = new ArrayList<>();
    private List<PropertyDomainChangedHandler> handlers = new ArrayList<>();

    private final Object lock = new Object();
    private boolean isInitialized = false;

    /**
     * Returns an instance of the PropertyConfiguration class.
     * @return the instance.
     */
    public static PropertyConfiguration getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PropertyConfiguration();
        }

        return INSTANCE;
    }

    private PropertyConfiguration() {
    }

    private void initialize() {
        if (!isInitialized) {

            synchronized (lock) {
                collectAndInstantiateResources();
                setupResourceMonitoring();

                isInitialized = true;
            }

        }
    }

    /**
     * Adds a handler which will receive events when a property domain has been changed.
     * @param handler the handler to add.
     */
    public void addPropertyDomainChangedHandler(@Nonnull PropertyDomainChangedHandler handler) {
        this.handlers.add(handler);
    }

    /**
     * Fires an event on all known handlers that what to be notified that a
     * property domain has been changed.
     *
     * @param event the event to fire.
     */
    private void firePropertyDomainChangedEvent(PropertyDomainChangedEvent event) {
        for (PropertyDomainChangedHandler handler : handlers) {
            handler.event(event);
        }
    }

    /**
     * Collects and instantiates any containers it can find.
     */
    private void collectAndInstantiateResources() {
        Set<Class<?>> resourceClasses = ClasspathWebUtil.instance().typesAnnotatedWith(Resource.class, "nl.sodeso");
        for (Class<?> clazz : resourceClasses) {
            try {
                LOG.log(Level.INFO, String.format("Found resource '%s'.", clazz.getName()));
                AbstractContainer resource = (AbstractContainer)clazz.newInstance();
                containers.add(resource);

                logResourceEntries(resource);
            } catch (IllegalAccessException | InstantiationException e) {
                LOG.log(Level.SEVERE, "Failed to load configuration resource '" + clazz.getCanonicalName() + "'.", e);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void logResourceEntries(AbstractContainer resource) {
        int colWidths[] = {3, 5};
        resource.getEntries().forEachRemaining(new Consumer<Map.Entry<String, String>>() {
            @Override
            public void accept(Map.Entry<String, String> entry) {
                String key = entry.getKey();
                String value = entry.getValue().replace("\n", "<lb>").replace("\r", "<nl>");
                colWidths[0] = colWidths[0] < key.length() ? key.length() : colWidths[0];

                if (colWidths[1] < value.length()) {
                    if (value.length() > 150) {
                        colWidths[1] = 150;
                    } else {
                        colWidths[1] = value.length();
                    }
                }
            }
        });

        StringBuilder logging = new StringBuilder("\n");
        logging.append(String.format("|%1$"+colWidths[0] + "s|%2$"+colWidths[1] + "s|\n", "key", "value"));

        resource.getEntries().forEachRemaining(new Consumer<Map.Entry<String, String>>() {
            @Override
            public void accept(Map.Entry<String, String> entry) {
                logging.append(String.format("|%1$"+colWidths[0] + "s|", entry.getKey()));

                String value = entry.getValue().replace("\n", "<lb>").replace("\r", "<nl>");
                if (value.length() > 150) {
                    logging.append(String.format("%1$" +colWidths[1] + "s|\n", value.substring(0, 150)));

                    String[] parts = value.split("(?<=\\G.{150})");
                    for (int index = 01; index < parts.length; index++) {
                        logging.append(String.format("|%1$"+colWidths[0] + "s|%2$"+colWidths[1] + "s|\n", "", parts[index]));
                    }
                } else {
                    logging.append(String.format("%1$"+colWidths[1] + "s|\n", value));
                }
            }
        });

        LOG.log(Level.INFO, logging.toString());
    }

    public void addContainer(AbstractContainer container) {
        containers.add(container);
    }

    /**
     * Sets up resource monitoring through a quartz job.
     */
    private void setupResourceMonitoring() {
        LOG.log(Level.INFO, "Setting up Quartz Scheduler for the properties configuration.");

        boolean enableScheduler = false;
        for (AbstractContainer propertySource : containers) {
            if (propertySource.isSynchronizationEnabled()) {
                enableScheduler = true;
                break;
            }
        }

        if (enableScheduler) {

            try {
                StdSchedulerFactory stdSchedulerFactory = new StdSchedulerFactory(QUARTZ_CONFIGURATION);
                Scheduler scheduler = stdSchedulerFactory.getScheduler();

                JobDetail jobDetail = newJob(PropertyConfigurationJob.class).build();
                scheduler.scheduleJob(jobDetail, newTrigger().forJob(jobDetail).withSchedule(
                        CronScheduleBuilder.cronSchedule(QUARTZ_CRON_EVERY_TEN_SECONDS)).withIdentity(QUARTZ_JOB).build());

                scheduler.start();
                LOG.log(Level.INFO, "Finished setting up Quartz Scheduler for the properties configuration.");
            } catch (SchedulerException e) {
                LOG.log(Level.INFO, "Failed to setup the Quartz Scheduler for the properties configuration.");
            }

        }
    }

    /**
     * Performs a synchronisation on all containers, when a container did perform a synchronisation
     * then an event will be thrown with the domain to notify any interested parties about the
     * change.
     */
    protected void synchronise() {
        for (AbstractContainer container : containers) {
            boolean didSynchronise = container.synchronise();

            if (didSynchronise) {
                PropertyDomainChangedEvent event = new PropertyDomainChangedEvent(container.getDomain());
                firePropertyDomainChangedEvent(event);
            }
        }
    }


//    /**
//     * Returns a list of classes that have the <code>Resource</code> annotation
//     * present.
//     *
//     * @return a list of classes.
//     */
//    private Set<Class<?>> collectClassesWithResourceAnnotation() {
//        LOG.log(Level.INFO, "Collecting classes with resource annotations.");
//
//        Collection<URL> urls = Web;
//        if (webInfLibUrls == null) {
//            urls = ClasspathHelper.forManifest();
//        }
//
//        StringBuilder urlOverview = new StringBuilder("The following URL's were found for resolving resources:\n\r");
//        for (URL url : urls) {
//            urlOverview.append(String.format("%s\n\r", url.toString()));
//        }
//        LOG.log(Level.INFO, urlOverview.toString());
//
//        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder()
//                .setScanners(new TypeAnnotationsScanner(), new SubTypesScanner())
//                .setExecutorService(Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(),
//                        new ThreadFactoryBuilder().setDaemon(true).setNameFormat("org.reflections-%d").build()
//                ))
//                .setUrls(urls);
//
//        ReflectionsHelper.registerUrlTypes();
//        Reflections reflections = new Reflections(configurationBuilder);
//        Set<Class<?>> typesAnnotatedWith = reflections.getTypesAnnotatedWith(Resource.class);
//
//        ExecutorService executorService = configurationBuilder.getExecutorService();
//        if (executorService != null) {
//            executorService.shutdownNow();
//        }
//
//        LOG.log(Level.INFO, String.format("Found %d classes with resource annotations.", typesAnnotatedWith.size()));
//
//        return typesAnnotatedWith;
//    }

    /**
     * Returns all the keys that reside within the container with the specified domain or <code>null</code>
     * when no container was found with the specified domain.
     *
     * @param domain the domain.
     * @return all the keys or <code>null</code>.
     */
    @SuppressWarnings("unchecked")
    public Iterator<String> getKeys(String domain) {
        AbstractContainer propertySource = getContainerByDomain(domain);
        if (propertySource != null) {
            return propertySource.getKeys();
        }

        return null;
    }

    /**
     * Returns all the entries that reside within the container with the specified domain or <code>null</code>
     * when no container was found with the specified domain.
     *
     * @param domain the domain.
     * @return all the entries or <code>null</code>.
     */
    @SuppressWarnings("unchecked")
    public Iterator<Map.Entry<Object, Object>> getEntries(String domain) {
        AbstractContainer propertySource = getContainerByDomain(domain);
        if (propertySource != null) {
            return propertySource.getEntries();
        }

        return null;
    }

    /**
     * Reverse container lookup, find a container which has a property
     * matching the key, value combination as provided.
     *
     * @param key the key to look for.
     * @param value the value that the key should have.
     * @return the domain name of the container.
     */
    public String getPropertyContainerDomainBy(String key, String value) {
        initialize();

        for (AbstractContainer container : containers) {
            if (container.containsProperty(key)) {
                if (container.getProperty(key).equals(value)) {
                    return container.getDomain();
                }
            }
        }

        return null;
    }

    /**
     * Returns the <code>String</code> value registered with the specified <code>key</code>
     * and <code>domain</code>.
     *
     * When there is no value registered with the key then <code>null</code> is returned.
     *
     * @param domain the domain
     * @param key the key
     * @return the value or null when no value is associated with the key.
     */
    public String getStringProperty(String domain, String key) {
        return getStringProperty(domain, key, null);
    }

    /**
     * Returns the <code>String</code> value registered with the specified <code>key</code>
     * and <code>domain</code>.
     *
     * When there is no value registered with the key then the <code>valueIfAbstent</code>
     * is returned.
     *
     * @param domain the domain
     * @param key the key
     * @param valueIfAbsent the value to return when the key cannot be found in the domain.
     * @return the value or valueIfAbstent when no value is associated with the key.
     */
    public String getStringProperty(String domain, String key, String valueIfAbsent) {
        AbstractContainer propertySource = getContainerByDomainAndKey(domain, key);
        if (propertySource != null) {
            return propertySource.getProperty(key);
        }

        return valueIfAbsent;
    }

    /**
     * Returns the <code>Boolean</code> value registered with the specified <code>key</code>
     * and <code>domain</code>.
     *
     * When there is no value registered with the key then <code>null</code>
     * is returned.
     *
     * @param domain the domain
     * @param key the key
     * @return the value or null when no value is associated with the key.
     */
    public Boolean getBooleanProperty(String domain, String key) {
        return getBooleanProperty(domain, key, null);
    }

    /**
     * Returns the <code>Boolean</code> value registered with the specified <code>key</code>
     * and <code>domain</code>.
     *
     * When there is no value registered with the key then the <code>valueIfAbstent</code>
     * is returned.
     *
     * @param domain the domain
     * @param key the key
     * @param valueIfAbsent the value to return when the key cannot be found in the domain.
     * @return the value or valueIfAbstent when no value is associated with the key.
     */
    public Boolean getBooleanProperty(String domain, String key, Boolean valueIfAbsent) {
        AbstractContainer propertySource = getContainerByDomainAndKey(domain, key);
        if (propertySource != null) {
            return Boolean.parseBoolean(propertySource.getProperty(key));
        }

        return valueIfAbsent;
    }

    /**
     * Returns the <code>Integer</code> value registered with the specified <code>key</code>
     * and <code>domain</code>.
     *
     * When there is no value registered with the key then <code>null</code>
     * is returned.
     *
     * @param domain the domain
     * @param key the key
     * @return the value or null when no value is associated with the key.
     */
    public Integer getIntegerProperty(String domain, String key) {
        return getIntegerProperty(domain, key, null);
    }

    /**
     * Returns the <code>Integer</code> value registered with the specified <code>key</code>
     * and <code>domain</code>.
     *
     * When there is no value registered with the key then the <code>valueIfAbstent</code>
     * is returned.
     *
     * @param domain the domain
     * @param key the key
     * @param valueIfAbsent the value to return when the key cannot be found in the domain.
     * @return the value or valueIfAbstent when no value is associated with the key.
     */
    public Integer getIntegerProperty(String domain, String key, Integer valueIfAbsent) {
        AbstractContainer propertySource = getContainerByDomainAndKey(domain, key);
        if (propertySource != null) {

            try {
                return Integer.parseInt(propertySource.getProperty(key));
            } catch (NumberFormatException e) {
                LOG.log(Level.SEVERE, "The value registered with the property '" + key + "' in domain '" + domain + "' is not an integer value, returning default.");
            }
        }

        return valueIfAbsent;
    }

    /**
     * Returns the <code>Long</code> value registered with the specified <code>key</code>
     * and <code>domain</code>.
     *
     * When there is no value registered with the key then <code>null</code>
     * is returned.
     *
     * @param domain the domain
     * @param key the key
     * @return the value or null when no value is associated with the key.
     */
    public Long getLongProperty(String domain, String key) {
        return getLongProperty(domain, key, null);
    }

    /**
     * Returns the <code>Long</code> value registered with the specified <code>key</code>
     * and <code>domain</code>.
     *
     * When there is no value registered with the key then the <code>valueIfAbstent</code>
     * is returned.
     *
     * @param domain the domain
     * @param key the key
     * @param valueIfAbsent the value to return when the key cannot be found in the domain.
     * @return the value or valueIfAbstent when no value is associated with the key.
     */
    public Long getLongProperty(String domain, String key, Long valueIfAbsent) {
        AbstractContainer propertySource = getContainerByDomainAndKey(domain, key);
        if (propertySource != null) {
            try {
                return Long.parseLong(propertySource.getProperty(key));
            } catch (NumberFormatException e) {
                LOG.log(Level.SEVERE, String.format("The value registered with the property '%s' in domain '%s' is not a long value, returning default.", key, domain));
            }
        }

        return valueIfAbsent;
    }

    /**
     * Returns the <code>Long</code> value registered with the specified <code>key</code>
     * and <code>domain</code>.
     *
     * When there is no value registered with the key then <code>null</code>
     * is returned.
     *
     * @param domain the domain
     * @param key the key
     * @return the value or null when no value is associated with the key.
     */
    public File getFileProperty(String domain, String key) {
        return getFileProperty(domain, key, null);
    }

    /**
     * Returns the <code>Long</code> value registered with the specified <code>key</code>
     * and <code>domain</code>.
     *
     * When there is no value registered with the key then the <code>valueIfAbstent</code>
     * is returned.
     *
     * @param domain the domain
     * @param key the key
     * @param valueIfAbsent the value to return when the key cannot be found in the domain.
     * @return the value or valueIfAbstent when no value is associated with the key.
     */
    public File getFileProperty(String domain, String key, File valueIfAbsent) {
        AbstractContainer propertySource = getContainerByDomainAndKey(domain, key);
        if (propertySource != null) {
            return new File(propertySource.getProperty(key));
        }

        return valueIfAbsent;
    }

    private AbstractContainer getContainerByDomainAndKey(String domain, String key) {
        initialize();

        for (AbstractContainer propertySource : containers) {
            if (propertySource.getDomain().equals(domain) && propertySource.containsProperty(key)) {
                return propertySource;
            }
        }

        LOG.log(Level.INFO, String.format("Property %s#%s not found.", domain, key));

        return null;
    }

    private AbstractContainer getContainerByDomain(String domain) {
        for (AbstractContainer propertySource : containers) {
            if (propertySource.getDomain().equals(domain)) {
                return propertySource;
            }
        }

        LOG.log(Level.INFO, String.format("Container %s not found.", domain));

        return null;
    }
}
