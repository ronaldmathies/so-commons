package nl.sodeso.commons.properties.containers.environment;

import nl.sodeso.commons.properties.AbstractContainer;
import nl.sodeso.commons.properties.annotations.EnvironmentResource;

/**
 * The FileContainer is used to load various property files from disk, it supports
 * monitoring of these files for changes.
 *
 * @author Ronald Mathies
 */
public class EnvironmentContainer extends AbstractContainer<EnvironmentEntry> {

    public EnvironmentContainer() {
        super();

        EnvironmentResource resource = getClass().getAnnotation(EnvironmentResource.class);
        if (resource != null) {
            addSource(new EnvironmentEntry(resource));
        }
    }

}
