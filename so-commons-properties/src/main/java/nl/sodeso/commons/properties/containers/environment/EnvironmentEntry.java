package nl.sodeso.commons.properties.containers.environment;

import nl.sodeso.commons.properties.AbstractEntry;
import nl.sodeso.commons.properties.annotations.EnvironmentResource;

import javax.annotation.Nonnull;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class EnvironmentEntry extends AbstractEntry {

    private static final Logger log = Logger.getLogger(EnvironmentEntry.class.getName());

    private EnvironmentResource resource;

    /**
     * Constructs a new object with the specified file.
     */
    public EnvironmentEntry(@Nonnull EnvironmentResource resource) {
        this.resource = resource;

        synchronise();
    }

    /**
     * {@inheritDoc}
     */
    public boolean isSynchronizationEnabled() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public boolean synchronise() {
        if (!hasProperties()) {
            load(System.getenv());
        }

        return false;
    }



}