package nl.sodeso.commons.properties;

import nl.sodeso.commons.properties.annotations.Resource;

import java.util.*;

/**
 * @author Ronald Mathies
 */
public abstract class AbstractContainer<T extends AbstractEntry> {

    private List<T> sources = new ArrayList<>();

    private Resource sourceConfig = null;

    public AbstractContainer() {
        sourceConfig = getClass().getAnnotation(Resource.class);
    }

    public Resource getSourceConfig() {
        return this.sourceConfig;
    }

    /**
     * Domain donates a group of properties belonging together, multiple resources can be combined into
     * a single domain.
     *
     * @return the domein.
     */

    /**
     * Returns the domain name that this container represents.
     */
    public String getDomain() {
        return sourceConfig.domain();
    }

    /**
     * Adds a new entry to the list.
     *
     * @param entry the entry to add.
     */
    public void addSource(T entry) {
        this.sources.add(entry);
    }

    /**
     * Returns all known sources.
     *
     * @return all known sources.
     */
    public List<T> getSources() {
        return this.sources;
    }

    /**
     * Returns all the keys that reside within this container.
     * @return all the keys that reside within this container.
     */
    public Iterator<String> getKeys() {
        IteratorSequence<String> iteratorSequence = new IteratorSequence<>();

        for (AbstractEntry entry : sources) {
            iteratorSequence.addIterator(entry.getKeys());
        }

        return iteratorSequence;
    }

    /**
     * Returns all the entries that reside within this container.
     * @return all the entries that reside within this container.
     */
    public Iterator<Map.Entry<String, String>> getEntries() {
        IteratorSequence<Map.Entry<String, String>> iteratorSequence = new IteratorSequence<>();

        for (AbstractEntry entry : sources) {
            iteratorSequence.addIterator(entry.getEntries());
        }

        return iteratorSequence;
    }

    /**
     * Flag indicating if this resource contains the specified key.
     *
     * @param key the key to look for.
     *
     * @return true if the resource contains the key, false if not.
     */
    public boolean containsProperty(String key) {
        for (AbstractEntry entry : sources) {
            if (entry.containesProperty(key)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the value associated with the specified key.
     *
     * @param key the key to retrieve.
     * @return the value associated with the key.
     */
    public String getProperty(String key) {
        for (AbstractEntry entry : sources) {
            if (entry.containesProperty(key)) {
                return entry.getProperty(key);
            }
        }

        return null;
    }

    /**
     * Triggers a synchronisation for every entry within this container.
     * @return flag indicating af any synchronisation has taken place.
     */
    public boolean synchronise() {
        boolean didSynchronise = false;

        for (T entry : sources) {
            if (entry.synchronise()) {
                didSynchronise = true;
            }
        }

        return didSynchronise;
    }

    /**
     * Flag indicating if any of the known sources has synchronization enabled.
     *
     * @return flag indicating if any of the known sources has synchronization enabled.
     */
    public boolean isSynchronizationEnabled() {
        for (T entry : getSources()) {
            if (entry.isSynchronizationEnabled()) {
                return true;
            }
        }

        return false;
    }

}
