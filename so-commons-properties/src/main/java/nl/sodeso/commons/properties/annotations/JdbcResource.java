package nl.sodeso.commons.properties.annotations;

import java.lang.annotation.*;

/**
 * The jdbc resource annotation can be used in conjunction with the
 * <code>Resource</code> annotation to add a JDBC based resource
 * to the <code>PropertyConfiguration</code>
 *
 * @author Ronald Mathies
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JdbcResource {

    /**
     * The JDNI name to use when creating a connection.
     * @return the jndi name.
     */
    String jndi();

    /**
     * The prefix for the database tables.
     * @return the prefix.
     */
    String prefix();

    /**
     * Flag indicating if we need to monitor this resource.
     * @return true to monitor this resource, false to ignore.
     */
    boolean monitor() default false;

}
