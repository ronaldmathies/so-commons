package nl.sodeso.commons.properties.containers.system;

import nl.sodeso.commons.properties.AbstractEntry;
import nl.sodeso.commons.properties.annotations.SystemResource;

import javax.annotation.Nonnull;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class SystemEntry extends AbstractEntry {

    private static final Logger log = Logger.getLogger(SystemEntry.class.getName());

    private SystemResource resource;

    /**
     * Constructs a new object with the specified file.
     */
    public SystemEntry(@Nonnull SystemResource resource) {
        this.resource = resource;

        synchronise();
    }

    /**
     * {@inheritDoc}
     */
    public boolean isSynchronizationEnabled() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public boolean synchronise() {
        if (!hasProperties()) {
            load(System.getProperties());
        }

        return false;
    }

}