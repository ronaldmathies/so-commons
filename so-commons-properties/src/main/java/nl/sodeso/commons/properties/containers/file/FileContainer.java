package nl.sodeso.commons.properties.containers.file;

import nl.sodeso.commons.properties.AbstractContainer;
import nl.sodeso.commons.properties.annotations.FileResource;
import nl.sodeso.commons.properties.annotations.FileResources;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The FileContainer is used to load various property files from disk, it supports
 * monitoring of these files for changes.
 *
 * @author Ronald Mathies
 */
public class FileContainer extends AbstractContainer<FileEntry> {

    private static final Logger log = Logger.getLogger(FileContainer.class.getName());

    public FileContainer() {
        super();

        FileResources resources = getClass().getAnnotation(FileResources.class);
        FileResource resource = null;

        try {
            for (int idx = 0; idx < resources.files().length; idx++) {
                resource = resources.files()[idx];

                // First try to open the file as a file, this only works when it is reachable from
                // disk itself. If the file resides in a JAR / WAR archive then try to open it as a stream.

                boolean loadByStream = false;
                try {
                    File file = new File(FileContainer.class.getResource(resource.file()).toURI());
                    if (file.exists()) {
                        addSource(new FileEntry(resource, file));
                    } else {
                        // Apparently the file does not exist, let's try input stream.
                        loadByStream = true;
                    }
                } catch (IllegalArgumentException e) {
                    // So it is not a standard file, fallback to input stream.
                    loadByStream = true;
                }
                if (loadByStream) {
                    try (InputStream inputStream = FileContainer.class.getResourceAsStream(resource.file())) {
                        addSource(new FileEntry(resource, inputStream));
                    }
                }
            }

        } catch (Exception exception) {
            log.log(Level.SEVERE, "Failed to load the " + resource.file() + " properties.");
        }
    }

}
