package nl.sodeso.commons.properties.annotations;

import java.lang.annotation.*;

/**
 * The file resource annotation can be used in conjunction with the
 * <code>Resource</code> annotation to add a file based resource
 * to the <code>PropertyConfiguration</code>
 *
 * NOTE: when the file mentioned in the resource resides in a JAR / WAR archive
 * it will be loaded, monitoring however is not supported and will be disabled
 * by default.
 *
 * @author Ronald Mathies
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FileResource {

    /**
     * The file that exists on the classpath.
     * @return the name of the file that exists on the classpath.
     */
    String file();

    /**
     * Flag indicating if this file should be monitored for changed.
     *
     * This option is not possible when the file to be loaded resides in
     * a JAR / WAR archive.
     *
     * @return true to enable monitoring, false to disable it.
     */
    boolean monitor() default false;
}
