package nl.sodeso.commons.properties.events;

import javax.annotation.Nonnull;

/**
 * This event will be thrown to any registered handlers when a domain has changed
 * property contents.
 *
 * @author Ronald Mathies
 */
public class PropertyDomainChangedEvent {

    private String domain = null;

    public PropertyDomainChangedEvent(@Nonnull String domain) {
        this.domain = domain;
    }

    public String getDomain() {
        return this.domain;
    }

}
