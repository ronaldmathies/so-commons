package nl.sodeso.commons.properties;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public abstract class AbstractEntry {

    private static final Logger log = Logger.getLogger(AbstractEntry.class.getName());

    private Map<String, String> properties = new HashMap<>();

    public AbstractEntry() {}

    public boolean containesProperty(@Nonnull String key) {
        return properties.containsKey(key);
    }

    public String getProperty(String key) {
        if (properties.containsKey(key)) {
            return (String)properties.get(key);
        }

        return null;
    }

    protected Iterator<String> getKeys() {
        return properties.keySet().iterator();
    }

    protected Iterator<Map.Entry<String, String>> getEntries() {
        return properties.entrySet().iterator();
    }

    public boolean hasProperties() {
        return !this.properties.isEmpty();
    }

    public void load(@Nonnull File file) throws IOException {
        try (FileInputStream fis = new FileInputStream(file)) {
            load(fis);

        }
    }

    public void load(@Nonnull Properties properties) {
        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            this.properties.put((String)entry.getKey(), (String)entry.getValue());
        }
    }

    public void load(@Nonnull InputStream inputStream) {
        try {
            Properties properties = new Properties();
            properties.load(inputStream);
            load(properties);
        } catch (IOException e) {
            log.log(Level.INFO, "Failed to load input stream for reading properties.");
        }
    }

    public void load(@Nonnull Map<String, String> values) {
        properties.putAll(values);
    }

    public abstract boolean synchronise();

    public abstract boolean isSynchronizationEnabled();
}
