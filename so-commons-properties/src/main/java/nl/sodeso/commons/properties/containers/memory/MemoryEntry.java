package nl.sodeso.commons.properties.containers.memory;

import nl.sodeso.commons.properties.AbstractEntry;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class MemoryEntry extends AbstractEntry {

    private static final Logger log = Logger.getLogger(MemoryEntry.class.getName());

    /**
     * Constructs a new object with the specified properties.
     */
    public MemoryEntry(@Nonnull Map<String, String> properties) {
        load(properties);
    }

    /**
     * {@inheritDoc}
     */
    public boolean isSynchronizationEnabled() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public boolean synchronise() {
        return false;
    }



}