package nl.sodeso.commons.properties.containers.file;

import nl.sodeso.commons.properties.AbstractEntry;
import nl.sodeso.commons.properties.annotations.FileResource;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class FileEntry extends AbstractEntry {

    private static final Logger log = Logger.getLogger(FileEntry.class.getName());

    private FileResource resource;

    private File file;
    private InputStream inputStream;

    private long[] times = {-1, -1};

    /**
     * Constructs a new object with the specified file.
     * @param resource the resource.
     * @param file the file.
     */
    public FileEntry(@Nonnull FileResource resource, @Nonnull File file) {
        this.resource = resource;
        this.file = file;

        synchronise();
    }

    /**
     * Constructs a new object with the specified input stream.
     *
     * Do note that file resources that end-up being a stream will not support
     * automatic reloading. Examples of this are resources that reside within a JAR / WAR archive.
     *
     * @param resource the resource.
     * @param inputStream the input stream.
     */
    public FileEntry(@Nonnull FileResource resource, @Nonnull InputStream inputStream) {
        this.resource = resource;
        this.inputStream = inputStream;

        synchronise();
    }

    /**
     * {@inheritDoc}
     */
    public boolean isSynchronizationEnabled() {
        return file != null && file.exists() && resource.monitor();
    }

    /**
     * {@inheritDoc}
     */
    public boolean synchronise() {
        if (times[0] == -1 || (resource.monitor() && isModified())) {
            if (file != null) {
                log.log(Level.INFO, "Property file '" + file.getAbsolutePath() + "', has changed, reloading resources.");

                try {
                    load(file);
                    this.times = getTimes();

                    return true;
                } catch (IOException e) {
                    log.log(Level.SEVERE, "Failed to reload properties from file '" + file.getAbsolutePath() + "', remove file from watch list.");
                }
            } else if (inputStream != null) {
                log.log(Level.INFO, String.format("The resource %s has monitoring enabled, however the resource does not result in a file, monitoring will be disabled.", resource.file()));

                load(inputStream);
                this.times = getTimes();
                return true;
            }
        }

        return false;
    }

    /**
     * Flag indicating if the underlying file has been changed since the last time we read it.
     * @return true if it has changed.
     */
    private boolean isModified() {
        long[] newTimes = getTimes();
        return times[0] != newTimes[0] || times[1] != newTimes[1];
    }

    /**
     * Returns an array containing the creation and last modified time.
     * @return array with two number of milliseconds since epoc.
     */
    private long[] getTimes() {
        try {
            if (file != null && file.exists()) {
                BasicFileAttributes attributes = Files.readAttributes(file.toPath(), BasicFileAttributes.class);

                return new long[]{
                        attributes.creationTime().toMillis(),
                        attributes.lastModifiedTime().toMillis()
                };
            } else if (inputStream != null) {
                // ignore, cannot reload an input stream resource.
                return new long[] {0, 0};
            } else {
                log.log(Level.SEVERE, "The file '" + file.getAbsolutePath() + "', doesn't exist anymore since it was loaded.");
            }
        } catch (IOException e) {
            log.log(Level.SEVERE, "Error reading attributes for file '" + file.getAbsolutePath() + "'.");
        }

        return times;
    }
}