package nl.sodeso.commons.properties.containers.memory;

import nl.sodeso.commons.properties.AbstractContainer;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * The MemoryContainer is used to load various property values that are created in-memory.
 *
 * @author Ronald Mathies
 */
public class MemoryContainer extends AbstractContainer<MemoryEntry> {

    private String domain = null;

    public MemoryContainer(@Nonnull String domain, @Nonnull Map<String, String> properties) {
        super();
        this.domain = domain;
        this.addSource(new MemoryEntry(properties));
    }

    /**
     * {@inheritDoc}
     */
    public String getDomain() {
        return domain;
    }

}
