package nl.sodeso.commons.properties;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * An <code>Iterator</code> implementation which can iterate over various other iterators in sequence.
 *
 * @author Ronald Mathies
 */
public class IteratorSequence<T> implements Iterator<T> {

    private List<Iterator<T>> iterators = new ArrayList<>();
    private int current = 0;

    /**
     * Constructs a new empty sequence iterator.
     */
    public IteratorSequence() {
    }

    /**
     * Construct a new iterator sequence which includes all the other iterators.
     * @param iterators the iterators to add.
     */
    @SafeVarargs
    public IteratorSequence(@Nonnull Iterator<T> ... iterators) {
        this.iterators.addAll(Arrays.asList(iterators));
    }

    /**
     * Adds an additional iterator to the existing list of iterators to
     * loop through.
     *
     * @param iterator
     */
    public void addIterator(@Nonnull Iterator<T> iterator) {
        this.iterators.add(iterator);
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasNext() {
        while ( current < iterators.size() && !iterators.get(current).hasNext() ) {
            current++;
        }

        return current < iterators.size();
    }

    /**
     * {@inheritDoc}
     */
    public T next() {
        while ( current < iterators.size() && !iterators.get(current).hasNext() ) {
            current++;
        }

        return iterators.get(current).next();
    }

}
