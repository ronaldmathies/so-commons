package nl.sodeso.commons.properties.containers.system;

import nl.sodeso.commons.properties.AbstractContainer;
import nl.sodeso.commons.properties.annotations.SystemResource;

/**
 * The FileContainer is used to load various property files from disk, it supports
 * monitoring of these files for changes.
 *
 * @author Ronald Mathies
 */
public class SystemContainer extends AbstractContainer<SystemEntry> {

    public SystemContainer() {
        super();

        SystemResource resource = getClass().getAnnotation(SystemResource.class);
        if (resource != null) {
            addSource(new SystemEntry(resource));
        }
    }

}
