package nl.sodeso.commons.properties.events;

import javax.annotation.Nonnull;

/**
 * Implement this handler to receive events when a property domain has changed
 * properties.
 *
 * @author Ronald Mathies
 */
public interface PropertyDomainChangedHandler {

    void event(@Nonnull PropertyDomainChangedEvent event);

}
