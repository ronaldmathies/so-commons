package nl.sodeso.commons.properties.annotations;

import java.lang.annotation.*;

/**
 * @author Ronald Mathies
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SystemResource {

}
