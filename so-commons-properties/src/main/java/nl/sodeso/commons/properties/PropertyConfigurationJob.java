package nl.sodeso.commons.properties;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Job that will call all known resources to reload their resources when necessary.
 *
 * @author Ronald Mathies
 */
public class PropertyConfigurationJob implements Job {

    private static final Logger log = Logger.getLogger(PropertyConfigurationJob.class.getName());

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.log(Level.INFO, "Starting synchronisation for all property sources.");

        PropertyConfiguration.getInstance().synchronise();

        log.log(Level.INFO, "Finished synchronisation for all property sources.");
    }
}
