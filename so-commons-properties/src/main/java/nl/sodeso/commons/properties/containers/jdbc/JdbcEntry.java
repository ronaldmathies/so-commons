package nl.sodeso.commons.properties.containers.jdbc;

import nl.sodeso.commons.properties.AbstractEntry;
import nl.sodeso.commons.properties.annotations.JdbcResource;

import javax.annotation.Nonnull;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class JdbcEntry extends AbstractEntry {

    private static final Logger log = Logger.getLogger(JdbcEntry.class.getName());

    private static final String SQL_QUERY_SELECT_PROPERTIES = "SELECT PROP_KEY, PROP_VALUE FROM %sPROPERTIES";
    private static final String SQL_QUERY_VERSION = "SELECT MAX(PROP_VERSION) FROM %sPROPERTIES";
    private static final String SQL_COLUMN_KEY = "PROP_KEY";
    private static final String SQL_COLUMN_VALUE = "PROP_VALUE";
    private static final String SQL_COLUMN_VERSION = "PROP_VERSION";

    private JdbcResource resource;
    private long highestVersion = -1;

    /**
     * Constructs a new object with the specified file.
     */
    public JdbcEntry(@Nonnull JdbcResource resource) {
        this.resource = resource;

        synchronise();
    }

    /**
     * {@inheritDoc}
     */
    public boolean isSynchronizationEnabled() {
        return resource.monitor();
    }

    /**
     * {@inheritDoc}
     */
    public boolean synchronise() {
        if (!resource.monitor() || highestVersion != -1) {
            return false;
        }

        Connection connection = getConnection();
        if (connection != null) {

            long newHighestVersion = getHighestVersion(connection);

            if (newHighestVersion != highestVersion) {
                log.log(Level.INFO, "JDBC Resource '" + resource.jndi() + "', has changed, reloading resources.");

                String query = String.format(SQL_QUERY_SELECT_PROPERTIES, resource.prefix());
                try (Statement statement = connection.createStatement(); ResultSet resultSet = statement.executeQuery(query)) {

                    Map<String, String> properties = new HashMap<>();
                    while (resultSet.next()) {
                        String key = resultSet.getString(SQL_COLUMN_KEY);
                        String value = resultSet.getString(SQL_COLUMN_VALUE);

                        properties.put(key, value);
                    }
                    load(properties);

                    return true;
                } catch (SQLException e) {
                    log.log(Level.SEVERE, "Failed to load properties from JDBC datasource with JNDI name '" + resource.jndi() + "'.", e);
                }
            }
        }

        return false;
    }

    /**
     * Returns an array containing the creation and last modified time.
     * @return array with two number of milliseconds since epoc.
     */
    private long getHighestVersion(Connection connection) {
        String query = String.format(SQL_QUERY_VERSION, resource.prefix());
        try (Statement statement = connection.createStatement(); ResultSet resultSet = statement.executeQuery(query)) {
            if (resultSet.next()) {
                return resultSet.getLong(1);
            }

        } catch (SQLException e) {
            log.log(Level.SEVERE, "Error retrieving highest version from JDBC datasource with JNDI name '" + resource.jndi() + "'.", e);
        }

        return -1;
    }

    private Connection getConnection() {
        try {
            Context context = new InitialContext();

            DataSource dataSource = (DataSource)context.lookup(resource.jndi());
            if (dataSource != null) {
                return dataSource.getConnection();
            } else {
                log.log(Level.SEVERE, "Failed to retrieve a datasource from context using JNDI name '" + resource.jndi() + "'.");
            }
        } catch (NamingException e) {
            log.log(Level.SEVERE, "Failed to retrieve a datasource from context using JNDI name '" + resource.jndi() + "'.", e);
        } catch (SQLException e) {
            log.log(Level.SEVERE, "SQL Error occured during connection creation.", e);
        }

        return null;
    }
}
