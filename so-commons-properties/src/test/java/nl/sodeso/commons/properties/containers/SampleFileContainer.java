package nl.sodeso.commons.properties.containers;

import nl.sodeso.commons.properties.annotations.FileResource;
import nl.sodeso.commons.properties.annotations.FileResources;
import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.containers.file.FileContainer;

/**
 * @author Ronald Mathies
 */
@Resource(
    domain = SampleFileContainer.DOMAIN
)
@FileResources(
    files = {
            @FileResource(
                    file= "/domain1.properties",
                    monitor=true
            ),
            @FileResource(
                    file= "/domain2.properties",
                    monitor=true
            )
    }
)
public class SampleFileContainer extends FileContainer {

    public static final String DOMAIN = "domain1";
    public static final String PROPKEY1 = "propkey1";
    public static final String PROPKEY2 = "propkey2";
    public static final String PROPKEYINT = "propkeyInt";
    public static final String PROPKEYLONG = "propkeyLong";
    public static final String PROPKEYBOOLEAN = "propkeyBoolean";
}
