package nl.sodeso.commons.properties.containers;

import nl.sodeso.commons.properties.annotations.JdbcResource;
import nl.sodeso.commons.properties.annotations.JdbcResources;
import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.containers.jdbc.JdbcContainer;

/**
 * @author Ronald Mathies
 */
@Resource(
    domain = "domain2"
)
@JdbcResources(
    jdbc = {
            @JdbcResource(
                    jndi="jdbc/datasource",
                    prefix="PREF_",
                    monitor=true
            )
    }
)
public class SampleJdbcContainer extends JdbcContainer {

    public static final String DOMAIN = "domain2";
    public static final String DBKEY1 = "DBKEY1";
}
