package nl.sodeso.commons.properties;

import nl.sodeso.commons.properties.containers.SampleFileContainer;
import nl.sodeso.commons.properties.containers.SampleJdbcContainer;
import nl.sodeso.commons.properties.containers.SampleSystemContainer;
import nl.sodeso.commons.properties.containers.memory.MemoryContainer;
import nl.sodeso.persistence.jdbc.SqlScriptExecutor;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Ronald Mathies
 */
public class ConfigurationPropertiesTest {

    private final static long HALF_MINUTE_IN_MILLIS = 30L * 100L;

    private DataSource dataSource = null;

    @Before
    public void before() throws NamingException, SQLException {

        System.setProperty("SYSPROP1", "SYSVALUE1");

        execute(connection -> {
            try {
                SqlScriptExecutor.execute(connection, "datasource.sql");
            } catch (IOException e) {
                assertTrue("Should not be hapening.", false);
            }
        });

    }

    public void execute(Work work) throws NamingException, SQLException {
        if (dataSource == null) {
            InitialContext context = new InitialContext();
            dataSource = (DataSource) context.lookup("jdbc/datasource");
        }

        try (Connection connection = dataSource.getConnection()) {
            work.execute(connection);
        }
    }

    public interface Work {

        void execute(Connection connection);

    }

    @Test
    public void checkSources() throws InterruptedException, IOException, URISyntaxException, NamingException, SQLException {
        PropertyConfiguration propertyConfiguration = PropertyConfiguration.getInstance();

        Map<String, String> memoryContainerValues = new HashMap<>();
        memoryContainerValues.put("key1", "value1");
        propertyConfiguration.addContainer(new MemoryContainer("memory", memoryContainerValues));

        String result = propertyConfiguration.getStringProperty("memory", "key1");
        assertEquals("Results are not equal.", "value1", result);

        result = propertyConfiguration.getStringProperty(SampleFileContainer.DOMAIN, SampleFileContainer.PROPKEY1);
        assertEquals("Results are not equal.", "valuekey1", result);
        result = propertyConfiguration.getStringProperty(SampleFileContainer.DOMAIN, SampleFileContainer.PROPKEY2);
        assertEquals("Results are not equal.", "valuekey2", result);
        Integer resultInt = propertyConfiguration.getIntegerProperty(SampleFileContainer.DOMAIN, SampleFileContainer.PROPKEYINT, 20);
        assertEquals("Results are not equal.", 10, resultInt.intValue());
        Long resultLong = propertyConfiguration.getLongProperty(SampleFileContainer.DOMAIN, SampleFileContainer.PROPKEYLONG, 30L);
        assertEquals("Results are not equal.", 20, resultLong.longValue());
        Boolean resultBool = propertyConfiguration.getBooleanProperty(SampleFileContainer.DOMAIN, SampleFileContainer.PROPKEYBOOLEAN);
        assertEquals("Results are not equal.", true, resultBool);

//        result = propertyConfiguration.getStringProperty(SampleJdbcContainer.DOMAIN, SampleJdbcContainer.DBKEY1);
//        assertEquals("Results are not equal.", "DBVALUE1", result);

        //result = PropertyConfiguration.getInstance().getStringProperty(SampleSystemContainer.DOMAIN, SampleSystemContainer.SYSPROP1, null);
        //assertEquals("Results are not equal.", "SYSVALUE1", result);

        // Update a properties file.
        Properties properties = new Properties();
        InputStream resourceAsStream = ConfigurationPropertiesTest.class.getResourceAsStream("/domain2.properties");
        properties.load(resourceAsStream);
        properties.put("propkey3", "valuekey3");
        properties.store(new FileOutputStream(new File(ConfigurationPropertiesTest.class.getResource("/domain2.properties").toURI())), null);

        // Update the database.
        execute(connection -> {
            try {
                SqlScriptExecutor.execute(connection, "datasource-update.sql");
            } catch (IOException e) {
                assertTrue("Should not be hapening.", false);
            }
        });


        Thread.sleep(HALF_MINUTE_IN_MILLIS * 3);

        result = propertyConfiguration.getStringProperty(SampleFileContainer.DOMAIN, "propkey3");
        assertEquals("Results are not equal.", "valuekey3", result);

//        result = propertyConfiguration.getStringProperty(SampleJdbcContainer.DOMAIN, SampleJdbcContainer.DBKEY1);
//        assertEquals("Results are not equal.", "DBNEWVALUE2", result);

        result = propertyConfiguration.getStringProperty(SampleSystemContainer.DOMAIN, SampleSystemContainer.SYSPROP1);
        assertEquals("Results are not equal.", "SYSVALUE1", result);
    }

    @Test
    public void checkValueIfAbsent() {
        String resultString = PropertyConfiguration.getInstance().getStringProperty(SampleFileContainer.DOMAIN, "non-existent-property", "default");
        assertEquals("Results are not equal.", "default", resultString);
        Integer resultInt = PropertyConfiguration.getInstance().getIntegerProperty(SampleFileContainer.DOMAIN, "non-existent-property", 110);
        assertEquals("Results are not equal.", 110, resultInt.intValue());
        Long resultLong = PropertyConfiguration.getInstance().getLongProperty(SampleFileContainer.DOMAIN, "non-existent-property", 120L);
        assertEquals("Results are not equal.", 120, resultLong.longValue());
        Boolean resultBool = PropertyConfiguration.getInstance().getBooleanProperty(SampleFileContainer.DOMAIN, "non-existent-property", true);
        assertEquals("Results are not equal.", true, resultBool);
    }

    @Test
    public void checkKeys() {
        Iterator<String> keys = PropertyConfiguration.getInstance().getKeys(SampleFileContainer.DOMAIN);

        int count = 0;
        while (keys.hasNext()) {
            switch (keys.next()) {
                case "propkey1":
                case "propkeyLong":
                case "propkeyInt":
                case "propkeyBoolean":
                case "propkey2":
                    count++;
            }
        }

        assertEquals("Should result in 5 keys", 5, count);
    }

}
