package nl.sodeso.commons.properties.containers;

import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.annotations.EnvironmentResource;
import nl.sodeso.commons.properties.containers.environment.EnvironmentContainer;

/**
 * @author Ronald Mathies
 */
@Resource(
        domain = SampleEnvironmentContainer.DOMAIN
)
@EnvironmentResource
public class SampleEnvironmentContainer extends EnvironmentContainer {

    public static final String DOMAIN = "domain3";
    public static final String SYSPROP1 = "SYSPROP1";

}
