package nl.sodeso.commons.properties.containers;

import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.annotations.SystemResource;
import nl.sodeso.commons.properties.containers.system.SystemContainer;

/**
 * @author Ronald Mathies
 */
@Resource(
        domain = SampleSystemContainer.DOMAIN
)
@SystemResource
public class SampleSystemContainer extends SystemContainer {

    public static final String DOMAIN = "domain4";
    public static final String SYSPROP1 = "SYSPROP1";

}
