package nl.sodeso.commons.ldap;

import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.annotations.SystemResource;
import nl.sodeso.commons.properties.containers.system.SystemContainer;

/**
 * @author Ronald Mathies
 */
@Resource(
    domain = LdapProperties.DOMAIN
)
@SystemResource
public class LdapProperties extends SystemContainer {

    public static final String DOMAIN = "ldap";
}
