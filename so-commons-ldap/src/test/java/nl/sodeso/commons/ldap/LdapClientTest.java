package nl.sodeso.commons.ldap;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.listener.InMemoryListenerConfig;
import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldif.LDIFChangeRecord;
import com.unboundid.ldif.LDIFException;
import com.unboundid.ldif.LDIFReader;
import nl.sodeso.commons.ldap.exception.*;
import nl.sodeso.commons.ldap.mapping.LdapMapper;
import nl.sodeso.commons.ldap.utils.LdapDebug;
import org.hamcrest.core.IsEqual;
import org.junit.*;

import javax.naming.directory.SearchResult;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * For more documentation on the UnboundID LDAP SDK please see the following
 * page:
 *
 * https://www.ldap.com/unboundid-ldap-sdk-for-java
 *
 * Altough the UnboundID LDAP SDK provides a fully in-memory LDAP server it does have
 * some quircks that may need some attention when odd behavior occurs, see the
 * following page for more information:
 *
 * https://docs.ldap.com/ldap-sdk/docs/in-memory-directory-server.html
 *
 * @author Ronald Mathies
 */
public class LdapClientTest {

    private static final int DEFAULT_PORT = 10389;
    private static final String PRINCIPAL = "cn=admin";
    private static final String CREDENTIALS = "secret";
    private static final String HOST = "127.0.0.1";

    private static final String LDAP_UNIT_NAME = "ldap";

    private static final String ROOT_BASE_DN = "dc=nl";

    private static InMemoryDirectoryServer server = null;

    private LdapClient client = null;

    @BeforeClass
    public static void beforeClass() {
        try {
            InMemoryListenerConfig listenerConfig =
                    InMemoryListenerConfig.createLDAPConfig("default", DEFAULT_PORT);

            InMemoryDirectoryServerConfig serverConfig =
                    new InMemoryDirectoryServerConfig(new DN(ROOT_BASE_DN));
            serverConfig.setListenerConfigs(listenerConfig);
            serverConfig.addAdditionalBindCredentials(PRINCIPAL, CREDENTIALS);

            server = new InMemoryDirectoryServer(serverConfig);

            // Load the LDIF file.
            InputStream ldifResourceAsStream = server.getClass().getResourceAsStream("/accounts.ldif");
            LDIFReader reader = new LDIFReader(ldifResourceAsStream);
            LDIFChangeRecord record;
            while ((record = reader.readChangeRecord()) != null) {
                record.processChange(server);
            }


            // Setup the configuration for the LdapClient.
            System.setProperty(LdapClient.KEY_JNDI_UNIT, LDAP_UNIT_NAME);
            System.setProperty(LdapClient.KEY_HOST, HOST);
            System.setProperty(LdapClient.KEY_PORT, String.valueOf(DEFAULT_PORT));
            System.setProperty(LdapClient.KEY_PRINCIPAL, PRINCIPAL);
            System.setProperty(LdapClient.KEY_CREDENTIALS, CREDENTIALS);

            // Start the server.
            server.startListening();
        } catch (LDAPException | LDIFException | IOException e) {
            assertNull(e);
        }
    }

    @AfterClass
    public static void afterClass() {
        if (server != null) {
            server.shutDown(true);
        }
    }

    @Before
    public void before() {
        try {
            client = new LdapClient(LDAP_UNIT_NAME);
            client.connect();
        } catch (LdapConnectException e) {
            assertNull(e);
        }
    }

    @After
    public void after() {
        try {
            client.disconnect();
        } catch (LdapDisconnectException e) {
            assertNull(e);
        }
    }

    @Test
    public void search() {
        try {
            ArrayList<SearchResult> result =
                    client.search("ou=users,dc=sodeso,dc=nl", "uid=*", Scope.ONELEVEL_SCOPE);

            LdapDebug.debug(result);

            assertEquals("Should result in one entry.", 1, result.size());

            User user = LdapMapper.map(result.get(0), User.class);
            assertEquals(user.getUsername(), "admin");
            Assert.assertThat(user.getPassword(), IsEqual.equalTo("secret".getBytes()));

        } catch (LdapSearchException | LdapNotConnectedException | LdapMappingException e) {
            assertNull(e);
        }
    }


}