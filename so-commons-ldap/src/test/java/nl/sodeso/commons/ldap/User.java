package nl.sodeso.commons.ldap;

import nl.sodeso.commons.ldap.metadata.annotations.LdapField;

/**
 * @author Ronald Mathies
 */
public class User {

    @LdapField(id = "uid")
    private String username;

    @LdapField(id = "userPassword")
    private byte[] password;

    public User() {}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public byte[] getPassword() {
        return password;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }
}
