package nl.sodeso.commons.ldap.exception;

/**
 * This exception could be thrown during the disconnecting phase.
 *
 * @author Ronald Mathies
 */
public class LdapDisconnectException extends Exception {

    /**
     * {@inheritDoc}
     */
    public LdapDisconnectException(Throwable cause) {
        super("Failed to disconnect from LDAP server.", cause);
    }
}
