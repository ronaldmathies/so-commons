package nl.sodeso.commons.ldap.exception;

/**
 * This exception can be thrown during the connecting phase, when
 * this exception occurs it could be that the settings are not correct
 * or that the connection itself failed.
 *
 * @author Ronald Mathies
 */
public class LdapConnectException extends Exception {

    /**
     * {@inheritDoc}
     */
    public LdapConnectException(String message) {
        super(message);
    }

    /**
     * {@inheritDoc}
     */
    public LdapConnectException(Throwable cause) {
        super(cause);
    }

    /**
     * {@inheritDoc}
     */
    public LdapConnectException(String message, Throwable cause) {
        super(message, cause);
    }
}
