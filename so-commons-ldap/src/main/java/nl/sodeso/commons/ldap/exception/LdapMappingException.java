package nl.sodeso.commons.ldap.exception;

/**
 * This exception is thrown when the mapping of a result from LDAP to an object or
 * vice versa failed.
 *
 * @author Ronald Mathies
 */
public class LdapMappingException extends Exception {

    /**
     * {@inheritDoc}
     */
    public LdapMappingException(Throwable cause) {
        super("Failed to map SearchResult to Object.", cause);
    }
}
