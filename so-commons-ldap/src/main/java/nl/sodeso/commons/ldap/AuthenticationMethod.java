package nl.sodeso.commons.ldap;

/**
 * @author Ronald Mathies
 */
public enum AuthenticationMethod {

    SIMPLE("simple");

    private String method;

    AuthenticationMethod(String method) {
        this.method = method;
    }

    public String getMethod() {
        return this.method;
    }

    public AuthenticationMethod from(String method) {
        switch (method) {
            case "simple":
                return SIMPLE;
        }

        return null;
    }

}
