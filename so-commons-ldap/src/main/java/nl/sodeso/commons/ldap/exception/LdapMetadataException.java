package nl.sodeso.commons.ldap.exception;

/**
 * This exception is thrown when the metadata from a class could not be collected.
 *
 * @author Ronald Mathies
 */
public class LdapMetadataException extends Exception {

    /**
     * {@inheritDoc}
     */
    public LdapMetadataException(String message, Throwable cause) {
        super(message, cause);
    }
}
