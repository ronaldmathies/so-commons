package nl.sodeso.commons.ldap.exception;

/**
 * This exception can be thrown when a search was to be performed but it failed.
 *
 * @author Ronald Mathies
 */
public class LdapSearchException extends Exception {

    /**
     * {@inheritDoc}
     */
    public LdapSearchException(String message, Throwable cause) {
        super(message, cause);
    }
}
