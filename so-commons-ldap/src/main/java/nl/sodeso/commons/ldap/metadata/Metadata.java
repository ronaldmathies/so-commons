package nl.sodeso.commons.ldap.metadata;

import nl.sodeso.commons.ldap.exception.LdapMetadataException;
import nl.sodeso.commons.ldap.metadata.annotations.LdapField;

import javax.annotation.Nonnull;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * A metadata model object containing the association between defined fields in the Java class
 * and the attribute ids in the LDAP server.
 *
 * @author Ronald Mathies
 */
public class Metadata {

    private Class clazz = null;
    private Map<String, Field> fields = new HashMap<>();

    /**
     * Constructs a new Metadata object and directly resolve all associations between the defined fields in the
     * Java class and the attribute ids in the LDAP server.
     *
     * @param aClass the class to resolve the fields and attribute ids.
     */
    protected Metadata(@Nonnull Class aClass) {
        this.clazz = aClass;

        Field[] declaredFields = aClass.getDeclaredFields();
        for (Field declaredField : declaredFields) {

            if (declaredField.isAnnotationPresent(LdapField.class)) {
                LdapField ldapField = declaredField.getAnnotation(LdapField.class);
                fields.put(ldapField.id(), declaredField);
            }

        }

    }

    /**
     * Constructs a new instance of the Java class.
     * @param <T> the instantiated Java class.
     *
     * @return the instantiated Java class.
     *
     * @throws LdapMetadataException when an error occured while instantiating a new instance of the Java class.
     */
    @SuppressWarnings("unchecked")
    public <T> T createInstance() throws LdapMetadataException {
        try {
            return (T)clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new LdapMetadataException("Failed to instantiate class.", e);
        }
    }

    /**
     * Returns a <code>Field</code> definition associated with the LDAP attribute id.
     * @param id the LDAP attribute id.
     * @return the Java <code>Field</code>.
     */
    public Field fieldForId(String id) {
        return fields.get(id);
    }

}
