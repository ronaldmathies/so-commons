package nl.sodeso.commons.ldap.mapping;

import nl.sodeso.commons.ldap.exception.LdapMetadataException;
import nl.sodeso.commons.ldap.metadata.Metadata;
import nl.sodeso.commons.ldap.metadata.MetadataFactory;
import nl.sodeso.commons.ldap.exception.LdapMappingException;
import nl.sodeso.commons.ldap.utils.LdapUtil;

import javax.annotation.Nonnull;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.SearchResult;
import java.lang.reflect.Field;

/**
 * @author Ronald Mathies
 */
public class LdapMapper {

    /**
     * Maps the data from a <code>SearchResult</code> to a new instance of the specified class.
     *
     * @param from the search results containing the data to be mapped.
     * @param to the class that will be instantiated and filled with the data from the search result.
     * @param <T> the class type.
     *
     * @return an instantiated class pre-filled with the data from the search result.
     *
     * @throws LdapMappingException When the mapping failed.
     */
    public static <T> T map(@Nonnull SearchResult from, @Nonnull Class<T> to) throws LdapMappingException {
        try {
            Metadata metadata = MetadataFactory.instance().forClass(to);
            T object = metadata.createInstance();

            NamingEnumeration<? extends Attribute> allAttributes = from.getAttributes().getAll();
            while (allAttributes.hasMoreElements()) {
                Attribute attribute = allAttributes.nextElement();
                String attributeId = attribute.getID();

                setValueForId(object, attributeId, LdapUtil.getAttributeValue(from, attributeId));
            }


            return object;
        } catch (LdapMetadataException e) {
            throw new LdapMappingException(e);
        }
    }

    /**
     * Sets the <code>value</code> for the field with the associated attributeId.
     *
     * @param object the object that contains a field with the correct annotation matching the attributeId.
     * @param attributeId the id to look for within the <code>object</code>.
     * @param value the value to set.
     *
     * @throws LdapMappingException when the mapping failed, not thrown when the associated field could not be found.
     */
    public static void setValueForId(@Nonnull Object object, @Nonnull String attributeId, Object value) throws LdapMappingException {
        try {
            Metadata metadata = MetadataFactory.instance().forClass(object.getClass());

            Field fieldToSet = metadata.fieldForId(attributeId);
            if (fieldToSet != null) {

                // Check if the field is accessible, if not make it so.
                if (!fieldToSet.isAccessible()) {
                    fieldToSet.setAccessible(true);
                }

                fieldToSet.set(object, value);
            }
        } catch (IllegalAccessException e) {
            throw new LdapMappingException(e);
        }
    }

}
