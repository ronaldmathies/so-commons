package nl.sodeso.commons.ldap.utils;

import javax.annotation.Nonnull;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.SearchResult;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class LdapUtil {

    /**
     * Returns the value of an attribute associated with the <code>attributeId</code>.
     *
     * @param result the search result.
     * @param attributeId the id of the attribute within the search result.
     *
     * @return the value.
     */
    @SuppressWarnings("unchecked")
    public static <X> X getAttributeValue(@Nonnull SearchResult result, @Nonnull String attributeId) {
        X value = null;
        try {
            NamingEnumeration<? extends Attribute> allAttributes = result.getAttributes().getAll();
            while (allAttributes.hasMoreElements()) {
                Attribute attribute = allAttributes.nextElement();
                if (attribute.getID().equals(attributeId)) {
                    value = (X)attribute.get(0);
                    break;
                }
            }
        } catch (NamingException e) {
            // Do nothing.
        }

        return value;
    }

    /**
     * Returns all the values of an attribute associated with the <code>attributeId</code>.
     *
     * @param result the search result.
     * @param attributeId the id of the attribute within the search result.
     *
     * @return the list of values.
     */
    public static List<Object> getAllAttributeValues(@Nonnull SearchResult result, @Nonnull String attributeId) {
        List<Object> values = new ArrayList<>();
        try {
            NamingEnumeration<? extends Attribute> allAttributes = result.getAttributes().getAll();
            while (allAttributes.hasMoreElements()) {
                Attribute attribute = allAttributes.nextElement();
                if (attribute.getID().equals(attributeId)) {


                    NamingEnumeration<?> allAttributeValues = attribute.getAll();
                    while (allAttributeValues.hasMoreElements()) {
                        Object object = allAttributeValues.nextElement();
                        values.add(object);
                    }
                }
            }
        } catch (NamingException e) {
            // Do nothing.
        }

        return values;
    }

}
