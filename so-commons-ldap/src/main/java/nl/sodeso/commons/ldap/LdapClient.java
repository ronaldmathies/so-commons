package nl.sodeso.commons.ldap;

import nl.sodeso.commons.ldap.exception.LdapConnectException;
import nl.sodeso.commons.ldap.exception.LdapDisconnectException;
import nl.sodeso.commons.ldap.exception.LdapNotConnectedException;
import nl.sodeso.commons.ldap.exception.LdapSearchException;
import nl.sodeso.commons.properties.PropertyConfiguration;

import javax.annotation.Nonnull;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchResult;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class LdapClient {

    private static final Logger LOG = Logger.getLogger(LdapClient.class.getName());

    public static final String KEY_JNDI_UNIT = "so.ldap.unit.name";
    public static final String KEY_PRINCIPAL = "ldap.principal";
    public static final String KEY_CREDENTIALS = "ldap.credentials";
    public static final String KEY_HOST = "ldap.host";
    public static final String KEY_PORT = "ldap.port";

    private DirContext ctx = null;

    private Properties configuration = null;

    public LdapClient(Properties configuration) {
        this.configuration = configuration;
    }

    public LdapClient(String ldapUnitName) {
        loadPropertiesUsingLdapUnitName(ldapUnitName);
    }

    /**
     * Connects to the LDAP server with the supplied configuration that is looked up
     * using the <code>ldapUnitName</code>.
     *
     * @throws LdapConnectException when the client failed to connect to the LDAP server.
     */
    public void connect() throws LdapConnectException {
        try {
            LOG.log(Level.INFO, "Connecting to LDAP server.");

            ctx = new InitialDirContext(getEnv());

            LOG.log(Level.INFO, "Connected to LDAP server.");
        } catch (NamingException e) {
            LOG.log(Level.SEVERE, "Failed to connect to LDAP server", e);

            throw new LdapConnectException(e);
        }
    }

    /**
     * Flag indicating if we have a connection with the LDAP server.
     * @return true if we have a connection, false if not.
     */
    public boolean isConnected() {
        return ctx != null;
    }

    public ArrayList<SearchResult> search(String name, String filter, Scope scope) throws LdapSearchException, LdapNotConnectedException {
        try {

            if (isConnected()) {

                NamingEnumeration<SearchResult> result = ctx.search(name, filter, scope.getSearchControls());
                return Collections.list(result);

            } else {
                throw new LdapNotConnectedException();
            }

        } catch (NamingException e) {
            throw new LdapSearchException(
                    String.format("Failed to perform search on '%s', using filter '%s'", name, filter), e);
        }
    }

    /**
     * Disconnects from the LDAP server (if no connection was made then it just passes through).
     *
     * @throws LdapDisconnectException when the client failed to disconnect from the LDAP server.
     */
    public void disconnect() throws LdapDisconnectException {
        try {
            LOG.log(Level.INFO, "Disconnecting from LDAP server.");

            if (ctx != null) {
                ctx.close();
            }

            LOG.log(Level.INFO, "Disconnected from LDAP server.");
        } catch (NamingException e) {
            LOG.log(Level.SEVERE, "Failed to disconnect from LDAP server", e);

            throw new LdapDisconnectException(e);
        } finally {
            ctx = null;
        }
    }

    /**
     * Loads the LDAP configuration associated with the LDAP unit name.
     * @param ldapUnitName the LDAP unit name.
     */
    private void loadPropertiesUsingLdapUnitName(@Nonnull String ldapUnitName) {
        this.configuration = new Properties();

        PropertyConfiguration instance = PropertyConfiguration.getInstance();
        String domain = instance.getPropertyContainerDomainBy(KEY_JNDI_UNIT, ldapUnitName);

        configuration.put(KEY_HOST, instance.getStringProperty(domain, KEY_HOST));
        configuration.put(KEY_PORT, instance.getStringProperty(domain, KEY_PORT));
        configuration.put(KEY_PRINCIPAL, instance.getStringProperty(domain, KEY_PRINCIPAL));
        configuration.put(KEY_CREDENTIALS, instance.getStringProperty(domain, KEY_CREDENTIALS));
    }

    /**
     * Constructs the LDAP initial context configuration.
     *
     * @return the LDAP initial context configuration.
     *
     * @throws LdapConnectException thrown when certain properties are not present or have no value.
     */
    private Hashtable<String, String> getEnv() throws LdapConnectException {
        Hashtable<String, String> env = new Hashtable<>();

        String host = configuration.getProperty(KEY_HOST);
        if (host == null || host.isEmpty()) {
            throw new LdapConnectException(String.format("No property or value specified for key '%s'.", KEY_HOST));
        }

        Integer port = Integer.valueOf(configuration.getProperty(KEY_PORT));
        if (port == null || port == 0) {
            throw new LdapConnectException(String.format("No property or (invalid) value specified for key '%s'.", KEY_PORT));
        }

        env.put(Context.PROVIDER_URL, "ldap://" + host + ":" + port);

        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.SECURITY_AUTHENTICATION, AuthenticationMethod.SIMPLE.getMethod());

        String principal = configuration.getProperty(KEY_PRINCIPAL);
        if (principal != null && !principal.isEmpty()) {
            env.put(Context.SECURITY_PRINCIPAL, principal);
        }

        String credentials = configuration.getProperty(KEY_CREDENTIALS);
        if (principal != null && !principal.isEmpty()) {
            env.put(Context.SECURITY_CREDENTIALS, credentials);
        }

        return env;
    }

}
