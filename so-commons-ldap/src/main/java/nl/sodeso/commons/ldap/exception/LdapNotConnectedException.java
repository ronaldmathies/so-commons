package nl.sodeso.commons.ldap.exception;

/**
 * This exception is thrown when an action is performed that requires a connection
 * with an LDAP server but no connection was made.
 *
 * @author Ronald Mathies
 */
public class LdapNotConnectedException extends Exception {

    /**
     * {@inheritDoc}
     */
    public LdapNotConnectedException() {
        super("Cannot perform action, no connection or connection was closed with the LDAP server..");
    }
}