package nl.sodeso.commons.ldap.utils;

import javax.annotation.Nonnull;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchResult;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility class for logging the information of a <code>SearchResult</code> to the log file.
 *
 * @author Ronald Mathies
 */
public class LdapDebug {

    private static final Logger LOG = Logger.getLogger(LdapDebug.class.getName());

    /**
     * Prints out the search results to the logging.
     * @param results the results to print out in the logging.
     */
    public static void debug(@Nonnull List<SearchResult> results) {
        for (SearchResult result : results) {
            debug(result);
        }
    }

    /**
     * Prints out the search result to the logging.
     * @param result the result to print out in the logging.
     */
    public static void debug(@Nonnull SearchResult result) {

        try {
            String name = result.getName();
            LOG.log(Level.INFO, "Distinguished name is " + name);

            NamingEnumeration<? extends Attribute> allAttributes = result.getAttributes().getAll();
            while (allAttributes.hasMoreElements()) {
                Attribute attribute = allAttributes.nextElement();
                LOG.info(attribute.getID() + " :");

                NamingEnumeration<?> allValues = attribute.getAll();
                while (allValues.hasMoreElements()) {
                    Object value = allValues.nextElement();
                    LOG.log(Level.INFO, "\t:" + value.toString());
                }
            }

            LOG.log(Level.INFO, "\n");
        } catch (NamingException e) {
            LOG.log(Level.INFO, "Failed to print out detailed logging for search result.");
        }

    }

}
