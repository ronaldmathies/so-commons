package nl.sodeso.commons.ldap.metadata.annotations;

import java.lang.annotation.*;

/**
 * Describes a field that can be mapped from an LDAP attribute.
 *
 * @author Ronald Mathies
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LdapField {

    /**
     * The name of the attribute as specified within the LDAP structure.
     * @return the name of the attribute as specified within the LDAP structure.
     */
    String id();
}
