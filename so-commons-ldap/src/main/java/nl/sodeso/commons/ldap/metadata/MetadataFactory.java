package nl.sodeso.commons.ldap.metadata;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class MetadataFactory {

    private Map<Class, Metadata> metadataMap = new HashMap<>();

    private static MetadataFactory factory;

    /**
     * Returns the singleton instance of the factory.
     * @return the singleton instance of the factory.
     */
    public static MetadataFactory instance() {
        if (factory == null) {
            factory = new MetadataFactory();
        }

        return factory;
    }

    /**
     * Returns the cached metadata for the specified class.
     * @param clazz the class for which the metadata needs to be returned.
     * @return the metadata associated with the specified class.
     */
    public Metadata forClass(@Nonnull Class clazz) {
        if (!metadataMap.containsKey(clazz)) {
            metadataMap.put(clazz, new Metadata(clazz));
        }

        return metadataMap.get(clazz);
    }
}
