package nl.sodeso.commons.ldap;

import javax.naming.directory.SearchControls;

/**
 * Search scope for the LDAP query.
 *
 * @author Ronald Mathies
 */
public enum Scope {

    /**
     * @see SearchControls#OBJECT_SCOPE;
     */
    OBJECT_SCOPE,

    /**
     * @see SearchControls#ONELEVEL_SCOPE
     */
    ONELEVEL_SCOPE,

    /**
     * @see SearchControls#SUBTREE_SCOPE
     */
    SUBTREE_SCOPE;

    /**
     * Returns the search scope for the LDAP query.
     * @return the search scope for the LDAP query.
     */
    public SearchControls getSearchControls() {
        int scope = SearchControls.SUBTREE_SCOPE;

        switch (this) {
            case OBJECT_SCOPE:
                scope = SearchControls.OBJECT_SCOPE;
                break;
            case ONELEVEL_SCOPE:
                scope = SearchControls.ONELEVEL_SCOPE;
                break;
            case SUBTREE_SCOPE:
                scope = SearchControls.SUBTREE_SCOPE;
                break;
        }

        SearchControls controls = new SearchControls();
        controls.setSearchScope(scope);

        return controls;
     }

}
