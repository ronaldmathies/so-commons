package nl.sodeso.commons.web.classpath;

import nl.sodeso.commons.web.executor.ManagedExecutorServiceFactory;
import nl.sodeso.commons.web.initialization.WebInitContextServletContextListener;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.lang.annotation.Annotation;
import java.net.URL;
import java.util.Collection;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class ClasspathWebUtil {

    private static final Logger LOG = Logger.getLogger(WebInitContextServletContextListener.class.getName());

    private Collection<URL> urls;

    private static ClasspathWebUtil INSTANCE = null;

    public static ClasspathWebUtil instance() {
        if (INSTANCE == null) {
            INSTANCE = new ClasspathWebUtil();
        }

        return INSTANCE;
    }

    public void setUrls(Collection<URL> urls) {
        this.urls = urls;
    }

    public Collection<URL> getUrls() {
        return this.urls;
    }

    public Set<Class<?>> typesAnnotatedWith(Class<? extends Annotation> annotation) {
        return typesAnnotatedWith(annotation, new String[] {});
    }

    public Set<Class<?>> typesAnnotatedWith(Class<? extends Annotation> annotation, String ... packages) {
        LOG.log(Level.INFO, String.format("Collecting classes with annotation '%s'.", annotation.getSimpleName()));

        Collection<URL> urls = ClasspathWebUtil.instance().getUrls();
        if (urls == null) {
            urls = ClasspathHelper.forManifest();
        }

        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder()
                .setScanners(new TypeAnnotationsScanner(), new SubTypesScanner())
//                .setExecutorService(ManagedExecutorServiceFactory.instance().getService())
//                .setExecutorService(Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(),
//                        new ThreadFactoryBuilder().setDaemon(true).setNameFormat("org.reflections-%d").build()
//                ))
                .setUrls(urls)
                .forPackages(packages);

//        ReflectionsHelper.registerUrlTypes();
        Reflections reflections = new Reflections(configurationBuilder);
        Set<Class<?>> typesAnnotatedWith = reflections.getTypesAnnotatedWith(annotation);

//        ExecutorService executorService = configurationBuilder.getExecutorService();
//        if (executorService != null) {
//            executorService.shutdownNow();
//        }

        LOG.log(Level.INFO, String.format("Found %d classes with annotation '%s'.", typesAnnotatedWith.size(), annotation.getSimpleName()));

        return typesAnnotatedWith;
    }

    public <T> Set<Class<? extends T>> subTypesOf(Class<T> type) {
        return subTypesOf(type, new String[] {});
    }

    public <T> Set<Class<? extends T>> subTypesOf(Class<T> type, String ... packages) {
        LOG.log(Level.INFO, String.format("Collecting classes that are a sub-type of '%s'.", type.getSimpleName()));

        Collection<URL> urls = ClasspathWebUtil.instance().getUrls();
        if (urls == null) {
            urls = ClasspathHelper.forManifest();
        }

        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder()
                .setScanners(new TypeAnnotationsScanner(), new SubTypesScanner())
//                .setExecutorService(ManagedExecutorServiceFactory.instance().getService())
                .setUrls(urls)
                .forPackages(packages);

        Reflections reflections = new Reflections(configurationBuilder);
        Set<Class<? extends T>> subTypes = reflections.getSubTypesOf(type);

        LOG.log(Level.INFO, String.format("Found %d classes that are a sub-type of '%s'.", subTypes.size(), type.getSimpleName()));

        return subTypes;
    }

}
