package nl.sodeso.commons.web.initialization.annotation;

import java.lang.annotation.*;

/**
 * @author Ronald Mathies
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WebInitContext {

    int priority() default 0;

}
