package nl.sodeso.commons.web.executor;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Named;

/**
 * @author Ronald Mathies
 */
@Named("managedExecutorServiceFactory")
public class ManagedExecutorServiceFactory {

    @Resource(lookup="java:jboss/ee/concurrency/executor/default")
    private ManagedExecutorService managedExecutorService = null;

    @SuppressWarnings("unchecked")
    public static ManagedExecutorServiceFactory instance() {
        BeanManager bm = CDI.current().getBeanManager();
        Bean<ManagedExecutorServiceFactory> bean = (Bean<ManagedExecutorServiceFactory>) bm.getBeans(ManagedExecutorServiceFactory.class).iterator().next();
        CreationalContext<ManagedExecutorServiceFactory> ctx = bm.createCreationalContext(bean);
        ManagedExecutorServiceFactory managedExecutorServiceFactory = (ManagedExecutorServiceFactory) bm.getReference(bean, ManagedExecutorServiceFactory.class, ctx);

        return managedExecutorServiceFactory;
    }

    public ManagedExecutorService getService() {
        return this.managedExecutorService;
    }
}
