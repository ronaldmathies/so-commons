package nl.sodeso.commons.web.initialization;

import nl.sodeso.commons.web.classpath.ClasspathWebUtil;
import nl.sodeso.commons.web.initialization.annotation.WebInitContext;
import org.reflections.util.ClasspathHelper;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * @author Ronald Mathies
 */
@WebListener
public class WebInitContextServletContextListener implements ServletContextListener {

    private static final Logger log = Logger.getLogger(WebInitContextServletContextListener.class.getName());

    private List<Class<? extends AbstractWebInitContext>> classes = null;
    private List<AbstractWebInitContext> classesInstances = new ArrayList<>();

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        log.log(Level.INFO, "Collecting WEB-INF/lib libraries.");

        List<URL> urls = new ArrayList<>();
        urls.add(ClasspathHelper.forWebInfClasses(servletContextEvent.getServletContext()));
        urls.addAll(ClasspathHelper.forWebInfLib(servletContextEvent.getServletContext()));
        ClasspathWebUtil.instance().setUrls(urls);

        StringBuilder urlOverview = new StringBuilder("The following URL's were found:\n\r");
        for (URL url : ClasspathWebUtil.instance().getUrls()) {
            urlOverview.append(String.format("%s\n\r", url.toString()));
        }
        log.log(Level.INFO, urlOverview.toString());

        classes = new ArrayList<>(ClasspathWebUtil.instance().subTypesOf(AbstractWebInitContext.class, "nl.sodeso"))
                .stream().filter(aClass -> !Modifier.isAbstract(aClass.getModifiers())).collect(Collectors.toList());

        Collections.sort(classes, (o1, o2) -> {
            int o1p = findWebInitContextAnnotation(o1).priority();
            int o2p = findWebInitContextAnnotation(o2).priority();
            return (o1p < o2p) ? -1 : ((o1p == o2p) ? 0 : 1);
        });

        log.log(Level.INFO, "Executing contexts ...");
        for (Class<? extends AbstractWebInitContext> _class : classes) {

            try {
                AbstractWebInitContext abstractWebInitContext = _class.newInstance();
                classesInstances.add(abstractWebInitContext);

                log.log(Level.INFO, String.format("Initializing '%s'", _class.getSimpleName()));
                abstractWebInitContext.initialize(servletContextEvent);
            } catch (InstantiationException | IllegalAccessException e) {
                log.log(Level.INFO, String.format("Failed to instantiate '%s'", _class.getSimpleName()));
            }

        }

        log.log(Level.INFO, "Finished executing contexts ...");
    }

    @SuppressWarnings("unchecked")
    private WebInitContext findWebInitContextAnnotation(Class<? extends AbstractWebInitContext> abstractWebInitContext) {
        if (abstractWebInitContext.isAnnotationPresent(WebInitContext.class)) {
            return abstractWebInitContext.getAnnotation(WebInitContext.class);
        }

        if (abstractWebInitContext.getSuperclass() != null) {
            return findWebInitContextAnnotation((Class<AbstractWebInitContext>)abstractWebInitContext.getSuperclass());
        }

        return null;
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        log.log(Level.INFO, "Destroying contexts ...");

        for (AbstractWebInitContext context : classesInstances) {
            context.destroy(servletContextEvent);
        }

        log.log(Level.INFO, "Finished destroying contexts ...");
    }

}
