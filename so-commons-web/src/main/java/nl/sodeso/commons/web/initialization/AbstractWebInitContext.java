package nl.sodeso.commons.web.initialization;

import nl.sodeso.commons.web.initialization.annotation.WebInitContext;

import javax.servlet.ServletContextEvent;

/**
 * @author Ronald Mathies
 */
@WebInitContext(priority = 0)
public class AbstractWebInitContext {

    public void initialize(ServletContextEvent servletContextEvent) {}

    public void destroy(ServletContextEvent servletContextEvent) {}

}
