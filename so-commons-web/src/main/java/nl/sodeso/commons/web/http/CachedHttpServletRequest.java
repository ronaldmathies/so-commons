package nl.sodeso.commons.web.http;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.*;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class CachedHttpServletRequest extends HttpServletRequestWrapper {

    private static final Logger log = Logger.getLogger(CachedHttpServletRequest.class.getName());

    private static final String TEMPORARY_FILENAME_PREFIX = "TmpRequest";
    private static final int LEN_BUFFER = 32768; //32 KB


    private File m_TemporaryFile;


    public CachedHttpServletRequest(HttpServletRequest httpServletRequest, File temporaryFolder)
            throws ServletException {

        super(httpServletRequest);

        try {
            //Create a temporary file to hold the contents of the request's input stream
            m_TemporaryFile = File.createTempFile(TEMPORARY_FILENAME_PREFIX, null, temporaryFolder);

            //Copy the request body to the temporary file
            BufferedInputStream is = new BufferedInputStream(super.getInputStream());
            FileOutputStream os = new FileOutputStream(m_TemporaryFile);
            byte[] buffer = new byte[LEN_BUFFER];
            int bytesRead = is.read(buffer);
            while(bytesRead != -1) {
                os.write(buffer, 0, bytesRead);
                bytesRead = is.read(buffer);
            }
            is.close();
            os.close();
        }
        catch(Exception e) {
            throw new ServletException(e);
        }
    }


    public void cleanup() {
        if (!m_TemporaryFile.delete()) {
            log.warning(String.format("Could not delete file '%s'.", m_TemporaryFile.getName()));
        }
    }


    @Override
    public ServletInputStream getInputStream() throws IOException {
        return new CachedServletInputStream(m_TemporaryFile);
    }


    @Override
    public BufferedReader getReader() throws IOException {
        String characterEncoding = getCharacterEncoding();
        if(characterEncoding == null) {
            characterEncoding = "UTF-8";
        }
        return new BufferedReader(new InputStreamReader(getInputStream(), characterEncoding));
    }
}