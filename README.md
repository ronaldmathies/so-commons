Project: so-commons
===================

The commons project contains centralised functionality which could be used by various projects.

Modules
==================

<dl>
    <dt>so-commons-collections</dt>
    <dd>Convenience classes for handling collections.</dd>
    <dt>so-commons-elasticsearch</dt>
    <dd>Convenience classes for using elastic search, for example to integrate with Hibernate.</dd>
    <dt>so-commons-fileutils</dt>
    <dd>Convenience classes for handling files, file searches, etc...</dd>
    <dt>so-commons-freemarker</dt>
    <dd>Convenience classes for using the Freemarker templating engine.</dd>
    <dt>so-commons-hibernate</dt>
    <dd>Convenience classes for using Hibernate in projects, contains unit of work, utility method for handling Restriction.in limitations, etc...</dd>
    <dt>so-commons-hibernate-test</dt>
    <dd>Convenience classes for writing unit-tests, like truncating data in a database based on all tables within a session.</dd>
    <dt>so-commons-jdbc</dt>
    <dd>Convenience classes for </dd>
    <dt>so-commons-ldap</dt>
    <dd></dd>
    <dt>so-commons-network</dt>
    <dd></dd>
    <dt>so-commons-properties</dt>
    <dd></dd>
    <dt>so-commons-quartz</dt>
    <dd></dd>
    <dt>so-commons-restful</dt>
    <dd></dd>
    <dt>so-commons-security</dt>
    <dd></dd>
    <dt>so-commons-web</dt>
    <dd></dd>
    <dt>so-commons-webservices</dt>
    <dd></dd>
</dl>
