package nl.sodeso.persistence.elasticsearch.provider;

import nl.sodeso.persistence.elasticsearch.util.ElasticSearchProperties;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.NodeBuilder;

/**
 * The default {@link ClientProvider}.
 *
 * Configuration:
 *
 * https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-discovery-zen.html
 *
 * @author Ronald Mathies
 */
public class DefaultClientProvider implements ClientProvider {

    private static final String CLUSTER_NAME = "cluster.name";
    private static final String MULTICAST_GROUP = "discovery.zen.ping.multicast.group";
    private static final String MULTICAST_PORT = "discovery.zen.ping.multicast.port";

    private Client client;

    private String elasticSearchUnit;

    public DefaultClientProvider(String elasticSearchUnit) {
        this.elasticSearchUnit = elasticSearchUnit;
    }

    /**
     * {@inheritDoc}
     */
    public Client client() {
        if (client == null) {

            Settings settings = ImmutableSettings.settingsBuilder()
                    .put(MULTICAST_GROUP, ElasticSearchProperties.getMulticastGroup(elasticSearchUnit)) // 224.0.0.123
                    .put(MULTICAST_PORT, ElasticSearchProperties.getMulticastPort(elasticSearchUnit)) // 54328
                    .put(CLUSTER_NAME, ElasticSearchProperties.getCluster(elasticSearchUnit)).build();

            client = NodeBuilder.nodeBuilder().settings(settings).client(true).node().client();
        }
        return client;
    }

    /**
     * {@inheritDoc}
     */
    public void open() {
    }

    /**
     * {@inheritDoc}
     */
    public void close() {
    }

}