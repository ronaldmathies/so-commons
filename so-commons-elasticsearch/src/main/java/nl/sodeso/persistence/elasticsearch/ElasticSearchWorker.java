package nl.sodeso.persistence.elasticsearch;

import nl.sodeso.persistence.elasticsearch.annotation.AnnotationUtil;
import nl.sodeso.persistence.elasticsearch.exception.ElasticSearchSearchException;
import nl.sodeso.persistence.elasticsearch.factory.ElasticSearchClientFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.action.count.CountRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;

import javax.annotation.Nonnull;

/**
 * The gateway to elasticsearch. Index creation, index updates and index searches should go through this gateway.
 *
 * To read more a about the query mechanism please refer to:
 *
 * https://www.elastic.co/guide/en/elasticsearch/guide/current/combining-filters.html
 *
 * @author Ronald Mathies
 */
public class ElasticSearchWorker {

    private static final Log LOG = LogFactory.getLog(ElasticSearchWorker.class);

    private ElasticSearchWorker() {
    }

    /**
     * Get the amount of hits found in the given index for the specified type.
     *
     * @param clazz the entity class.
     * @return the number of indexed documents
     */
    public static long count(@Nonnull Class clazz) {
        String type = AnnotationUtil.getElasticSearchType(clazz);
        return countByQuery(clazz, QueryBuilders.termQuery("_type", type));
    }

    /**
     * Get the amount of hits found in the given index and type for the given lucene query.
     *
     * @param entity the entity class
     * @param builder the query builder
     *
     * @return the number of indexed documents
     */
    public static long countByQuery(@Nonnull Class entity, @Nonnull QueryBuilder builder) {
        String unit = AnnotationUtil.getElasticSearchUnit(entity);
        String index = AnnotationUtil.getElasticSearchIndex(entity);

        return new CountRequestBuilder(ElasticSearchClientFactory.getInstance().getClient(unit))
                .setIndices(index)
                .setQuery(builder)
                .execute().actionGet().getCount();
    }

    /**
     * Search a document in the elasticsearch indices by index, type and document id.
     *
     * @param entity the entity class.
     * @param id  the document id.
     * @return the elasticsearch result.
     */
    public static SearchHit searchById(@Nonnull Class entity, @Nonnull String id) {

        // zoeken op id, gewoon searchByField doen? krijgen we tenminste een searchhit.
        SearchHits hits = searchByField(entity, null, "_id", id, 1);
        if (hits.getTotalHits() > 0) {
            return hits.getHits()[0];

        }

        return null;
    }

    /**
     * Search a document in the elasticsearch indices by index, type and field / value. Note that the search
     * is performed using a match query so capitalization is ignored.
     *
     * @param entity the entity class.
     * @param field the field of the entity.
     * @param value the value to search for.
     * @return the elasticsearch result.
     */
    public static SearchHits searchByField(@Nonnull Class entity, @Nonnull String field, @Nonnull String value, int max) {
        return searchByQuery(entity, null, QueryBuilders.matchQuery(field, value), null, max);
    }

    /**
     * Search a document in the elasticsearch indices by index, type and field / value. Note that the search
     * is performed using a match query so capitalization is ignored.
     *
     * @param entity the entity class.
     * @param indices the indices to perform the search on.
     * @param field the field of the entity.
     * @param value the value to search for.
     * @return the elasticsearch result.
     */
    public static SearchHits searchByField(@Nonnull Class entity, String[] indices, @Nonnull String field, @Nonnull String value, int max) {
        return searchByQuery(entity, indices, QueryBuilders.matchQuery(field, value), null, max);
    }

    /**
     * Search a document in the elasticsearch indices by index, type and field / value. Note that the search
     * is performed using a match query so capitalization is ignored.
     *
     * @param entity the entity class.
     * @return the elasticsearch result.
     */
    public static SearchHits searchByQuery(@Nonnull Class entity, @Nonnull QueryBuilder builder, @Nonnull String[] fields, int max) {
        return searchByQuery(entity, null, builder, fields, max);
    }

    /**
     * Search a document in the elasticsearch indices by index, type and field / value. Note that the search
     * is performed using a match query so capitalization is ignored.
     *
     * @param entity the entity class.
     * @return the elasticsearch result.
     */
    public static SearchHits searchByQuery(@Nonnull Class entity, String[] indices, @Nonnull QueryBuilder builder, String[] fields, int max) {
        String unit = AnnotationUtil.getElasticSearchUnit(entity);
        String type = AnnotationUtil.getElasticSearchType(entity);

        if (indices == null || indices.length == 0) {
            indices = new String[] {AnnotationUtil.getElasticSearchIndex(entity)};
        }

        try {
            final SearchRequestBuilder searchRequestBuilder = ElasticSearchClientFactory.getInstance().getClient(unit)
                    .prepareSearch(indices)
                    .setIndicesOptions(IndicesOptions.fromOptions(true, true, false, false))
                    .setTypes(type)
                    .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                    .setQuery(builder);

            if (fields != null && fields.length > 0) {
                searchRequestBuilder.addFields(fields);
            }

            if (max > -1) {
                searchRequestBuilder.setFrom(0).setSize(max).setExplain(true);
            }

            return searchRequestBuilder.execute().actionGet().getHits();

        } catch (Exception e) {
            LOG.warn(String.format("An '%s' occurred with message: '%s'", e.getClass().getSimpleName(), e.getMessage()));
            throw new ElasticSearchSearchException("Error performing search.", e);
        }

    }

}
