package nl.sodeso.persistence.elasticsearch.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Specifies a given field bridge implementation.
 *
 * @author Ronald Mathies
 */
@Retention(RetentionPolicy.RUNTIME)
@Target( { ElementType.FIELD })
public @interface ElasticSearchFieldBridge {

    String name() default "";
    Class<? extends ElasticSearchFieldBridgeMapper> impl();
}
