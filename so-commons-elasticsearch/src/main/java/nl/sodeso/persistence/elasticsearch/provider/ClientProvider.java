package nl.sodeso.persistence.elasticsearch.provider;

import org.elasticsearch.client.Client;

/**
 * A ClientProvider provides an instance of {@link Client}. This instance will be used for request execution.
 *
 * @author Ronald Mathies
 */
public interface ClientProvider {

    /**
     * Opens the provider.
     */
    void open();

    /**
     * Returns a client instance.
     *
     * @return
     */
    Client client();

    /**
     * Closes the provider.
     */
    void close();
}