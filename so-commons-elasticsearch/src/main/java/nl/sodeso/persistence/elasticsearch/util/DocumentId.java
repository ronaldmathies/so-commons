package nl.sodeso.persistence.elasticsearch.util;

import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchDocumentId;
import nl.sodeso.persistence.elasticsearch.exception.ElasticSearchIndexException;

import javax.annotation.Nonnull;
import java.lang.reflect.Field;

/**
 * @author Ronald Mathies
 */
public class DocumentId {

    public static String from(Object entity) {
        return from(entity, entity.getClass());
    }

    /**
     * Get the id for elasticsearch to persist the entity under.
     * If the name is set in the {@link nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchDocumentId} annotation, that name is used as the id. If not set, the name of the field is used.
     *
     * @param entity the entity.
     * @return the id for elasticsearch.
     */
    public static String from(@Nonnull Object entity, @Nonnull Class clazz) {
        String documentId = null;
        final Class<?> superclass = clazz.getSuperclass();
        if (!superclass.getSimpleName().equals(Object.class.getSimpleName())) {
            documentId = from(entity, superclass);
        }
        if (documentId != null) {
            return documentId;
        }
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(ElasticSearchDocumentId.class)) {
                field.setAccessible(true);
                documentId = field.getAnnotation(ElasticSearchDocumentId.class).name();
                if (documentId == null || documentId.isEmpty()) {

                    try {
                        documentId = "" + field.get(entity);
                    } catch (IllegalAccessException e) {
                        throw new ElasticSearchIndexException(String.format("Failed to access field '%s' in entity '%s'.", field.getName(), entity.getClass().getSimpleName()));
                    }
                }
                break;
            }
        }
        return documentId;
    }

}
