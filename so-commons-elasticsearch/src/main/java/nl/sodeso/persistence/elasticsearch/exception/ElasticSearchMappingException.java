package nl.sodeso.persistence.elasticsearch.exception;

import javax.annotation.Nonnull;

/**
 * Exception thrown if anything goes wrong in creating or updating an index.
 *
 * @author Ronald Mathies
 */
public class ElasticSearchMappingException extends RuntimeException {

    /**
     * {@inheritDoc}
     */
    public ElasticSearchMappingException() {
    }

    /**
     * {@inheritDoc}
     */
    public ElasticSearchMappingException(@Nonnull String message) {
        super(message);
    }

    /**
     * {@inheritDoc}
     */
    public ElasticSearchMappingException(@Nonnull String message, @Nonnull Throwable cause) {
        super(message, cause);
    }

    /**
     * {@inheritDoc}
     */
    public ElasticSearchMappingException(@Nonnull Throwable cause) {
        super(cause);
    }
}
