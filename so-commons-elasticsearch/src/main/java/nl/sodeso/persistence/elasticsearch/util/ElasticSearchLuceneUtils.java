package nl.sodeso.persistence.elasticsearch.util;

import nl.sodeso.persistence.elasticsearch.exception.ElasticSearchSearchException;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Query;

import javax.annotation.Nonnull;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Ronald Mathies
 */
public class ElasticSearchLuceneUtils {

    private static Map<String, String> replacementsInIndexStrings = new HashMap<>();
    public static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");

    static {
        replacementsInIndexStrings.put("@", "XX_ATSIGN_XX");
    }

    /**
     * Creates a lucene Query object.
     * Uses a QueryParser if there is only one term in the specified list of fields. Otherwise, uses a MultiFieldQueryParser.
     *
     * @param fields       a list of fields to search in
     * @param searchString Het zoekargument om mee te zoeken
     * @return de fulltextQuery
     */
    public static Query createLuceneQuery(String searchString, @Nonnull String... fields) {
        try {
            if (searchString != null) {

                searchString = adjustStringForIndexOperation(searchString);

                if (fields.length == 1) {
                    return new QueryParser(fields[0], new StandardAnalyzer()).parse(searchString);
                } else {
                    return new MultiFieldQueryParser(fields, new StandardAnalyzer()).parse(searchString);
                }

            }
            throw new ElasticSearchSearchException("Search argument cannot be empty.");
        } catch (ParseException e) {
            throw new ElasticSearchSearchException("Invalid search argument: '" + searchString + "'", e);
        }
    }

    /**
     * Adjust the delivered value of type Object before adding to- or searching in the index.
     *
     * @param value the value to store or search for in the index.
     * @return the indexable value.
     */
    public static Object adjustValueForIndex(@Nonnull Object value) {
        if (value instanceof String) {
            return adjustStringForIndexOperation((String) value);
        }
        if (value instanceof Date) {
            return DATE_FORMAT.format(value);
        }

        return value;
    }

    /**
     * Adjust the delivered string for storing and searching in de index. This basically means that replacements in the string are executed based on
     * replacementsInIndexStrings.
     *
     * @param searchString the string to be adjusted
     * @return the adjusted string
     */
    public static String adjustStringForIndexOperation(@Nonnull String searchString) {
        String adjustedString = searchString;
        final Set<String> keys = replacementsInIndexStrings.keySet();
        for (String key : keys) {
            if (searchString.contains(key)) {
                adjustedString = searchString.replaceAll(key, replacementsInIndexStrings.get(key));
            }
        }
        return adjustedString;
    }
}
