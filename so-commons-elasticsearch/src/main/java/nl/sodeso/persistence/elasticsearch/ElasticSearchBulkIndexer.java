package nl.sodeso.persistence.elasticsearch;

import nl.sodeso.persistence.elasticsearch.annotation.AnnotationUtil;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapper;
import nl.sodeso.persistence.elasticsearch.exception.ElasticSearchIndexException;
import nl.sodeso.persistence.elasticsearch.factory.ElasticSearchClientFactory;
import nl.sodeso.persistence.elasticsearch.serialization.Serializer;
import nl.sodeso.persistence.elasticsearch.util.DocumentId;
import nl.sodeso.persistence.elasticsearch.util.ElasticSearchProperties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ElasticSearchBulkIndexer {

    private static final Log LOG = LogFactory.getLog(ElasticSearchBulkIndexer.class);

    /**
     * Indexes all the specified entities into the elastic search cluster.
     * @param concurrentRequests the number of concurrent requests to execute.
     * @param elasticSearchUnit the elastic search unit.
     * @param entities the entities to index.
     */
    public static void bulkIndex(final int concurrentRequests, @Nonnull String elasticSearchUnit, @Nonnull List<Object> entities) {
        int bulkActions = ElasticSearchProperties.getBulkIndexerSize(elasticSearchUnit);
        int totalActions = entities.size();

        BulkProcessor bulkProcessor = BulkProcessor.builder(ElasticSearchClientFactory.getInstance().getClient(elasticSearchUnit), new BulkProcessor.Listener() {

            int fromActions = 0;
            int toActions = 0;

            public void beforeBulk(long executionId, BulkRequest request) {
                if (LOG.isInfoEnabled()) {
                    final int numberOfActions = request.numberOfActions();
                    fromActions = toActions;
                    toActions += numberOfActions;
                    LOG.info(String.format("Going to execute new bulk index on index '%s' and type '%s': actions %s to %s of %s",
                            "", "", String.valueOf(fromActions), String.valueOf(toActions), String.valueOf(totalActions)));
                }
            }

            public void afterBulk(long executionId, BulkRequest request, BulkResponse response) {
                if (LOG.isInfoEnabled()) {

                    LOG.info(String.format("Executed bulk index on index '%s' and type '%s': actions %s to %s of %s",
                            "", "", String.valueOf(fromActions), String.valueOf(toActions), String.valueOf(totalActions)));
                }
            }

            public void afterBulk(long executionId, BulkRequest request, Throwable failure) {
                LOG.warn("Error executing bulk", failure);
                throw new ElasticSearchIndexException(failure);
            }

        }).setBulkActions(bulkActions).setConcurrentRequests(concurrentRequests).build();

        // add all requests
        long start = System.currentTimeMillis();
        for (Object entity : entities) {

            Object objectToIndex = entity;
            ElasticSearchMapper<Object, Object> elasticSearchMapper = AnnotationUtil.getElasticSearchMapper(entity.getClass());
            if (elasticSearchMapper != null) {
                objectToIndex = elasticSearchMapper.map(entity);
            }

            String type = AnnotationUtil.getElasticSearchType(entity.getClass());
            String index = AnnotationUtil.getElasticSearchIndex(entity.getClass());

            try {
                IndexRequest indexRequest = new IndexRequest(index, type, DocumentId.from(objectToIndex)).source(Serializer.from(objectToIndex));
                bulkProcessor.add(indexRequest);
            } catch (IOException e) {
                LOG.error("Failed to create index for entity '%s'" + objectToIndex.toString());
            }

        }
        if (LOG.isInfoEnabled()) {
            long duration = System.currentTimeMillis() - start;
            LOG.info(String.format("Adding %s entities to the bulprocessor took %s milliseconds", entities.size(), String.valueOf(duration)));
        }

        bulkProcessor.close();
    }

    /**
     * Removes all the specified entities from the elastic search cluster.
     * @param concurrentRequests the number of concurrent requests to execute.
     * @param elasticSearchUnit the elastic search unit.
     * @param entities the entities to remove.
     */
    public static void bulkDelete(final int concurrentRequests, @Nonnull String elasticSearchUnit, @Nonnull List<Object> entities) {
        int bulkActions = ElasticSearchProperties.getBulkIndexerSize(elasticSearchUnit);
        int totalActions = entities.size();

        BulkProcessor bulkProcessor = BulkProcessor.builder(ElasticSearchClientFactory.getInstance().getClient(elasticSearchUnit), new BulkProcessor.Listener() {

            int fromActions = 0;
            int toActions = 0;

            public void beforeBulk(long executionId, BulkRequest request) {
                if (LOG.isInfoEnabled()) {
                    final int numberOfActions = request.numberOfActions();
                    fromActions = toActions;
                    toActions += numberOfActions;
                    LOG.info(String.format("Going to execute new bulk delete on index '%s' and type '%s': actions %s to %s of %s",
                            "", "", String.valueOf(fromActions), String.valueOf(toActions), String.valueOf(totalActions)));
                }
            }

            public void afterBulk(long executionId, BulkRequest request, BulkResponse response) {
                if (LOG.isInfoEnabled()) {
                    LOG.info(String.format("Executed bulk delete on index '%s' and type '%s': actions %s to %s of %s",
                            "", "", String.valueOf(fromActions), String.valueOf(toActions), String.valueOf(totalActions)));
                }
            }

            public void afterBulk(long executionId, BulkRequest request, Throwable failure) {
                LOG.warn("Error executing bulk", failure);
                throw new ElasticSearchIndexException(failure);
            }

        }).setBulkActions(bulkActions).setConcurrentRequests(concurrentRequests).build();

        // add all requests
        long start = System.currentTimeMillis();
        for (Object entity : entities) {

            Object objectToIndex = entity;
            ElasticSearchMapper<Object, Object> elasticSearchMapper = AnnotationUtil.getElasticSearchMapper(entity.getClass());
            if (elasticSearchMapper != null) {
                objectToIndex = elasticSearchMapper.map(entity);
            }

            String type = AnnotationUtil.getElasticSearchType(entity.getClass());
            String index = AnnotationUtil.getElasticSearchIndex(entity.getClass());

            DeleteRequest deleteRequest = new DeleteRequest(index, type, DocumentId.from(objectToIndex));
            bulkProcessor.add(deleteRequest);

        }
        if (LOG.isInfoEnabled()) {
            long duration = System.currentTimeMillis() - start;
            LOG.info(String.format("Deleted %s entities to the bulprocessor took %s milliseconds", entities.size(), String.valueOf(duration)));
        }

        bulkProcessor.close();
    }

}
