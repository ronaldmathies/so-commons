package nl.sodeso.persistence.elasticsearch.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * Specifies that an association is to be indexed in the root entity index.
 *
 * @author Ronald Mathies
 */
@Retention(RetentionPolicy.RUNTIME)
@Target( { ElementType.FIELD })
public @interface ElasticSearchEmbeddedField {

    String name() default "";
}
