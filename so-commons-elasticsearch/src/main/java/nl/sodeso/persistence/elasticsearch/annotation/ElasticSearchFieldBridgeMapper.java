package nl.sodeso.persistence.elasticsearch.annotation;

import java.util.Map;

/**
 * Link between a java property and an elasticsearch.
 * Usually a Java property will be linked to a Document Field.
 *
 * @author Ronald Mathies
 */
public interface ElasticSearchFieldBridgeMapper {

    void map(Object bridgedField, Map<String, Object> mappings, String keyPrefixPath) throws NoSuchFieldException, InstantiationException, IllegalAccessException;
}
