package nl.sodeso.persistence.elasticsearch.provider;

import org.elasticsearch.client.Client;
import org.elasticsearch.common.io.FileSystemUtils;
import org.elasticsearch.common.network.NetworkUtils;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;

import javax.annotation.Nonnull;
import java.io.File;

/**
 * LocalClientProvider instantiates a provider node with in-memory index store type.
 * Note: this one is for testing mainly!
 *
 * @author Ronald Mathies
 */
public class LocalClientProvider implements ClientProvider {

    private Node node = null;
    private Client client = null;
    private Settings settings = null;

    public LocalClientProvider() {
    }

    /**
     * {@inheritDoc}
     */
    public LocalClientProvider(@Nonnull Settings settings) {
        this.settings = settings;
    }

    /**
     * {@inheritDoc}
     */
    public void open() {
        if (node == null || node.isClosed()) {
            // Build and start the node
            client = NodeBuilder.nodeBuilder().settings(buildNodeSettings()).node().client();

            // Wait for Yellow status
            client.admin().cluster()
                    .prepareHealth()
                    .setWaitForYellowStatus()
                    .setTimeout(TimeValue.timeValueMinutes(1))
                    .execute()
                    .actionGet();
        }
    }

    /**
     * {@inheritDoc}
     */
    public Client client() {
        if (client == null) {
            open();
        }

        return client;
    }

    /**
     * {@inheritDoc}
     */
    public void close() {
        if (client() != null) {
            client.close();
        }

        if ((node != null) && (!node.isClosed())) {
            node.close();

            FileSystemUtils.deleteRecursively(new File("./target/elasticsearch-test/"), true);
        }
    }

    /**
     * {@inheritDoc}
     */
    protected Settings buildNodeSettings() {

        // Build settings
        ImmutableSettings.Builder builder = ImmutableSettings.settingsBuilder()
                .put("node.name", "node-test-" + System.currentTimeMillis())
                .put("node.data", true)
                .put("cluster.name", "cluster-test-" + NetworkUtils.getLocalAddress().getHostName() + "-" + System.currentTimeMillis())
                .put("index.store.type", "memory")
                .put("index.store.fs.memory.enabled", "true")
                .put("gateway.type", "none")
                .put("path.data", "./target/elasticsearch-test/data")
                .put("path.work", "./target/elasticsearch-test/work")
                .put("path.logs", "./target/elasticsearch-test/logs")
                .put("index.number_of_shards", "1")
                .put("index.number_of_replicas", "0")
                .put("cluster.routing.schedule", "50ms")
                .put("node.provider", true);

        if (settings != null) {
            builder.put(settings);
        }

        return builder.build();
    }
}