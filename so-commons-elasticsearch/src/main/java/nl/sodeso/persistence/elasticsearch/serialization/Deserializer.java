package nl.sodeso.persistence.elasticsearch.serialization;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.sodeso.persistence.elasticsearch.exception.ElasticSearchMappingException;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.search.SearchHit;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * @author Ronald Mathies
 */
public class Deserializer {

    /**
     * Deserializes the result and maps the information to the target class.
     *
     * @param hit the search result.
     * @param target the target to create and update with the search result information.
     * @param <T> the target type.
     *
     * @return the instantiated target processed with the results information.
     */
    public static <T> T from(@Nonnull SearchHit hit, @Nonnull Class<T> target) {
        return from(hit.getSourceAsString(), target);
    }

    public static <T> T from(@Nonnull GetResponse response, @Nonnull Class<T> target) {
        return from(response.getSourceAsString(), target);
    }

    public static <T> T from(@Nonnull String source, @Nonnull Class<T> target) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
            return mapper.readValue(source, target);
        } catch (IOException e) {
            throw new ElasticSearchMappingException(e);
        }
    }

}
