package nl.sodeso.persistence.elasticsearch.provider;

import nl.sodeso.persistence.elasticsearch.util.ElasticSearchProperties;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import javax.annotation.Nonnull;

/**
 * The default {@link nl.sodeso.persistence.elasticsearch.provider.ClientProvider}.
 *
 * @author Ronald Mathies
 */
public class TransportClientProvider implements ClientProvider {

    private static final String CLUSTER_NAME = "cluster.name";

    private Client client;

    private String elasticSearchUnit = null;

    public TransportClientProvider(@Nonnull String elasticSearchUnit) {
        this.elasticSearchUnit = elasticSearchUnit;
    }

    /**
     * {@inheritDoc}
     */
    public Client client() {
        if (client == null) {
            client = new TransportClient(ImmutableSettings.settingsBuilder()
                    .put(CLUSTER_NAME, ElasticSearchProperties.getCluster(elasticSearchUnit)).build());

            ((TransportClient) client).addTransportAddress(
                    new InetSocketTransportAddress(
                            ElasticSearchProperties.getTransportHost(elasticSearchUnit),
                            ElasticSearchProperties.getTransportPort(elasticSearchUnit)));
        }

        return client;
    }

    /**
     * {@inheritDoc}
     */
    public void open() {
    }

    /**
     * {@inheritDoc}
     */
    public void close() {
    }

}