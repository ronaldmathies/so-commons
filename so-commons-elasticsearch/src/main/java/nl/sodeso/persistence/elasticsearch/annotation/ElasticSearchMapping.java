package nl.sodeso.persistence.elasticsearch.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this annotation to define a mapper that will map the data from a source object to a destination object.
 * The destination object will then be used for indexing purposes.
 *
 * This annotation is optional, when the annotation is not present the source object will be used for indexing purposes.
 *
 * @author Ronald Mathies
 */
@Retention(RetentionPolicy.RUNTIME)
@Target( { ElementType.TYPE })
public @interface ElasticSearchMapping {

    /**
     * The mapper that will be used to map the data from the source object to the destination object.
     * @return the mapper.
     */
    Class<? extends ElasticSearchMapper> mapper();

    Class mapsToClass();
}
