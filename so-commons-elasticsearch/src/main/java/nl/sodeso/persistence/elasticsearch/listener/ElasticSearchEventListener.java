package nl.sodeso.persistence.elasticsearch.listener;

import nl.sodeso.persistence.elasticsearch.ElasticSearchIndexer;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;

/**
 * Binds JPA events to elasticsearch.
 *
 * The following phases are used in the event binding:
 *
 * PostPersist initiates an index creation.
 * PostUpdate updates an index.
 * PostRemove removes the index.
 *
 * @author Ronald Mathies
 */
public class ElasticSearchEventListener {

    /**
     * Execute actions that need to occur after the entity has been stored for the first time.
     *
     * For example, creating a new index within the Elastic Search cluster.
     *
     * @param entity the entity.
     */
    @PostPersist
    public void postPersist(Object entity) {
        ElasticSearchIndexer.createIndex(entity);
    }

    /**
     * Execute actions that need to occur after the entity has been updated.
     *
     * For example, updating an existing index within the Elastic Search cluster.
     *
     * @param entity the entity.
     */
    @PostUpdate
    public void postUpdate(Object entity) {
        ElasticSearchIndexer.updateIndex(entity);

    }

    /**
     * Execute actions that need to occur after the entity has been removed.
     *
     * For example, removing an existing index from the Elastic Search cluster.
     *
     * @param entity the entity.
     */
    @PostRemove
    public void postRemove(Object entity) {
        ElasticSearchIndexer.removeIndex(entity);
    }

}
