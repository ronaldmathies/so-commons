package nl.sodeso.persistence.elasticsearch.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used for determining wich elastic-search cluster this entity belongs to.
 *
 * @author Ronald Mathies
 */
@Retention(RetentionPolicy.RUNTIME)
@Target( { ElementType.TYPE })
public @interface ElasticSearchIndex {

    /**
     * The index in which the enity should be indexed.
     * @return the index.
     */
    String index() default "index";

}
