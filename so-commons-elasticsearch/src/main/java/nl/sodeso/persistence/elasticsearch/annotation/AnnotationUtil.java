package nl.sodeso.persistence.elasticsearch.annotation;

import nl.sodeso.persistence.elasticsearch.exception.ElasticSearchIndexException;

import javax.annotation.Nonnull;

/**
 * @author Ronald Mathies
 */
public class AnnotationUtil {

    /**
     * Returns the class that will be returned from the mapper when the mapper is used.
     * @param clazz the class to lookup the mapper from which contains the result class.
     * @return the result class which will be returned after mapping the class.
     */
    @SuppressWarnings("unchecked")
    public static Class getMapsToClass(Class clazz) {
        ElasticSearchMapping annotation = (ElasticSearchMapping)clazz.getAnnotation(ElasticSearchMapping.class);
        if (annotation != null) {
            return annotation.mapsToClass();
        }

        return null;
    }

    /**
     * Returns the ElasticSearchMapper implementation class which is registered on the specified class.
     * @param clazz the class for which to lookup the ElasticSearchMapper implementation.
     * @param <S> the source class.
     * @param <D> the destination class.
     * @return the implemented ElasticSearchMapper.
     */
    @SuppressWarnings("unchecked")
    public static <S, D> ElasticSearchMapper<S, D> getElasticSearchMapper(Class clazz) {
        ElasticSearchMapper<S, D> elasticSearchMapper = null;

        ElasticSearchMapping annotation = (ElasticSearchMapping)clazz.getAnnotation(ElasticSearchMapping.class);
        if (annotation != null) {
            try {
                elasticSearchMapper = annotation.mapper().newInstance();
            } catch (IllegalAccessException | InstantiationException e) {
                throw new ElasticSearchIndexException(String.format("Failed to instantiate mapper '%s'.", annotation.mapper().getSimpleName()), e);
            }
        }

        return elasticSearchMapper;
    }

    /**
     * Returns the elastic search index as registered on the specified class.
     * @param clazz the class to lookup the elastic search index for.
     * @return the elastic search index.
     */
    @SuppressWarnings("unchecked")
    public static String getElasticSearchIndex(Class clazz) {
        ElasticSearchIndex annotation = (ElasticSearchIndex)clazz.getAnnotation(ElasticSearchIndex.class);
        if (annotation != null) {
            return annotation.index();
        }

        Class fallbackClazz = getMapsToClass(clazz);
        if (fallbackClazz != null) {
            annotation = (ElasticSearchIndex)fallbackClazz.getAnnotation(ElasticSearchIndex.class);
            if (annotation != null) {
                return annotation.index();
            }
        }

        throw new ElasticSearchIndexException(String.format("No index was specified on object '%s'", clazz.getName())
                + (fallbackClazz != null ? String.format(" or '%s'.", fallbackClazz.getName()) : ""));
    }

    @SuppressWarnings("unchecked")
    public static String getElasticSearchType(Class clazz) {
        ElasticSearchType annotation = (ElasticSearchType)clazz.getAnnotation(ElasticSearchType.class);
        if (annotation != null) {
            return annotation.type();
        }

        Class fallbackClazz = getMapsToClass(clazz);
        if (fallbackClazz != null) {
            annotation = (ElasticSearchType)fallbackClazz.getAnnotation(ElasticSearchType.class);
            if (annotation != null) {
                return annotation.type();
            }
        }

        throw new ElasticSearchIndexException(String.format("No type was specified on object '%s'", clazz.getName())
            + (fallbackClazz != null ? String.format(" or '%s'.", fallbackClazz.getName()) : ""));
    }

    @SuppressWarnings("unchecked")
    public static String getElasticSearchUnit(Class clazz) {
        ElasticSearchUnit annotation = (ElasticSearchUnit)clazz.getAnnotation(ElasticSearchUnit.class);
        if (annotation != null) {
            return annotation.unit();
        }

        Class fallbackClazz = getMapsToClass(clazz);
        if (fallbackClazz != null) {
            annotation = (ElasticSearchUnit)fallbackClazz.getAnnotation(ElasticSearchUnit.class);
            if (annotation != null) {
                return annotation.unit();
            }
        }

        throw new ElasticSearchIndexException(String.format("No unit was specified on object '%s'", clazz.getName())
                + (fallbackClazz != null ? String.format(" or '%s'.", fallbackClazz.getName()) : ""));
    }

}
