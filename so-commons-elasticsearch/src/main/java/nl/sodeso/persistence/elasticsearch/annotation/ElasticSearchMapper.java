package nl.sodeso.persistence.elasticsearch.annotation;

/**
 * @author Ronald Mathies
 */
public interface ElasticSearchMapper<S, D> {

    /**
     * Should map the data from the source object to the destination object, the destination object
     * will be the object that is used within the indexing process.
     *
     * @param object the source object.
     * @return the destination obejct.
     */
    public D map(S object);

}
