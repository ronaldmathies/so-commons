package nl.sodeso.persistence.elasticsearch;

import nl.sodeso.persistence.elasticsearch.annotation.AnnotationUtil;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapper;
import nl.sodeso.persistence.elasticsearch.factory.ElasticSearchClientFactory;
import nl.sodeso.persistence.elasticsearch.serialization.Serializer;
import nl.sodeso.persistence.elasticsearch.util.DocumentId;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.flush.FlushRequest;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.engine.DocumentMissingException;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * @author Ronald Mathies
 */
public class ElasticSearchIndexer {

    private static final Log LOG = LogFactory.getLog(ElasticSearchIndexer.class);

    private ElasticSearchIndexer() {}

    /**
     * Create an index in elasticsearch.
     *
     * @param entity the entity to add to the index.
     */
    public static void createIndex(@Nonnull Object entity) {
        Object objectToIndex = entity;
        ElasticSearchMapper<Object, Object> elasticSearchMapper = AnnotationUtil.getElasticSearchMapper(entity.getClass());
        if (elasticSearchMapper != null) {
            objectToIndex = elasticSearchMapper.map(entity);
        }

        String documentID = DocumentId.from(objectToIndex);

        String type = AnnotationUtil.getElasticSearchType(entity.getClass());
        String unit = AnnotationUtil.getElasticSearchUnit(entity.getClass());
        String index = AnnotationUtil.getElasticSearchIndex(entity.getClass());

        try {
            Client client = ElasticSearchClientFactory.getInstance().getClient(unit);
            client.prepareIndex(index, type, documentID)
                    .setSource(Serializer.from(objectToIndex))
                    .execute().actionGet();

        } catch (IOException e) {
            LOG.warn(String.format("creation of index failed: %s, %s, %s: message is %s",
                    index, type, documentID, e.getMessage()));
        }
    }

    /**
     * Update an index in elasticsearch.
     *
     * @param entity the entity to update in the index.
     */
    public static void updateIndex(@Nonnull Object entity) {
        Object objectToIndex = entity;
        ElasticSearchMapper<Object, Object> elasticSearchMapper = AnnotationUtil.getElasticSearchMapper(entity.getClass());
        if (elasticSearchMapper != null) {
            objectToIndex = elasticSearchMapper.map(entity);
        }

        String documentID = DocumentId.from(objectToIndex);

        String type = AnnotationUtil.getElasticSearchType(entity.getClass());
        String unit = AnnotationUtil.getElasticSearchUnit(entity.getClass());
        String index = AnnotationUtil.getElasticSearchIndex(entity.getClass());

        try {
            Client client = ElasticSearchClientFactory.getInstance().getClient(unit);
            client.prepareUpdate(index, type, documentID)
                    .setDoc(Serializer.from(objectToIndex))
                    .execute().actionGet();

        } catch (DocumentMissingException e) {
            LOG.warn(String.format("update of index failed: %s, %s, %s: message is %s.. Is the index removed in the mean time? Trying to re-create it...",
                    index, type, documentID, e.getMessage()));

            createIndex(entity);
        } catch (IOException e) {
            LOG.warn(String.format("update of index failed: %s, %s, %s: message is %s",
                    index, type, documentID, e.getMessage()));
        }
    }

    /**
     * Remove an index in elasticsearch.
     *
     * @param entity the entity to remove from the index.
     */
    public static void removeIndex(@Nonnull Object entity) {
        Object objectToIndex = entity;
        ElasticSearchMapper<Object, Object> elasticSearchMapper = AnnotationUtil.getElasticSearchMapper(entity.getClass());
        if (elasticSearchMapper != null) {
            objectToIndex = elasticSearchMapper.map(entity);
        }

        String documentID = DocumentId.from(objectToIndex);

        String unit = AnnotationUtil.getElasticSearchUnit(entity.getClass());
        String type = AnnotationUtil.getElasticSearchType(entity.getClass());
        String index = AnnotationUtil.getElasticSearchIndex(entity.getClass());

        Client client = ElasticSearchClientFactory.getInstance().getClient(unit);
        client.prepareDelete(index, documentID, type)
                .execute()
                .actionGet();
    }

    /**
     * Clears the complete index or if the index is not found (or another exception occurs) does nothing.
     *
     * @param index the index to clear.
     * @param elasticSearchUnit the Elastic Search unit.
     */
    public static void clearIndex(@Nonnull String index, @Nonnull String elasticSearchUnit) {
        ElasticSearchClientFactory.getInstance().getClient(elasticSearchUnit)
                .admin().indices().delete(new DeleteIndexRequest(index))
                .actionGet();
    }

    /**
     * Refresh the index with a specified index.
     *
     * @param elasticSearchUnit the Elastic Search unit.
     * @param index the index to refresh.
     */
    public static void refreshIndex(@Nonnull String elasticSearchUnit, @Nonnull String index) {
        Client client = ElasticSearchClientFactory.getInstance().getClient(elasticSearchUnit);
        client.admin().indices().refresh(new RefreshRequest(index))
                .actionGet();
    }

    /**
     * Flushes the index with a specified index.
     *
     * @param elasticSearchUnit the Elastic Search Unit.
     * @param index, the index to flush.
     */
    public static void flushIndex(@Nonnull String elasticSearchUnit, @Nonnull String index) {
        Client client = ElasticSearchClientFactory.getInstance().getClient(elasticSearchUnit);
        client.admin().indices().flush(new FlushRequest(index))
                .actionGet();
    }


}
