package nl.sodeso.persistence.elasticsearch;

import nl.sodeso.persistence.elasticsearch.factory.ElasticSearchClientFactory;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequestBuilder;
import org.elasticsearch.client.Client;

import javax.annotation.Nonnull;

/**
 * @author Ronald Mathies
 */
public class ElasticSearchAdmin {

    /**
     * Returns a flag indicating if the specified index exists.
     * @param elasticSearchUnit the elastic search unit.
     * @param index the index to look for.
     * @return true if the index exists, false if not.
     */
    public static boolean indexExists(@Nonnull String elasticSearchUnit, @Nonnull String index) {
        Client client = ElasticSearchClientFactory.getInstance().getClient(elasticSearchUnit);
        return indexExists(client, index);
    }

    /**
     * Returns a flag indicating if the specified index exists.
     * @param client the elastic search client.
     * @param index the index to look for.
     * @return true if the index exists, false if not.
     */
    public static boolean indexExists(@Nonnull Client client, @Nonnull String index) {
        IndicesExistsRequestBuilder indicesExistsRequestBuilder = client.admin().indices().prepareExists(index);
        return indicesExistsRequestBuilder.execute().actionGet().isExists();
    }

    /**
     * Removes the specified index from the elastic search cluster.
     * @param elasticSearchUnit the elastic search unit.
     * @param index the index to remove.
     * @return true if the index was removed, false if not.
     */
    public static boolean deleteIndex(@Nonnull String elasticSearchUnit, @Nonnull String index) {
        Client client = ElasticSearchClientFactory.getInstance().getClient(elasticSearchUnit);
        return deleteIndex(client, index);
    }

    /**
     * Removes the specified index form the elastic search cluster.
     * @param client the client to use for removing the index.
     * @param index the index to remove.
     * @return true if the index was removed, false if not.
     */
    public static boolean deleteIndex(@Nonnull Client client, @Nonnull String index) {
        DeleteIndexRequestBuilder deleteIndexRequestBuilder = client.admin().indices().prepareDelete(index);
        return deleteIndexRequestBuilder.execute().actionGet().isAcknowledged();
    }

}
