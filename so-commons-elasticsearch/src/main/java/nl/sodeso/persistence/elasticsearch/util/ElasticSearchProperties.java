package nl.sodeso.persistence.elasticsearch.util;

import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.persistence.elasticsearch.exception.ElasticSearchPropertyException;

import javax.annotation.Nonnull;

/**
 * @author Ronald Mathies
 */
public class ElasticSearchProperties {

    public static final String ELASTICSEARCH_UNIT = "elasticsearch.unit";
    public static final String CLIENT_PROVIDER = "elasticsearch.provider";
    public static final String CLUSTER = "elasticsearch.cluster";

    public static final String BULK_INDEXER_SIZE = "elasticsearch.bulk.indexer.size";

    public static final String TRANSPORT_HOST = "elasticsearch.provider.transport.host";
    public static final String TRANSPORT_PORT = "elasticsearch.provider.transport.port";

    public static final String DEFAULT_MULTICAST_GROUP = "elasticsearch.provider.default.discovery.zen.ping.multicast.group";
    public static final String DEFAULT_MULTICAST_PORT = "elasticsearch.provider.default.discovery.zen.ping.multicast.port";

    /**
     * Returns the size of entries that will be used within the bulk-indexer.
     * @param elasticsearchUnit the elastic search unit.
     * @return the size of entries.
     */
    public static Integer getBulkIndexerSize(@Nonnull String elasticsearchUnit) {
        String domain = getConfigurationDomainUsingElasticSearchUnit(elasticsearchUnit);
        Integer size = PropertyConfiguration.getInstance().getIntegerProperty(domain, ElasticSearchProperties.BULK_INDEXER_SIZE);
        if (size == null) {
            throw new ElasticSearchPropertyException(String.format("The property '%s' is not defined in the domain '%s'", BULK_INDEXER_SIZE, domain));
        }

        return size;
    }

    /**
     * Returns the elastic search cluster.
     * @param elasticsearchUnit the elastic search unit.
     * @return the elastic search cluster.
     */
    public static String getCluster(@Nonnull String elasticsearchUnit) {
        String domain = getConfigurationDomainUsingElasticSearchUnit(elasticsearchUnit);
        String provider =  PropertyConfiguration.getInstance().getStringProperty(domain, ElasticSearchProperties.CLUSTER);
        if (provider == null) {
            throw new ElasticSearchPropertyException(String.format("The property '%s' is not defined in the domain '%s'", CLUSTER, domain));
        }

        return provider;
    }

    /**
     * Returns the provider that will be used to setup a connection with the elastic search index.
     * @param elasticsearchUnit the elastic search unit.
     * @return the provider.
     */
    public static String getProvider(@Nonnull String elasticsearchUnit) {
        String domain = getConfigurationDomainUsingElasticSearchUnit(elasticsearchUnit);
        String provider =  PropertyConfiguration.getInstance().getStringProperty(domain, ElasticSearchProperties.CLIENT_PROVIDER);
        if (provider == null) {
            throw new ElasticSearchPropertyException(String.format("The property '%s' is not defined in the domain '%s'", CLIENT_PROVIDER, domain));
        }

        return provider;
    }

    /**
     * Returns the host of the elastic search index.
     * @param elasticsearchUnit the elastic search unit.
     * @return the host.
     */
    public static String getTransportHost(@Nonnull String elasticsearchUnit) {
        String domain = getConfigurationDomainUsingElasticSearchUnit(elasticsearchUnit);
        String host = PropertyConfiguration.getInstance().getStringProperty(domain, ElasticSearchProperties.TRANSPORT_HOST);
        if (host == null) {
            throw new ElasticSearchPropertyException(String.format("The property '%s' is not defined in the domain '%s'", TRANSPORT_HOST, domain));
        }

        return host;
    }

    /**
     * Returns the port number of the elastic search index.
     * @param elasticsearchUnit the elastic search unit.
     * @return the port.
     */
    public static int getTransportPort(@Nonnull String elasticsearchUnit) {
        String domain = getConfigurationDomainUsingElasticSearchUnit(elasticsearchUnit);
        Integer port = PropertyConfiguration.getInstance().getIntegerProperty(domain, ElasticSearchProperties.TRANSPORT_PORT);
        if (port == null) {
            throw new ElasticSearchPropertyException(String.format("The property '%s' is not defined in the domain '%s'", TRANSPORT_PORT, domain));
        }

        return port;
    }

    /**
     * Returns the multicast group to find the elastic search index.
     * @param elasticsearchUnit the elastic search unit.
     * @return the multicast group.
     */
    public static String getMulticastGroup(@Nonnull String elasticsearchUnit) {
        String domain = getConfigurationDomainUsingElasticSearchUnit(elasticsearchUnit);
        String group = PropertyConfiguration.getInstance().getStringProperty(domain, ElasticSearchProperties.DEFAULT_MULTICAST_GROUP);
        if (group == null) {
            throw new ElasticSearchPropertyException(String.format("The property '%s' is not defined in the domain '%s'", DEFAULT_MULTICAST_GROUP, domain));
        }

        return group;
    }

    /**
     * Return ths multicast port to find the elastic search index.
     * @param elasticsearchUnit the elastic search unit.
     * @return the multicast port.
     */
    public static int getMulticastPort(@Nonnull String elasticsearchUnit) {
        String domain = getConfigurationDomainUsingElasticSearchUnit(elasticsearchUnit);
        Integer port = PropertyConfiguration.getInstance().getIntegerProperty(domain, ElasticSearchProperties.DEFAULT_MULTICAST_PORT);
        if (port == null) {
            throw new ElasticSearchPropertyException(String.format("The property '%s' is not defined in the domain '%s'", DEFAULT_MULTICAST_PORT, domain));
        }

        return port;
    }

    /**
     * Returns the configuration domain that contains the specified elastic search unit.
     * @param elasticsearchUnit the elastic search unit.
     * @return the configuration domain.
     */
    public static String getConfigurationDomainUsingElasticSearchUnit(@Nonnull String elasticsearchUnit) {
        String domain = PropertyConfiguration.getInstance().getPropertyContainerDomainBy(ELASTICSEARCH_UNIT, elasticsearchUnit);
        if (domain == null || domain.isEmpty()) {
            throw new ElasticSearchPropertyException(String.format("Unable to find Elastic Search configuration that uses the '%s' unit name.", elasticsearchUnit));
        }

        return domain;
    }

}
