package nl.sodeso.persistence.elasticsearch.exception;

import javax.annotation.Nonnull;

/**
 * Exception thrown if anything goes wrong in searching an index.
 *
 * @author Ronald Mathies
 */
public class ElasticSearchSearchException extends RuntimeException {

    /**
     * {@inheritDoc}
     */
    public ElasticSearchSearchException() {
    }

    /**
     * {@inheritDoc}
     */
    public ElasticSearchSearchException(@Nonnull String message) {
        super(message);
    }

    /**
     * {@inheritDoc}
     */
    public ElasticSearchSearchException(@Nonnull String message, @Nonnull Throwable cause) {
        super(message, cause);
    }
}
