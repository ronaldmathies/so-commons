package nl.sodeso.persistence.elasticsearch.exception;

import javax.annotation.Nonnull;

/**
 * Exception thrown if anything goes wrong in instantiating a connection with elasticseearch due to invalid or non existing properties.
 *
 * @author Ronald Mathies
 */
public class ElasticSearchPropertyException extends RuntimeException {

    /**
     * {@inheritDoc}
     */
    public ElasticSearchPropertyException(@Nonnull String message) {
        super(message);
    }

    /**
     * {@inheritDoc}
     */
    public ElasticSearchPropertyException(@Nonnull String message, @Nonnull Throwable cause) {
        super(message, cause);
    }

    /**
     * {@inheritDoc}
     */
    public ElasticSearchPropertyException(@Nonnull Throwable cause) {
        super(cause);
    }
}
