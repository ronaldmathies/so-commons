package nl.sodeso.persistence.elasticsearch.factory;

import nl.sodeso.persistence.elasticsearch.provider.ClientProvider;
import nl.sodeso.persistence.elasticsearch.provider.DefaultClientProvider;
import nl.sodeso.persistence.elasticsearch.provider.LocalClientProvider;
import nl.sodeso.persistence.elasticsearch.provider.TransportClientProvider;
import nl.sodeso.persistence.elasticsearch.util.ElasticSearchProperties;
import org.elasticsearch.client.Client;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class ElasticSearchClientFactory {

    private Map<String, ClientProvider> clients = new HashMap<>();

    private static ElasticSearchClientFactory factory = null;

    private ElasticSearchClientFactory() {
    }

    public static ElasticSearchClientFactory getInstance() {
        if (factory == null) {
            factory = new ElasticSearchClientFactory();
        }

        return factory;
    }

    public ClientProvider getClientProvider(@Nonnull String elasticSearchUnit) {
        if (clients.containsKey(elasticSearchUnit)) {
            return clients.get(elasticSearchUnit);
        }

        ClientProvider client = getElasticSearchClient(elasticSearchUnit);
        clients.put(elasticSearchUnit, client);

        return client;
    }

    public Client getClient(@Nonnull String elasticSearchUnit) {
        return getClientProvider(elasticSearchUnit).client();
    }

    private ClientProvider getElasticSearchClient(@Nonnull String elasticSearchUnit) {
        ClientProvider clientProvider = null;

        final String clientproviderProperty = ElasticSearchProperties.getProvider(elasticSearchUnit);
        if (clientproviderProperty == null) {
            clientProvider = new DefaultClientProvider(elasticSearchUnit);
        } else {
            switch (clientproviderProperty) {
                case "local":
                    clientProvider = new LocalClientProvider();
                    break;
                case "transport":
                    clientProvider = new TransportClientProvider(elasticSearchUnit);
                    break;
                default:
                    clientProvider = new DefaultClientProvider(elasticSearchUnit);
                    break;
            }
        }

        return clientProvider;
    }
}
