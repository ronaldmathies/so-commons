package nl.sodeso.persistence.elasticsearch.serialization;

import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Ronald Mathies
 */
public class Serializer {

    public static XContentBuilder from(@Nonnull Object entity) throws IOException {
        // get all mappings. Create an empty map to pass along.
        Map<String, Object> mappings = new HashMap<>();
        SerializationUtil.collectMappings(entity, entity.getClass(), mappings, null);

        XContentBuilder builder = XContentFactory.jsonBuilder().startObject();

        final Set<String> keys = mappings.keySet();
        for (String key : keys) {
            builder.field(key, mappings.get(key));
        }

        return builder.endObject();
    }

}
