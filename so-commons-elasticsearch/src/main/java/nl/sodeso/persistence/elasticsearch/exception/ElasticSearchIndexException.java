package nl.sodeso.persistence.elasticsearch.exception;

import javax.annotation.Nonnull;

/**
 * Exception thrown if anything goes wrong in creating or updating an index.
 *
 * @author Ronald Mathies
 */
public class ElasticSearchIndexException extends RuntimeException {

    /**
     * {@inheritDoc}
     */
    public ElasticSearchIndexException() {
    }

    /**
     * {@inheritDoc}
     */
    public ElasticSearchIndexException(@Nonnull String message) {
        super(message);
    }

    /**
     * {@inheritDoc}
     */
    public ElasticSearchIndexException(@Nonnull Throwable cause) {
        super(cause);
    }

    /**
     * {@inheritDoc}
     */
    public ElasticSearchIndexException(@Nonnull String message, @Nonnull Throwable cause) {
        super(message, cause);
    }
}
