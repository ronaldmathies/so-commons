package nl.sodeso.persistence.elasticsearch.serialization;

import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchDocumentId;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchEmbeddedField;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchField;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchFieldBridge;
import nl.sodeso.persistence.elasticsearch.exception.ElasticSearchIndexException;
import nl.sodeso.persistence.elasticsearch.util.ElasticSearchLuceneUtils;

import javax.annotation.Nonnull;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class SerializationUtil {

    private static final String KEY_PATH_SEPERATOR = ".";

    /**
     * Collect all mappings to be persisted in elasticsearch. All associations are traversed and all the key-values found are added to the specified mappings map.
     *
     * @param entity        the entity to be indexed.
     * @param clazz         the class or a superclass of the specified entity
     * @param mappings      a {@link java.util.Map} to add all fields to be persisted to.
     * @param keyPrefixPath the prefix for the index field. If not null, this entity is an embedded entity and the index gets prefixed with the name of the parent field.
     */
    public static void collectMappings(@Nonnull Object entity, @Nonnull Class clazz, @Nonnull Map<String, Object> mappings, @Nonnull String keyPrefixPath) {
        // check super classes first
        final Class<?> superclass = clazz.getSuperclass();

        if (!superclass.getSimpleName().equals(Object.class.getSimpleName())) {
            collectMappings(entity, superclass, mappings, keyPrefixPath);
        }

        addElasticSearchFields(entity, mappings, keyPrefixPath, clazz);
        addElasticSearchEmbeddedFields(entity, clazz, mappings, keyPrefixPath);
        addElasticSearchBridgedFields(entity, clazz, mappings, keyPrefixPath);
    }

    private static void addElasticSearchFields(@Nonnull Object entity, @Nonnull Map<String, Object> mappings, @Nonnull String keyPrefixPath, @Nonnull Class clazz) {
        addElasticSearchFields(entity, mappings, keyPrefixPath, clazz.getDeclaredFields());
    }

    /**
     * Add entries to the specified map for all specified fields that are annotated with {@link ElasticSearchField} or {@link ElasticSearchDocumentId}.
     * The name of the annotation field will be the key in the map.
     * This method calls addElasticSearchFields(Object entity, Map<String, List<Object>> mappings, String keyPrefixPath, Field[] fields, boolean checkHasElasticSearchAnnotation)
     * with checkHasElasticSearchAnnotation set to 'true'
     * to do the actual work.
     *
     * @param entity                          the entity to search for fileds with an {@link ElasticSearchField} annotations.
     * @param mappings                        the {@link java.util.Map} to add the field mappings to.
     * @param keyPrefixPath                   the prefix for the index field. If not null, this entity is an embedded entity and the index gets prefixed with the name of the parent field.
     */
    public static void addElasticSearchFields(@Nonnull Object entity, @Nonnull Map<String, Object> mappings, @Nonnull String keyPrefixPath, @Nonnull Field[] fields) {
        addElasticSearchFields(entity, mappings, keyPrefixPath, fields, true);
    }

    /**
     * Add entries to the specified map for all specified fields.
     * If 'checkHasElasticSearchAnnotation' is set to 'true', only the fields that are annotated with {@link ElasticSearchField} or {@link ElasticSearchDocumentId}
     * will be processed. Otherwise, all fields are processed.
     * The name of the annotation field will be the key in the map.
     *
     * @param entity                          the entity to search for fileds with an {@link ElasticSearchField} annotations.
     * @param mappings                        the {@link java.util.Map} to add the field mappings to.
     * @param keyPrefixPath                   the prefix for the index field. If not null, this entity is an embedded entity and the index gets prefixed with the name of the parent field.
     * @param checkHasElasticSearchAnnotation indicator whether to check the fields for having an annotation known by Elasticsearch
     */
    public static void addElasticSearchFields(@Nonnull Object entity, @Nonnull Map<String, Object> mappings, String keyPrefixPath, @Nonnull Field[] fields, boolean checkHasElasticSearchAnnotation) {
        for (Field field : fields) {
            if (!checkHasElasticSearchAnnotation ||
                    field.isAnnotationPresent(ElasticSearchField.class) || field.isAnnotationPresent(ElasticSearchDocumentId.class)) {
                field.setAccessible(true);
                String key = getFieldIndexName(field);
                if (keyPrefixPath != null) {
                    key = keyPrefixPath + key;
                }

                Object value;
                try {
                    value = field.get(entity);
                    if (value == null) {
                        continue;
                    }
                } catch (IllegalAccessException e) {
                    throw new ElasticSearchIndexException(String.format("Failed to access field '%s' in entity '%s'.", field.getName(), entity.getClass().getSimpleName()));
                }

                mappings.put(key, ElasticSearchLuceneUtils.adjustValueForIndex(value));
            }
        }
    }

    /**
     * Add entries to the specified map for all fields on the entity that are annotated with {@link ElasticSearchEmbeddedField}.
     * The name of the annotation field will be the key in the map.
     * The fields to be persisted are all fields annotated with {@link ElasticSearchField} on the object (field) that is annotated with {@link ElasticSearchEmbeddedField}.
     *
     * @param entity        the entity to search for fields with an {@link ElasticSearchEmbeddedField} annotations.
     * @param clazz         the class or a superclass of the specified entity.
     * @param mappings      the {@link java.util.Map} to add the field mappings to.
     * @param keyPrefixPath the prefix for the index field. This entity is an embedded entity and the index gets prefixed with the name of the parent field.
     */
    private static void addElasticSearchEmbeddedFields(@Nonnull Object entity, @Nonnull Class clazz, @Nonnull Map<String, Object> mappings, String keyPrefixPath) {
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(ElasticSearchEmbeddedField.class)) {
                field.setAccessible(true);

                Object embeddedEntity;
                try {
                    embeddedEntity = field.get(entity);
                    if (embeddedEntity == null) {
                        continue;
                    }
                } catch (IllegalAccessException e) {
                    throw new ElasticSearchIndexException(String.format("Failed to access field '%s' in entity '%s'.", field.getName(), entity.getClass().getSimpleName()));
                }

                String key = getFieldIndexName(field);
                if (keyPrefixPath != null) {
                    key = keyPrefixPath + key;
                }

                collectMappings(embeddedEntity, embeddedEntity.getClass(), mappings, key + KEY_PATH_SEPERATOR);
            }
        }
    }

    /**
     * Add entries to the specified map for all fields on the entity that are annotated with {@link ElasticSearchFieldBridge}.
     * The name of the annotation field will be the key in the map.
     * Which fields should be persisted is decided in the specified implementation in the {@link ElasticSearchFieldBridge} annotation.
     *
     * @param entity   the entity to search for fields with {@link ElasticSearchFieldBridge} annotations.
     * @param clazz    the class or a superclass of the specified entity.
     * @param mappings the Map to add the field mappings to.
     */
    private static void addElasticSearchBridgedFields(@Nonnull Object entity, @Nonnull Class clazz, @Nonnull Map<String, Object> mappings, String keyPrefixPath) {
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(ElasticSearchFieldBridge.class)) {
                field.setAccessible(true);
                ElasticSearchFieldBridge annotation = field.getAnnotation(ElasticSearchFieldBridge.class);

                Object bridgedField;
                try {
                    bridgedField = field.get(entity);
                    if (bridgedField == null) {
                        continue;
                    }
                } catch (IllegalAccessException e) {
                    throw new ElasticSearchIndexException(String.format("Failed to access field '%s' in entity '%s'.", field.getName(), entity.getClass().getSimpleName()));
                }

                String key = getFieldIndexName(field);
                if (keyPrefixPath != null) {
                    key = keyPrefixPath + key;
                }
                if (bridgedField instanceof Collection) {
                    Collection coll = (Collection) bridgedField;
                    for (Object obj : coll) {
                        try {
                            annotation.impl().newInstance().map(obj, mappings, key + KEY_PATH_SEPERATOR);
                        } catch (InstantiationException | IllegalAccessException | NoSuchFieldException e) {
                            throw new ElasticSearchIndexException(String.format("Failed construct field bridge mapping implementation '%s'.", annotation.impl().getSimpleName()));
                        }
                    }
                } else {
                    try {
                        annotation.impl().newInstance().map(bridgedField, mappings, key + KEY_PATH_SEPERATOR);
                    } catch (InstantiationException | IllegalAccessException | NoSuchFieldException e) {
                        throw new ElasticSearchIndexException(String.format("Failed construct field bridge mapping implementation '%s'.", annotation.impl().getSimpleName()));
                    }
                }
            }
        }
    }

    /**
     * Gets the index for the specified {@link java.lang.reflect.Field}.
     * The field is checked for annotations. If found and the annotation has a name() method which is not null or empty,
     * that value is used for this index.
     * Otherwise, the name of the field intself is used.
     *
     * @param field the {link @Field} to get the index name for.
     * @return the index name for the specified {@link java.lang.reflect.Field}
     */
    public static String getFieldIndexName(@Nonnull Field field) {
        String name = null;
        if (field.isAnnotationPresent(ElasticSearchField.class)) {
            name = field.getAnnotation(ElasticSearchField.class).name();
        } else if (field.isAnnotationPresent(ElasticSearchDocumentId.class)) {
            name = field.getAnnotation(ElasticSearchDocumentId.class).name();
        } else if (field.isAnnotationPresent(ElasticSearchEmbeddedField.class)) {
            name = field.getAnnotation(ElasticSearchEmbeddedField.class).name();
        } else if (field.isAnnotationPresent(ElasticSearchFieldBridge.class)) {
            name = field.getAnnotation(ElasticSearchFieldBridge.class).name();
        }

        if (name == null || name.isEmpty()) {
            name = field.getName();
        }
        return name;
    }
}
