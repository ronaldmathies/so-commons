package nl.sodeso.persistence.elasticsearch;

import nl.sodeso.persistence.elasticsearch.domain.IndexEntry;
import nl.sodeso.persistence.elasticsearch.domain.Person;
import nl.sodeso.persistence.elasticsearch.serialization.Deserializer;
import nl.sodeso.persistence.hibernate.UnitOfWork;
import nl.sodeso.persistence.hibernate.UnitOfWorkFactory;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Ronald Mathies
 */
public class ElasticSearchEntityWorkerTest {

    /**
     * The persistence unit name should be the same as in the so-persistence.properties configuration file
     * and the META-INF/persistence.xml configuration file.
     */
    private final static String PERSISTENCE_UNIT = "persistence-unit";

    private static UnitOfWork unitOfWork = null;

    private static Person person = null;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void before() {
        unitOfWork = UnitOfWorkFactory.startUnitOfWork(PERSISTENCE_UNIT);

        person = new Person();
        person.setFirstname("Ronald");
        person.setLastname("Mathies");
        person.addKeyword("developer");
        person.addKeyword("1978");
        person.addKeyword("Sodeso");

        unitOfWork.getSession().save(PERSISTENCE_UNIT, person);

        unitOfWork.commit();

        ElasticSearchIndexer.refreshIndex("elasticsearch", "index");
    }

    @Test
    public void count() {
        long count = ElasticSearchWorker.count(Person.class);
        assertEquals("Should be 1", 1, count);
    }

    @Test
    public void searchByField() {
        SearchHits hits = ElasticSearchWorker.searchByField(Person.class, "fullname", "ronald mathies", -1);
        assertEquals("Should be 1", 1, hits.totalHits());

        IndexEntry indexEntry = Deserializer.from(hits.getHits()[0], IndexEntry.class);
        assertNotNull(indexEntry);

        assertEquals("Should be 'Ronald Mathies'", "Ronald Mathies", indexEntry.getFullname());
    }

    @Test
    public void searchById() {
        SearchHit hit = ElasticSearchWorker.searchById(Person.class, String.valueOf(person.getId()));
        assertNotNull("Should not be null.", hit);
    }

    @Test
    public void searchByQuery() {
        SearchHits hits = ElasticSearchWorker.searchByQuery(Person.class, QueryBuilders.matchQuery("fullname", "ronald mathies"), null, -1);
        assertEquals("Should be 1", 1, hits.totalHits());

        IndexEntry indexEntry = Deserializer.from(hits.getHits()[0], IndexEntry.class);
        assertNotNull(indexEntry);

        assertEquals("Should be 'Ronald Mathies'", "Ronald Mathies", indexEntry.getFullname());

    }

}