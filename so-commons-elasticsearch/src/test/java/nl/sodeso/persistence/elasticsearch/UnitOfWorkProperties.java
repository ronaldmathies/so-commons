package nl.sodeso.persistence.elasticsearch;

import nl.sodeso.commons.properties.annotations.FileResource;
import nl.sodeso.commons.properties.annotations.FileResources;
import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.containers.file.FileContainer;

/**
 * @author Ronald Mathies
 */
@Resource(
        domain = "persistence-unit"
)
@FileResources(
        files = {
                @FileResource(file = "/persistence.properties")
        }
)
public class UnitOfWorkProperties extends FileContainer {
    
}
