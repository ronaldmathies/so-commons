package nl.sodeso.persistence.elasticsearch;

import nl.sodeso.commons.properties.annotations.FileResource;
import nl.sodeso.commons.properties.annotations.FileResources;
import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.containers.file.FileContainer;

/**
 * @author Ronald Mathies
 */
@Resource(
        domain = ElasticSearchProperties.DOMAIN
)
@FileResources(
        files = {
                @FileResource(file = "/elasticsearch.properties")
        }
)
public class ElasticSearchProperties extends FileContainer {

    public final static String DOMAIN = "elasticsearch";

}
