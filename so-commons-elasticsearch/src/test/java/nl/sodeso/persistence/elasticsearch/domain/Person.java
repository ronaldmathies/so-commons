package nl.sodeso.persistence.elasticsearch.domain;

import nl.sodeso.persistence.elasticsearch.annotation.*;
import nl.sodeso.persistence.elasticsearch.listener.ElasticSearchEventListener;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@Entity
@Table(name = "PERSON")
@EntityListeners({ElasticSearchEventListener.class})
@PersistenceEntity(persistenceUnit = "persistence-unit")
@ElasticSearchUnit
@ElasticSearchMapping(
        mapper = PersonElasticSearchMapper.class,
        mapsToClass = IndexEntry.class
)
@ElasticSearchIndex(index = "index")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @ElasticSearchDocumentId
    private Long id;

    @Column(name = "FIRSTNAME")
    @ElasticSearchField
    private String firstname;

    @Column(name = "LASTNAME")
    @ElasticSearchField
    private String lastname;

    @ElasticSearchField
    private transient List<String> keywords = new ArrayList<>();

    public Person() {}

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    public void addKeyword(String keyword) {
        this.keywords.add(keyword);
    }

    public List<String> getKeywords() {
        return this.keywords;
    }
}
