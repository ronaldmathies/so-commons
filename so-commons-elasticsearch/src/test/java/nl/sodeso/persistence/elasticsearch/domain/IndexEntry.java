package nl.sodeso.persistence.elasticsearch.domain;

import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchDocumentId;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchField;
import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
@ElasticSearchType(type = "IndexEntry")
public class IndexEntry {

    @ElasticSearchDocumentId
    private String id;

    @ElasticSearchField
    private String fullname;

    @ElasticSearchField
    private List<String> keywords = new ArrayList<>();

    public IndexEntry() {}

    public IndexEntry(String id, String firstname, String lastname) {
        this.id = id;
        this.fullname = firstname + " " + lastname;
    }

    public String getId() {
        return this.id;
    }

    public String getFullname() {
        return this.fullname;
    }

    public List<String> getKeywords() {
        return this.keywords;
    }

    public void addKeyword(String keyword) {
        this.keywords.add(keyword);
    }
}
