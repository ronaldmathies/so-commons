package nl.sodeso.persistence.elasticsearch.domain;

import nl.sodeso.persistence.elasticsearch.annotation.ElasticSearchMapper;

/**
 * @author Ronald Mathies
 */
public class PersonElasticSearchMapper implements ElasticSearchMapper<Person, IndexEntry> {

    @Override
    public IndexEntry map(Person person) {
        IndexEntry indexEntry = new IndexEntry(String.valueOf(person.getId()), person.getFirstname(), person.getLastname());

        for (String keyword : person.getKeywords()) {
            indexEntry.addKeyword(keyword);
        }

        return indexEntry;
    }

}
