package nl.sodeso.persistence.mock;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Mock for creating a dummy <code>OutputStream</code>, it does nothing except
 * for keeping track of the close() method being called or.
 *
 * @author Ronald Mathies
 */

public class OutputStreamMock extends OutputStream {

    private boolean isClosed = false;

    @Override
    public void write(int b) throws IOException {
    }

    @Override
    public void close() throws IOException {
        super.close();

        isClosed = true;
    }

    public boolean isClosed() {
        return isClosed;
    }
}