package nl.sodeso.persistence.mock;

import java.io.IOException;
import java.io.InputStream;

/**
 * Mock for creating a dummy <code>InputStream</code>, it does nothing except
 * for keeping track of the close() method being called or.
 *
 * @author Ronald Mathies
 */
public class InputStreamMock extends InputStream {

    private boolean isClosed = false;

    @Override
    public int read() throws IOException {
        return 0;
    }

    @Override
    public void close() throws IOException {
        super.close();

        isClosed = true;
    }

    public boolean isClosed() {
        return isClosed;
    }
}