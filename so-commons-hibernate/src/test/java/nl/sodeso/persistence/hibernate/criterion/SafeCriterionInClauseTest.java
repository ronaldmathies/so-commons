package nl.sodeso.persistence.hibernate.criterion;

import org.hibernate.criterion.Criterion;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public class SafeCriterionInClauseTest {

    @Test
    public void in() throws Exception {
        List<Long> ids = Arrays.asList(1l, 2l, 3l, 4l, 5l, 6l, 7l, 8l, 9l, 10l);
        Criterion criterion = SafeCriterionInClause.in("id", ids, 3);
        assertEquals("Generated SQL Query does not match.", "id in (1, 2, 3) or id in (4, 5, 6) or id in (7, 8, 9) or id in (10)", criterion.toString());
    }
}