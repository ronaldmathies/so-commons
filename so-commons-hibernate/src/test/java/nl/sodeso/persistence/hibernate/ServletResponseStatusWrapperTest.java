package nl.sodeso.persistence.hibernate;

import nl.sodeso.persistence.hibernate.filter.ServletResponseStatusWrapper;
import nl.sodeso.persistence.mock.HttpServletResponseMock;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Ronald Mathies
 */
public class ServletResponseStatusWrapperTest {

    @Test
    public void checkStatusNotSet() {
        ServletResponseStatusWrapper wrapper = new ServletResponseStatusWrapper(new HttpServletResponseMock());
        assertTrue(wrapper.isOkStatus());
    }

    @Test
    public void checkStatusSetTo399() {
        ServletResponseStatusWrapper wrapper = new ServletResponseStatusWrapper(new HttpServletResponseMock());
        wrapper.setStatus(399);
        assertTrue(wrapper.isOkStatus());
    }

    @Test
    public void checkStatusSetTo400() {
        ServletResponseStatusWrapper wrapper = new ServletResponseStatusWrapper(new HttpServletResponseMock());
        wrapper.setStatus(400);
        assertFalse(wrapper.isOkStatus());
    }

    @Test
    public void checkStatusSendError() throws IOException {
        ServletResponseStatusWrapper wrapper = new ServletResponseStatusWrapper(new HttpServletResponseMock());
        wrapper.sendError(100);
        assertFalse(wrapper.isOkStatus());
    }

}