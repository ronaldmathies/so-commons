package nl.sodeso.persistence.hibernate;

import nl.sodeso.persistence.hibernate.exception.PersistenceException;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class PersistenceExceptionTest {

    @Test
    public void checkEmptyConstructor() {
        PersistenceException persistenceException = new PersistenceException();
        assertNull("PersistenceException should not have a message.", persistenceException.getMessage());
        assertNull("PersistenceException should not have a cause.", persistenceException.getCause());
    }

    @Test
    public void checkMessageContstructor() {
        PersistenceException persistenceException = new PersistenceException("message");
        assertNotNull("PersistenceException should have a message.", persistenceException.getMessage());
        assertNull("PersistenceException should not have a cause.", persistenceException.getCause());
    }

    @Test
    public void checkMessageAndThrowableConstructor() {
        PersistenceException persistenceException = new PersistenceException("message", new Exception());
        assertNotNull("PersistenceException should not have a cause.", persistenceException.getMessage());
        assertNotNull("PersistenceException should have a cause.", persistenceException.getCause());
    }

    @Test
    public void checkThrowableConstuctor() {
        PersistenceException persistenceException = new PersistenceException(new Exception("Exception"));
        assertNotNull("PersistenceException should not have a message.", persistenceException.getMessage());
        assertNotNull("PersistenceException should have a cause.", persistenceException.getCause());
    }


}
