package nl.sodeso.persistence.hibernate;

import nl.sodeso.commons.properties.annotations.FileResource;
import nl.sodeso.commons.properties.annotations.FileResources;
import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.containers.file.FileContainer;

/**
 * @author Ronald Mathies
 */
@Resource(
        domain = UnitOfWorkProperties.PERSISTENCE_UNIT
)
@FileResources(
        files = {
                @FileResource(file = "/so-persistence.properties")
        }
)
public class UnitOfWorkProperties extends FileContainer {

    public final static String PERSISTENCE_UNIT = "persistence-unit";

}
