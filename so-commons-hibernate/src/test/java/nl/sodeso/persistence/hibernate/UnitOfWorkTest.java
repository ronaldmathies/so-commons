package nl.sodeso.persistence.hibernate;

import nl.sodeso.persistence.hibernate.exception.PersistenceException;
import nl.sodeso.persistence.hibernate.executors.Executor;
import org.hibernate.Session;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class UnitOfWorkTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    /**
     * The persistence unit name should be the same as in the so-persistence.properties configuration file
     * and the META-INF/persistence.xml configuration file.
     */
    private final static String PERSISTENCE_UNIT = "persistence-unit";

    private Session session = ThreadSafeSession.getSession(PERSISTENCE_UNIT);
    
    @Test
    public void checkTransactionFlow() {

        // An exception will be thrown when we start a transaction (since the
        // start also tries to see if there is not already a transaction)
        // which throws an exception since it doesn't exist.
        //thrown.expect(PersistenceException.class);

        // Create the unit of work.
        UnitOfWork unitOfWork = UnitOfWorkFactory.startUnitOfWork(PERSISTENCE_UNIT);

        // Create an entity which we will store.
        Person person = new Person();
        person.setFirstname("Ronald");
        person.setLastname("Mathies");

        // Save the entity.
        session.save(person);

        // Check that the auto-generated ID has been stored into the person entity.
        assertNotNull("ID of person should not be null.", person.getId());

        // Commit the transaction
        unitOfWork.commit();

        // Start a new transaction.
        unitOfWork = UnitOfWorkFactory.startUnitOfWork(PERSISTENCE_UNIT);

        // Load the person from the database and check if it is the same person.
        Person loadedPerson = session.get(Person.class, person.getId());
        assertEquals("ID of the loaded person should be the same as the saved person.", person.getId(), loadedPerson.getId());
        assertEquals("Firstname of the loaded person should be the same as the saved person.", "Ronald", loadedPerson.getFirstname());
        assertEquals("Lastname of the loaded person should be the same as the saved person.", "Mathies", loadedPerson.getLastname());

        // Remove the person from the database and commit the transaction.
        session.delete(loadedPerson);
        unitOfWork.commit();

        unitOfWork = UnitOfWorkFactory.startUnitOfWork(PERSISTENCE_UNIT);

        Person deletedPerson = session.get(Person.class, person.getId());
        assertNull("Maker sure that the person is removed from the database.", deletedPerson);

        // Rollback the transaction
        unitOfWork.rollback();
    }

    @Test
    public void checkDestroyed() {
        // An exception will be thrown when we start a transaction (since the
        // start also tries to see if there is not already a transaction)
        // which throws an exception since it doesn't exist.
        //thrown.expect(PersistenceException.class);

        // Create the unit of work.
        UnitOfWork unitOfWork = UnitOfWorkFactory.startUnitOfWork(PERSISTENCE_UNIT);

        // Rollback the transaction
        unitOfWork.rollback();

        boolean unitOfWorkDestroyed = false;
        try {
            unitOfWork.commit();
        } catch (PersistenceException e) {
            unitOfWorkDestroyed = true;
        }
        assertTrue("Unit of work should be destroyed.", unitOfWorkDestroyed);
    }

    @Test
    public void checkBeforeCommit() {
        // An exception will be thrown when we start a transaction (since the
        // start also tries to see if there is not already a transaction)
        // which throws an exception since it doesn't exist.
        //thrown.expect(PersistenceException.class);

        // Create the unit of work.
        UnitOfWork unitOfWork = UnitOfWorkFactory.startUnitOfWork(PERSISTENCE_UNIT);

        final boolean[] beforeCommit = {false};
        final boolean[] afterCommit = {false};

        unitOfWork.addExecutor(new Executor() {

            @Override
            public Phase[] isApplicableFor() {
                return new Phase[] {Phase.BeforeCommit};
            }

            @Override
            public void execute() {
                beforeCommit[0] = true;
            }

        });

        unitOfWork.addExecutor(new Executor() {

            @Override
            public Phase[] isApplicableFor() {
                return new Phase[] {Phase.AfterCommit};
            }

            @Override
            public void execute() {
                afterCommit[0] = true;
            }

        });

        unitOfWork.commit();

        assertTrue("Before commit should be executed.", beforeCommit[0]);
        assertTrue("After commit should be executed.", afterCommit[0]);

        assertTrue("Executors should be cleaned up.", unitOfWork.getExecutors().isEmpty());
    }

    @Test
    public void checkCleanupBeforeAndAfterCommit() {
        // An exception will be thrown when we start a transaction (since the
        // start also tries to see if there is not already a transaction)
        // which throws an exception since it doesn't exist.
        //thrown.expect(PersistenceException.class);

        // Create the unit of work.
        UnitOfWork unitOfWork = UnitOfWorkFactory.startUnitOfWork(PERSISTENCE_UNIT);

        final boolean[] beforeDestroy = {false};
        final boolean[] afterDestroy = {false};
        unitOfWork.addExecutor(new Executor() {

            @Override
            public Phase[] isApplicableFor() {
                return new Phase[] {Phase.BeforeClose};
            }

            @Override
            public void execute() {
                beforeDestroy[0] = true;
            }

        });

        unitOfWork.addExecutor(new Executor() {

            @Override
            public Phase[] isApplicableFor() {
                return new Phase[] {Phase.AfterClose};
            }

            @Override
            public void execute() {
                afterDestroy[0] = true;
            }

        });

        unitOfWork.commit();

        assertTrue("Executor should be executed after destroy.", beforeDestroy[0]);
        assertTrue("Executor should be executed after destroy.", afterDestroy[0]);

        assertTrue("Executors should be cleaned up.", unitOfWork.getExecutors().isEmpty());
    }
}