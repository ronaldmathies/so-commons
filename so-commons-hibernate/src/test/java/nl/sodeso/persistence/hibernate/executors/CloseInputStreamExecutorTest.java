package nl.sodeso.persistence.hibernate.executors;

import nl.sodeso.persistence.hibernate.UnitOfWork;
import nl.sodeso.persistence.hibernate.UnitOfWorkFactory;
import nl.sodeso.persistence.mock.InputStreamMock;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class CloseInputStreamExecutorTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private final static String PERSISTENCE_UNIT = "persistence-unit";

    private InputStreamMock inputStreamMock = new InputStreamMock();
    private CloseInputStreamExecutor cise = new CloseInputStreamExecutor(inputStreamMock);

    @Test
    public void checkPhase() throws Exception {
        assertEquals("Phase should be AfterCommit.", Executor.Phase.AfterCommit, cise.isApplicableFor()[0]);
    }

    @Test
    public void testExecute() throws Exception {
        // An exception will be thrown when we start a transaction (since the
        // start also tries to see if there is not already a transaction)
        // which throws an exception since it doesn't exist.
        //thrown.expect(PersistenceException.class);

        // Create the unit of work.
        UnitOfWork unitOfWork = UnitOfWorkFactory.startUnitOfWork(PERSISTENCE_UNIT);

        // Add the executor.
        unitOfWork.addExecutor(cise);

        // Perform a commit.
        unitOfWork.commit();

        // Check if the executor did his job.
        assertTrue("Stream should be closed.", inputStreamMock.isClosed());
    }


}