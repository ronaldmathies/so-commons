package nl.sodeso.persistence.hibernate.manager;

/**
 * @author Ronald Mathies
 */
public class TransactionReadOnlyResult<T> {

    private final T result;

    public TransactionReadOnlyResult(T result) {
        this.result = result;
    }

    public T getResult() {
        return this.result;
    }

}
