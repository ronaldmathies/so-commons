package nl.sodeso.persistence.hibernate.executors;

/**
 * Executors can be used to perform certain tasks in different phases of a transaction. Currently
 * there is support for the following phases:
 *
 * Before commit : Executed directly before a commit on a transaction.
 * After commit : Executed directly after a commit on a transaction.
 *
 * Before destroy : Executed directly before a session is destroyed.
 * After destroy : Executed directly after a session has been destroyed.
 *
 * There are two executes available for use by default:
 *
 * CloseInputStreamExecutor: Closes an input stream directly after a commit.
 * CloseOutputStreamExecutor: Closes an output stream directly after a commit.
 *
 * @author Ronald Mathies
 */
public interface Executor {

    enum Phase {
        BeforeCommit,
        AfterCommit,
        CommitFailed,
        BeforeClose,
        AfterClose,
        BeforeRollback,
        AfterRollback,
        RollbackFailed
    }

    /**
     * Should return the phase in which this executor will be executed.
     *
     * @return the phase.
     */
    Phase[] isApplicableFor();

    /**
     * Executes the executor.
     */
    void execute();

}
