package nl.sodeso.persistence.hibernate.util;

import java.util.UUID;

/**
 * Utility class for generating UUID values.
 *
 * @author Ronald Mathies
 */
public class UuidUtils {

    /**
     * Generates a random UUID.
     * @return the UUID.
     */
    public static String generate() {
        return UUID.randomUUID().toString();
    }

}
