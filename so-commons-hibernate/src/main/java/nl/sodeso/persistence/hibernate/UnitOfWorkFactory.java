package nl.sodeso.persistence.hibernate;

import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.persistence.hibernate.exception.PersistenceException;
import nl.sodeso.persistence.hibernate.executors.Executor;
import nl.sodeso.persistence.hibernate.util.UnitOfWorkUtil;
import org.hibernate.SessionFactory;
import org.hibernate.jpa.AvailableSettings;
import org.hibernate.jpa.internal.EntityManagerFactoryImpl;

import javax.persistence.Persistence;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class UnitOfWorkFactory {

    private static final Logger LOG = Logger.getLogger(UnitOfWorkFactory.class.getName());

    private static Map<String, SessionFactory> sessionFactories = new HashMap<>();
    private static ThreadLocal<Map<String, UnitOfWork>> unitOfWorkMap = new ThreadLocal<>();

    private static Map<String, UnitOfWork> getUnitOfWorkMap() {
        if (unitOfWorkMap.get() == null) {
            unitOfWorkMap.set(new HashMap<>());
        }

        return unitOfWorkMap.get();
    }

    public static UnitOfWork startUnitOfWork(String persistenceUnit) {
        if (!getUnitOfWorkMap().containsKey(persistenceUnit)) {
            UnitOfWork unitOfWork = new UnitOfWork(persistenceUnit, getSessionFactory(persistenceUnit).openSession());
            unitOfWork.addExecutor(new Executor() {
                @Override
                public Phase[] isApplicableFor() {
                    return new Phase[] {Phase.AfterClose};
                }

                @Override
                public void execute() {
                    getUnitOfWorkMap().remove(persistenceUnit);
                }
            });
            getUnitOfWorkMap().put(persistenceUnit, unitOfWork);
        }

        return getUnitOfWorkMap().get(persistenceUnit);
    }

    /**
     * Returns the current unit of work that exists for the specified persistence unit.
     * @param persistenceUnit the persistence unit.
     * @return the unit of work.
     */
    public static UnitOfWork currentUnitOfWork(String persistenceUnit) {
        UnitOfWork unitOfWork = getUnitOfWorkMap().get(persistenceUnit);
        if (unitOfWork == null) {
            throw new PersistenceException("startUnitOfWork hasn't been called for the persistence unit: " + persistenceUnit);
        }

        return unitOfWork;
    }

    /**
     * Returns the cached session factory that is associated with the persistence unit, if none exists it will
     * be created.
     *
     * @param persistenceUnit the persistence unit.
     * @return the session factory.
     */
    public static SessionFactory getSessionFactory(String persistenceUnit) {
        if (sessionFactories.containsKey(persistenceUnit)) {
            return sessionFactories.get(persistenceUnit);
        }

        String domain = PropertyConfiguration.getInstance().getPropertyContainerDomainBy("so.persistence.unit.name", persistenceUnit);
        if (domain == null || domain.isEmpty()) {
            throw new PersistenceException(String.format("No property domain found with so.persistence.unit.name=%s.", persistenceUnit));
        }

        Iterator<Map.Entry<Object, Object>> entries = PropertyConfiguration.getInstance().getEntries(domain);

        Properties properties = new Properties();
        while (entries.hasNext()) {
            Map.Entry<Object, Object> entry = entries.next();
            properties.put(entry.getKey(), entry.getValue());
        }

        // Check that the persistence unit does not already exist.
        List<Class> entities = UnitOfWorkUtil.getInstance().collectEntitiesForPersistenceUnit(persistenceUnit);
        if (!entities.isEmpty()) {
            StringBuilder classOverview = new StringBuilder(String.format("The following entities were found for persistence unit '%s':", persistenceUnit));
            for (Class _class : entities) {
                classOverview.append(String.format("\t\t%s\n\r", _class.getName()));
            }
            LOG.log(Level.INFO, classOverview.toString());

            properties.put(AvailableSettings.LOADED_CLASSES, entities);
        } else {
            LOG.log(Level.SEVERE, String.format("No entities found to load for persistence unit '%s'.", persistenceUnit));
        }

        EntityManagerFactoryImpl entityManagerFactory =
                (EntityManagerFactoryImpl) Persistence.createEntityManagerFactory(persistenceUnit, properties);

        sessionFactories.put(persistenceUnit, entityManagerFactory.getSessionFactory());
        return sessionFactories.get(persistenceUnit);
    }

    public static void destroy(String persistenceUnit) {
        getSessionFactory(persistenceUnit).close();
        sessionFactories.remove(persistenceUnit);
    }

}
