package nl.sodeso.persistence.hibernate.criterion;

import nl.sodeso.commons.collections.ListUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Convenience class for creating a Restriction.in clause wihtout exceeding the limit.
 * When the limit is exceeded it will automatically split the in-clause up in different segments
 * and concatenate them using a or clause.
 *
 * @author Ronald Mathies
 */
public class SafeCriterionInClause {

    /**
     * Creates an in-clause criteria based on the values in the list. Since
     * the in-clause is often limited to a number of entries this method allowes
     * you to split the list in blocks (limit) and creates several in-clauses.
     *
     * @param property the property to match.
     * @param values the list of values for the in-clause.
     * @param limit the number of entries within a single in-clause.
     *
     * @return the criterion object containing all the elements from the list of values.
     */
    public static Criterion in(@Nonnull String property, @Nonnull List values, int limit) {
        final Criterion[] criterion = {null};

        ListUtils.split(values, limit, sublist -> {
            if (criterion[0] == null) {
                criterion[0] = Restrictions.in(property, sublist);
            } else {
                criterion[0] = Restrictions.or(criterion[0], Restrictions.in(property, sublist));
            }
        });

        return criterion[0];
    }

}
