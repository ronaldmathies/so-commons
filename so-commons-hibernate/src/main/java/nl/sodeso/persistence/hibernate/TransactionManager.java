package nl.sodeso.persistence.hibernate;

import nl.sodeso.persistence.hibernate.manager.TransactionClosure;
import nl.sodeso.persistence.hibernate.manager.TransactionReadOnlyClosure;
import nl.sodeso.persistence.hibernate.manager.TransactionReadOnlyResult;
import nl.sodeso.persistence.hibernate.manager.TransactionResult;
import org.hibernate.CacheMode;
import org.hibernate.FlushMode;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class TransactionManager {

    private static final Logger LOG = Logger.getLogger(TransactionManager.class.getName());

    public static <T> T executeInTransaction(String persistenceName, TransactionClosure<T> closure) {
        boolean commitTransaction = false;
        UnitOfWork unitOfWork = UnitOfWorkFactory.startUnitOfWork(persistenceName);

        try {
            TransactionResult<T> result = closure.execute();
            commitTransaction = result.isCommit();
            return result.getResult();
        } catch (RuntimeException e) {
            LOG.log(Level.SEVERE, "Error while performing execution of the transaction closure.", e);
            throw e;
        } finally {
            if (commitTransaction) {
                unitOfWork.commit();
            } else {
                unitOfWork.rollback();
            }
        }
    }

    public static <T> T executeInReadOnlyTransaction(String persistenceName, TransactionReadOnlyClosure<T> closure) {
        UnitOfWork unitOfWork = UnitOfWorkFactory.startUnitOfWork(persistenceName);
        unitOfWork.getSession().setCacheMode(CacheMode.IGNORE);
        unitOfWork.getSession().setFlushMode(FlushMode.MANUAL);
        unitOfWork.getSession().setDefaultReadOnly(true);

        try {
            TransactionReadOnlyResult<T> result = closure.execute();
            return result.getResult();
        } catch (RuntimeException e) {
            LOG.log(Level.SEVERE, "Error while performing execution of the transaction closure.", e);
            throw e;
        } finally {
            unitOfWork.rollback();
        }
    }

}
