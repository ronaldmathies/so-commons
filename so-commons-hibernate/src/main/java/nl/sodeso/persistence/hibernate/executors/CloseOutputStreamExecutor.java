package nl.sodeso.persistence.hibernate.executors;

import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Closes an <code>OutputStream</code> directly after the transaction
 * has been committed.
 *
 * @author Ronald Mathies
 */
public class CloseOutputStreamExecutor implements Executor {

    private static final Logger LOG = Logger.getLogger(CloseOutputStreamExecutor.class.getName());

    private OutputStream outputStream = null;

    public CloseOutputStreamExecutor(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    /**
     * Returns phase in which this executor will be executed.
     *
     * @return the AfterCommit phase.
     */
    @Override
    public Phase[] isApplicableFor() {
        return new Phase[] {Phase.AfterCommit};
    }

    /**
     * Closes the output stream that has been passed in using object creation.
     */
    @Override
    public void execute() {
        if (outputStream != null) {
            try {
                outputStream.close();
            } catch (IOException e) {
                LOG.log(Level.SEVERE, "Failed to close outputstream.", e);
            }
        }
    }
}
