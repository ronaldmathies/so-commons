package nl.sodeso.persistence.hibernate.util;

/**
 * @author Ronald Mathies
 */
public interface HasUuid {

    String getUuid();

}
