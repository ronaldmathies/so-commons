package nl.sodeso.persistence.hibernate.manager;

/**
 * @author Ronald Mathies
 */
public interface TransactionReadOnlyClosure<T> {

    TransactionReadOnlyResult<T> execute();

}
