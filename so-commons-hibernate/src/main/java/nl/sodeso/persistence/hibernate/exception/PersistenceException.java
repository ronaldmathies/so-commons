package nl.sodeso.persistence.hibernate.exception;

/**
 * @author Ronald Mathies
 */
public class PersistenceException extends RuntimeException {

    public PersistenceException() {
    }

    public PersistenceException(String message) {
        super(message);
    }

    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }

    public PersistenceException(Throwable cause) {
        super(cause);
    }
}
