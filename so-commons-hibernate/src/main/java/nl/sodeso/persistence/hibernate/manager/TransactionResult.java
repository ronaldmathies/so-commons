package nl.sodeso.persistence.hibernate.manager;

/**
 * @author Ronald Mathies
 */
public class TransactionResult<T> {

    private final boolean commit;
    private final T result;

    public TransactionResult(boolean commit) {
        this(commit, null);
    }

    public TransactionResult(boolean commit, T result) {
        this.commit = commit;
        this.result = result;
    }

    public boolean isCommit() {
        return this.commit;
    }

    public T getResult() {
        return this.result;
    }

}
