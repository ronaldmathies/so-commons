package nl.sodeso.persistence.hibernate;

import org.hibernate.Session;
import org.hibernate.engine.spi.SessionImplementor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @author Ronald Mathies
 */
public class ThreadSafeSession {

    public static Session getSession(final String persistenceUnit) {
        InvocationHandler handler = (proxy, method, args) -> {
            Session session = UnitOfWorkFactory.currentUnitOfWork(persistenceUnit).getSession();
            return method.invoke(session, args);
        };

        return (Session) Proxy.newProxyInstance(Session.class.getClassLoader(), new Class<?>[]{Session.class, SessionImplementor.class}, handler);
    }

}
