package nl.sodeso.persistence.hibernate.executors;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Closes an <code>InputStream</code> directly after the transaction
 * has been committed.
 *
 * @author Ronald Mathies
 */

public class CloseInputStreamExecutor implements Executor {

    private static final Logger LOG = Logger.getLogger(CloseInputStreamExecutor.class.getName());

    private InputStream inputStream = null;

    public CloseInputStreamExecutor(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    /**
     * Returns phase in which this executor will be executed.
     *
     * @return the AfterCommit phase.
     */
    @Override
    public Phase[] isApplicableFor() {
        return new Phase[] {Phase.AfterCommit};
    }

    /**
     * Closes the input stream that has been passed in using object creation.
     */
    @Override
    public void execute() {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                LOG.log(Level.SEVERE, "Failed to close inputstream.", e);
            }
        }
    }
}
