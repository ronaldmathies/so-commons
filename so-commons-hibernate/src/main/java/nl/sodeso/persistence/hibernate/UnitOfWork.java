package nl.sodeso.persistence.hibernate;

import nl.sodeso.persistence.hibernate.exception.PersistenceException;
import nl.sodeso.persistence.hibernate.executors.Executor;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The UnitOfWork maintains the Hibernate session factories for every loaded
 * persistence unit.
 *
 * Besides loading the session factory it is also the entry point for starting and
 * stopping a transaction and committing and rolling back a transaction.
 *
 * For performing actual actions inside a transaction use the ThreadSafeSession.
 *
 * @author Ronald Mathies
 */
public class UnitOfWork {

    private static final Logger LOG = Logger.getLogger(UnitOfWork.class.getName());

    private Session session = null;
    private String persistenceUnitName = null;

    private List<Executor> executors = new ArrayList<>();

    protected UnitOfWork(String persistenceUnitName, Session session) {
        this.persistenceUnitName = persistenceUnitName;
        this.session = session;
        this.session.getTransaction().begin();
    }

    /**
     * Commits the transaction for the specified persistence unit, also executes all executors
     * that are in the BeforeCommit or AfterCommit phase.
     */
    public void commit() {
        Transaction transaction = getActiveTransaction();

        try {
            executeExecutors(Executor.Phase.BeforeCommit);

            transaction.commit();

            executeExecutors(Executor.Phase.AfterCommit);

            close();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "An error occurred while committing the transaction for persistence unit: " + persistenceUnitName, e);

            executeExecutors(Executor.Phase.CommitFailed);

            try {
                rollback();
            } catch (PersistenceException pe) {
                LOG.log(Level.SEVERE, "Commit failed and subsequent rollback also failed persistence unit: " + persistenceUnitName, pe);
            } finally {
                close();
            }

            throw new PersistenceException("An error occurred while committing the transaction for persistence unit: " + persistenceUnitName, e);
        }

    }

    /**
     * Rolls back the existing session for the persistence unit.
     */
    public void rollback() {
        Transaction transaction = getActiveTransaction();

        try {
            executeExecutors(Executor.Phase.BeforeRollback);

            transaction.rollback();

            executeExecutors(Executor.Phase.AfterRollback);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "An error occurred while performing the rollback for persistence unit: " + persistenceUnitName, e);

            executeExecutors(Executor.Phase.RollbackFailed);

            throw new PersistenceException("Failed to rollback the current transaction.");
        } finally {
            close();
        }
    }

    public Session getSession() {
        return this.session;
    }

    public boolean isTransactionActive() {
        if (session != null && session.isOpen()) {
            Transaction transaction = session.getTransaction();
            return transaction.getStatus().isOneOf(TransactionStatus.ACTIVE);
        }

        return false;
    }

    public Transaction getActiveTransaction() {
        if (isTransactionActive()) {
            return session.getTransaction();
        } else {
            throw new PersistenceException("No active transaction found within the session.");
        }
    }

    /**
     * Adds a new executor to the list of executors.
     * @param executor the executor to add.
     */
    public void addExecutor(Executor executor) {
        executors.add(executor);
    }

    public List<Executor> getExecutors() {
        return this.executors;
    }

    /**
     * Executes the executors in a certain phase.
     * @param phase the phase to execute.
     */
    private void executeExecutors(Executor.Phase phase) {
        for (Executor executor : executors) {
            Executor.Phase[] applicablePhases = executor.isApplicableFor();
            for (Executor.Phase applicablePhase : applicablePhases) {
                if (applicablePhase.equals(phase)) {
                    try {
                        executor.execute();
                    } catch (Exception e) {
                        LOG.log(Level.SEVERE, "An error occurred during the processing of the executor in the " + phase.name() + " for persistence unit: " + persistenceUnitName, e);
                    }
                }
            }
        }
    }

    /**
     * Closses the session of the persistence unit and also executes all the executors
     * in the BeforeClose or AfterClose phase.
     */
    private void close() {
        try {
            executeExecutors(Executor.Phase.BeforeClose);

            if (session != null && session.isOpen()) {
                session.close();
                session = null;

                executeExecutors(Executor.Phase.AfterClose);
                executors.clear();
            }


        } catch (Exception e) {
            LOG.log(Level.SEVERE, "An error occurred during the closing of the session for persistence unit: " + persistenceUnitName, e);
        }
    }

}
