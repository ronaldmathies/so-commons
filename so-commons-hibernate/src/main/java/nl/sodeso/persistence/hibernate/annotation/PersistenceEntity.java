package nl.sodeso.persistence.hibernate.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * The PersistenceEntity is used to associate the entity on which it is
 * registered with a persistence-unit within the UnitOfWork.
 *
 * The name of the persistence unit should be similair to the name as defined in the
 * persistence.properties.
 *
 * @author Ronald Mathies
 */
@Documented
@Target(TYPE)
@Retention(RUNTIME)
public @interface PersistenceEntity {

    /**
     * The persistence unit which will be responsible for handling the entity.
     * @return the persistence unit name.
     */
    String persistenceUnit();

}
