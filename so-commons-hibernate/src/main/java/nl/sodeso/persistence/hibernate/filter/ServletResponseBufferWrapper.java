package nl.sodeso.persistence.hibernate.filter;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Ronald Mathies
 */
public class ServletResponseBufferWrapper extends HttpServletResponseWrapper {

    private ByteArrayOutputStream output;

    public ServletResponseBufferWrapper(HttpServletResponse response) {
        super(response);

        output = new ByteArrayOutputStream();
    }

    public byte[] toByteArray() {
        return output.toByteArray();
    }

    public void closeBuffer() throws IOException {
        output.close();
    }

    public boolean isEmpty() {
        return output.size() == 0;
    }

    /**
     * This method cannot be used.
     *
     * @throws java.lang.RuntimeException
     */
    @Override
    public String toString() {
        throw new RuntimeException("Calling toString() from a filter or servlet is unsupported when using the ServletResponseBufferWrapper.");
    }

    /**
     * This method cannot be used.
     *
     * @throws java.lang.RuntimeException
     */
    @Override
    public PrintWriter getWriter() throws IOException {
        throw new RuntimeException("Calling getWriter() from a filter or servlet is unsupported when using the ServletResponseBufferWrapper.");
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {

        return new ServletOutputStream() {
            @Override
            public void write(int b) throws IOException {
                output.write(b);
            }

            @Override
            public boolean isReady() {
                return true;
            }

            @Override
            public void setWriteListener(WriteListener writeListener) {

            }
        };
    }


}
