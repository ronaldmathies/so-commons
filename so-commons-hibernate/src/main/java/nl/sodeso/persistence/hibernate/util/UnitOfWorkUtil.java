package nl.sodeso.persistence.hibernate.util;

import nl.sodeso.commons.web.classpath.ClasspathWebUtil;
import nl.sodeso.persistence.hibernate.annotation.PersistenceEntity;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.net.URL;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class UnitOfWorkUtil {

    private static final Logger LOG = Logger.getLogger(UnitOfWorkUtil.class.getName());

    private static UnitOfWorkUtil INSTANCE = null;

    private Map<String, List<Class>> entitiesForPersistenceUnit = new HashMap<>();

    /**
     * Returns an instance of the PropertyConfiguration class.
     * @return the instance.
     */
    public static UnitOfWorkUtil getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new UnitOfWorkUtil();
        }

        return INSTANCE;
    }

    private UnitOfWorkUtil() {
    }

    /**
     * Returns a list of classes that have the <code>PersistenceEntity</code> annotation
     * present tha matches with the required persistence unit.
     *
     * @param persistenceUnit the persistence unit
     *
     * @return a list of classes.
     */
    @SuppressWarnings("unchecked")
    public List<Class> collectEntitiesForPersistenceUnit(String persistenceUnit) {
        if (!entitiesForPersistenceUnit.containsKey(persistenceUnit)) {
            List<Class> entities = new ArrayList<>();
            Set<Class<?>> allEntities = ClasspathWebUtil.instance().typesAnnotatedWith(PersistenceEntity.class, "nl.sodeso");
            for (Class entity : allEntities) {
                PersistenceEntity annotation = (PersistenceEntity)entity.getAnnotation(PersistenceEntity.class);
                if (persistenceUnit.equals(annotation.persistenceUnit())) {
                    entities.add(entity);
                }
            }

            entitiesForPersistenceUnit.put(persistenceUnit, entities);
        }

        return entitiesForPersistenceUnit.get(persistenceUnit);
    }

}
