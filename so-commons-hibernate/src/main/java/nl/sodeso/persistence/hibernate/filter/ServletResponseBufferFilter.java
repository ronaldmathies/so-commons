package nl.sodeso.persistence.hibernate.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter which wraps the ServletResponse in a Wrapper
 * for the Servlet. The wrapper is given to the doFilter method
 * to ensure that the filters and servlets which are remaining in the
 * chain will not close the response's original output stream.
 * When the doFilter chain finishes this filter will then write the buffered
 * response to the original stream and close it. <br /><br/>
 *
 * The main reason to do this is to <b>ensure that the filters after this filter will
 * complete before the Servlet can close the original response stream</b> and thus
 * give control back to the client. This is especially the case for the {@code UnitOfWorkFilter}
 * where the commit() is called after the doFilter(). When the {@code ServletResponseBufferFilter} is
 * configured to run before the {@code UnitOfWorkFilter} the commit() is guaranteed to complete before
 * the Response stream to the client is closed.
 *
 * @author Ronald Mathies
 */
@WebFilter("/*")
public class ServletResponseBufferFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    /** {@inheritDoc} */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        ServletOutputStream out = null;
        ServletResponseBufferWrapper responseWrapper = null;

        try {
            // Wrap the output steam from the original response object
            out = response.getOutputStream();
            responseWrapper = new ServletResponseBufferWrapper((HttpServletResponse) response);

            // Give the chain the wrapper
            chain.doFilter(request, responseWrapper);

            // Write response bytes from the buffer to the original output stream.
            if (!responseWrapper.isEmpty()) {
                out.write(responseWrapper.toByteArray());
            }
        } finally {
            if (responseWrapper != null) {
                responseWrapper.closeBuffer();
            }

            if (out != null) {
                out.close();
            }
        }
    }

    public void destroy() {
    }
}
