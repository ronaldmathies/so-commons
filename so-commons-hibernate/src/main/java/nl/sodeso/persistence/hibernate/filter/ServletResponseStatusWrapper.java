package nl.sodeso.persistence.hibernate.filter;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

/**
 * @author Ronald Mathies
 */
public class ServletResponseStatusWrapper extends HttpServletResponseWrapper {

    private boolean isOkStatus = true;

    public ServletResponseStatusWrapper(HttpServletResponse response) {
        super(response);
    }

    @Override
    public void setStatus(int statusCode) {
        super.setStatus(statusCode);

        setOkStatus(statusCode);
    }

    private void setOkStatus(int statusCode) {
        this.isOkStatus = (statusCode - 400) < 0;
    }

    @Override
    public void sendError(int statusCode) throws IOException {
        super.sendError(statusCode);
        this.isOkStatus = false;
    }

    public boolean isOkStatus() {
        return isOkStatus;
    }

}
