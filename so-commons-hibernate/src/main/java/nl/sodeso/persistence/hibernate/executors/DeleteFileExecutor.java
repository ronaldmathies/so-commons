package nl.sodeso.persistence.hibernate.executors;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Deletes a <code>file</code> directly after the transaction
 * has been committed.
 *
 * @author Ronald Mathies
 */
public class DeleteFileExecutor implements Executor {

    private static final Logger LOG = Logger.getLogger(DeleteFileExecutor.class.getName());

    private File file = null;

    public DeleteFileExecutor(File file) {
        this.file = file;
    }

    /**
     * Returns phase in which this executor will be executed.
     *
     * @return the AfterCommit phase.
     */
    @Override
    public Phase[] isApplicableFor() {
        return new Phase[] {Phase.AfterCommit};
    }

    /**
     * Closes the output stream that has been passed in using object creation.
     */
    @Override
    public void execute() {
        if (file != null && file.exists()) {
            if (!file.delete()) {
                LOG.log(Level.WARNING, "Unable to delete file: '%s'", file);
            }
        }
    }
}
