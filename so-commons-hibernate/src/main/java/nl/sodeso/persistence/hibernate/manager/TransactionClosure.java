package nl.sodeso.persistence.hibernate.manager;

/**
 * @author Ronald Mathies
 */
public interface TransactionClosure<T> {

    TransactionResult<T> execute();

}
