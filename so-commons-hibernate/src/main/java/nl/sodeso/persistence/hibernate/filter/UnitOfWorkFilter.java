package nl.sodeso.persistence.hibernate.filter;

import nl.sodeso.persistence.hibernate.TransactionManager;
import nl.sodeso.persistence.hibernate.UnitOfWorkFactory;
import nl.sodeso.persistence.hibernate.exception.PersistenceException;
import nl.sodeso.persistence.hibernate.manager.TransactionResult;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is a web-filter which adds a transaction scope around a request, upon completion of
 * the request the transaction is either committed or rolling back a transaction based on the
 * success of a request (HTTP code), or when an exception has occurred.
 *
 * To register a UnitOfWorkFilter inside your web.xml you need to add the following:
 *
 * <filter>
 *      <filter-name>UnitOfWorkFilter</filter-name>
 *      <filter-class>nl.sodeso.persistence.hibernate.filter.UnitOfWorkFilter</filter-class>
 *      <init-param>
 *          <param-name>persistence-unit-name</param-name>
 *          <param-value>[name of the persistence unit]</param-value>
 *      </init-param>
 * </filter>
 * <filter-mapping>
 *      <filter-name>UnitOfWorkFilter</filter-name>
 *      <url-pattern>/*</url-pattern>
 * </filter-mapping>
 *
 * @author Ronald Mathies
 */
public class UnitOfWorkFilter implements Filter {

    private static final Logger LOG = Logger.getLogger(UnitOfWorkFilter.class.getName());

    public final static String INIT_PERSISTENCE_UNIT = "persistence-unit-name";
    public final static String INIT_EXTRA_URL_PATTERN = "extra-url-pattern";

    private String persistenceUnitName = null;
    private String extraUrlPattern = null;

    public void init(FilterConfig filterConfig) throws ServletException {
        persistenceUnitName = filterConfig.getInitParameter(INIT_PERSISTENCE_UNIT);
        extraUrlPattern = filterConfig.getInitParameter(INIT_EXTRA_URL_PATTERN);

        LOG.info("Persistence unit name according to init-param: " + persistenceUnitName);
        LOG.info("Extra url pattern according to init-param: " + (extraUrlPattern != null ? extraUrlPattern : "<none>"));

        LOG.info("Awakening persistence unit: " + persistenceUnitName + ".");
        UnitOfWorkFactory.startUnitOfWork(persistenceUnitName).rollback();
    }

    @SuppressWarnings("unchecked")
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        if (!isApplicableAccordingToExtraUrlPattern(request)) {
            chain.doFilter(request, response);
            return;
        }

        TransactionManager.executeInTransaction(persistenceUnitName, () -> {
            try {
                ServletResponseStatusWrapper wrapper = new ServletResponseStatusWrapper((HttpServletResponse) response);
                chain.doFilter(request, wrapper);

                return new TransactionResult(wrapper.isOkStatus());
            } catch (IOException | ServletException e) {
                throw new PersistenceException(e);
            }
        });

    }

    private boolean isApplicableAccordingToExtraUrlPattern(final ServletRequest request) {
        if (extraUrlPattern == null) {
            return true;
        }

        String requestUrl = ((HttpServletRequest)request).getRequestURL().toString();
        return requestUrl.matches(extraUrlPattern);
    }

    public void destroy() {
        UnitOfWorkFactory.destroy(persistenceUnitName);
        LOG.log(Level.INFO, "UnitOfWork filter with persistence name " + persistenceUnitName + " destroyed.");
    }
}
