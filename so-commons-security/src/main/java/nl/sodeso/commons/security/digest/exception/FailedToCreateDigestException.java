package nl.sodeso.commons.security.digest.exception;

/**
 * @author Ronald Mathies
 */
public class FailedToCreateDigestException extends Exception {

    public FailedToCreateDigestException(String message) {
        super(message);
    }

    public FailedToCreateDigestException(String message, Throwable cause) {
        super(message, cause);
    }
}
