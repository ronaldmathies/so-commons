package nl.sodeso.commons.security.digest;

import nl.sodeso.commons.security.digest.exception.FailedToCreateDigestException;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Ronald Mathies
 */
public class DigestUtil {

    private static int BUFFER_SIZE = 1024;

    public static String digest(final String value, final Algorithm algorithm) throws FailedToCreateDigestException {
        try {
            return digest(new ByteArrayInputStream(value.getBytes("UTF-8")), algorithm);
        } catch (IOException | NoSuchAlgorithmException e) {
            throw new FailedToCreateDigestException(
                    String.format("Failed to create digest from string."), e);
        }
    }

    public static String digest(final File file, final Algorithm algorithm) throws FailedToCreateDigestException {
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            return digest(fileInputStream, algorithm);
        } catch (IOException | NoSuchAlgorithmException e) {
            throw new FailedToCreateDigestException(
                    String.format("Failed to create digest from file '%s'.", file.toString()), e);
        }
    }

    public static String digest(final InputStream inputStream, final Algorithm algorithm) throws IOException, NoSuchAlgorithmException {
        final MessageDigest messageDigest = MessageDigest.getInstance(algorithm.getAlgorithm());

        byte[] buffer = new byte[BUFFER_SIZE];

        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            messageDigest.update(buffer, 0, length);
        }
        ;
        byte[] mdbytes = messageDigest.digest();

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < mdbytes.length; i++) {
            stringBuilder.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        return stringBuilder.toString();
    }

}
