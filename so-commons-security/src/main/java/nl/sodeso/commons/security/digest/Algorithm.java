package nl.sodeso.commons.security.digest;

/**
 * @author Ronald Mathies
 */
public enum Algorithm {

    SHA256("SHA-256");

    private String algorithm;

    Algorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getAlgorithm() {
        return this.algorithm;
    }

}
