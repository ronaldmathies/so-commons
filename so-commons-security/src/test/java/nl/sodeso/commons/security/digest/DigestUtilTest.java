package nl.sodeso.commons.security.digest;

import nl.sodeso.commons.security.digest.exception.FailedToCreateDigestException;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.File;
import nl.sodeso.commons.fileutils.FileHandler;
import nl.sodeso.commons.fileutils.FileFinder;

import static org.junit.Assert.*;

/**
 * @author Ronald Mathies
 */
public class DigestUtilTest {

    @Test
    public void testDigestFromString() throws Exception {
        String digest = DigestUtil.digest("This is a value for which a digest is created", Algorithm.SHA256);
        assertEquals("Digest does not match.", "2a948a84f74291f23db8b2e5a7918521d6e211dcb8e6547e95c806bbe630eb9e", digest);
    }

    @Test
    public void testDigestFromFile() throws Exception {
        FileFinder.findFileInUserPath("file.txt", new FileHandler() {

            public void handle(File file) {
                try {
                    String digest = DigestUtil.digest(file, Algorithm.SHA256);
                    assertEquals("Digest does not match.", "2a948a84f74291f23db8b2e5a7918521d6e211dcb8e6547e95c806bbe630eb9e", digest);
                } catch (FailedToCreateDigestException e) {
                    assertNull(e);
                }
            }

        });
    }

    @Test
    public void testDigestFromStream() throws Exception {
        ByteArrayInputStream bais = new ByteArrayInputStream("This is a value for which a digest is created".getBytes("UTF-8"));
        String digest = DigestUtil.digest(bais, Algorithm.SHA256);
        assertEquals("Digest does not match.", "2a948a84f74291f23db8b2e5a7918521d6e211dcb8e6547e95c806bbe630eb9e", digest);
    }
}