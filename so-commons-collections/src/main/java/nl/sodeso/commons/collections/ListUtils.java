package nl.sodeso.commons.collections;

import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ListUtils {

    /**
     * Splits the list into sub-lists and calls the consumer for each sub-list created.
     * @param values the list of values to split.
     * @param limit the number of items within a split sub-list.
     * @param consumer the consumer which will handle the sub-list.
     */
    @SuppressWarnings("unchecked")
    public static void split(List values, int limit, SubListConsumer consumer) {
        int totalSize = values.size();
        for (int index = 0; index < totalSize; index += limit) {

            List subList;
            if (totalSize > index + limit) {
                subList = values.subList(index, (index + limit));
            } else {
                subList = values.subList(index, totalSize);
            }

            consumer.handle(subList);
        }
    }

}
