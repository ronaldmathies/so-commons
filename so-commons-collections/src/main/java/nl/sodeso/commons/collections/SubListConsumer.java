package nl.sodeso.commons.collections;

import java.util.List;

/**
 * @author Ronald Mathies
 */
@FunctionalInterface
public interface SubListConsumer<E> {

    /**
     * Implement this method to handle sub-list.
     * @param list the sub-list.
     */
    void handle(List<E> list);

}
