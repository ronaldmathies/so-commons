package nl.sodeso.commons.collections;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Ronald Mathies
 */
public class ListUtilsTest {

    @Test
    public void testSplit() throws Exception {
        List<String> value = new ArrayList<>();
        value.addAll(Arrays.asList(
                "block 1-a", "block 1-b", "block 1-c",
                "block 2-a", "block 2-b", "block 2-c",
                "block 3-a", "block 3-b", "block 3-c"
        ));

        int block[] = {1};
        ListUtils.split(value, 3, list -> {
            assertEquals(3, list.size());
            assertTrue(((String)list.get(0)).indexOf(String.valueOf(block[0])) > 0);
            assertTrue(((String)list.get(1)).indexOf(String.valueOf(block[0])) > 0);
            assertTrue(((String)list.get(2)).indexOf(String.valueOf(block[0])) > 0);
            block[0]++;
        });
    }
}