package nl.sodeso.common.quartz;

import nl.sodeso.commons.quartz.annotation.QuartzJob;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @author Ronald Mathies
 */
@QuartzJob(
        name="SimpleJob",
        cron="0 0 0/1 1/1 * ? *"
)
@DisallowConcurrentExecution
public class SimpleJob implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

    }
}
