package nl.sodeso.common.quartz;

import nl.sodeso.commons.quartz.AbstractQuartzSchedulerContextListener;
import org.junit.Test;
import org.quartz.JobDataMap;

/**
 * @author Ronald Mathies
 */
public class AbstractQuartzSchedulerContextListenerTest {

    @Test
    public void init() {
        AbstractQuartzSchedulerContextListener quartzSchedulerServlet = new AbstractQuartzSchedulerContextListener() {

            @Override
            public JobDataMap getJobDataMap(String name) {
                return null;
            }

        };
        quartzSchedulerServlet.initialize(null);
    }

}
