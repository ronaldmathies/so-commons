package nl.sodeso.commons.quartz.jobs;

import nl.sodeso.commons.fileutils.FileFinder;
import nl.sodeso.commons.fileutils.FileHandler;
import org.quartz.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
@DisallowConcurrentExecution
public abstract class CleanupFilesJob implements Job {

    private static final Logger LOG = Logger.getLogger(CleanupFilesJob.class.getName());

    public static final String JOB_CONFIGURATION = "configuration";

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        LOG.log(Level.FINE, "Started CleanupFilesJob.");

        Statistics statistics = new Statistics();

        CleanupFilesConfig config = null;
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        if (!jobDataMap.containsKey(JOB_CONFIGURATION)) {
            config = getCleanupFilesConfig();
            jobDataMap.put(JOB_CONFIGURATION, config);
        }

        if (config != null && isFolderCorrect(config)) {
            performCleanup(config, statistics);
            LOG.log(Level.FINE, "Finished CleanupFilesJob, deleted %d out of %d files.", new Integer[] {statistics.deleted, statistics.total});
        } else {
            LOG.log(Level.SEVERE, "Failed to perform CleanupFilesJob, configuration not setup correctly.");
        }

    }

    public abstract CleanupFilesConfig getCleanupFilesConfig();

    /**
     * Checks if the folder to clean actually exists and is a folder.
     *
     * @param config the configuration containing the folder.
     * @return flag indicating if the folder is correct or not.
     */
    private boolean isFolderCorrect(CleanupFilesConfig config) {
        File folder = new File(config.getFolder());
        if (folder.exists()) {

            if (folder.isDirectory()) {
                return true;
            } else {
                LOG.log(Level.SEVERE, "The specified location '%s' is not a folder.", config.getFolder());
            }

        } else {
            LOG.log(Level.SEVERE, "The specified location '%s' does not exist.", config.getFolder());
        }

        return false;
    }

    /**
     * Performs the actual cleanup of the configured folder.
     *
     * @param config the configuration for cleaning the files.
     * @param statistics statistics object for keeping track how many files were encountered and how many were deleted.
     */
    private void performCleanup(final CleanupFilesConfig config, final Statistics statistics) {
        final long current = System.currentTimeMillis();

        FileFinder.findFilesMatchingExpression(config.getFolder(), config.getExpression(), file -> {
            statistics.total++;

            try {
                BasicFileAttributes attributes = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
                FileTime fileTime = attributes.creationTime();
                long diff = current - fileTime.toMillis();
                if (diff < config.getExpirationSinceCreated()) {
                    if (delete(file)) {
                        statistics.deleted++;
                    }
                    return;
                }

                fileTime = attributes.lastModifiedTime();
                diff = current - fileTime.toMillis();
                if (diff < config.getExpirationSinceModified()) {
                    if (delete(file)) {
                        statistics.deleted++;
                    }
                    return;
                }

                fileTime = attributes.lastAccessTime();
                diff = current - fileTime.toMillis();
                if (diff < config.getExpirationSinceLastAccess()) {
                    if (delete(file)) {
                        statistics.deleted++;
                    }
                }

            } catch (IOException ignored) {
                // Ignore...
            }
        });
    }

    private boolean delete(File fileToDelete) {
        return fileToDelete.isFile() && fileToDelete.delete();
    }

    private class Statistics {

        public int total;
        public int deleted;

    }

}
