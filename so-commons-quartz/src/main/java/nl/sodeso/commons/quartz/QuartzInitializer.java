package nl.sodeso.commons.quartz;

import nl.sodeso.commons.quartz.annotation.QuartzJob;
import nl.sodeso.commons.web.classpath.ClasspathWebUtil;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import javax.annotation.Nonnull;
import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * @author Ronald Mathies
 */
public abstract class QuartzInitializer {

    private static final Logger LOG = Logger.getLogger(QuartzInitializer.class.getName());

    private Scheduler scheduler;

    /**
     * {@inheritDoc}
     */
    public void initialize() {
        try {
            LOG.log(Level.INFO, "Starting Quartz Scheduler Servlet.");
            StdSchedulerFactory stdSchedulerFactory = new StdSchedulerFactory();
            scheduler = stdSchedulerFactory.getScheduler();

            QuartzDatabaseChecker.createOrReplaceTables(scheduler);
            scheduler.start();

            initJobs();
        } catch (SchedulerException e) {
            LOG.log(Level.SEVERE, "Failed to start job.", e);
        }
    }

    /**
     * Resolves all jobs with the <code>QuartzJob</code> annotation and registers (or replaces) them in the Quartz scheduler.
     */
    private void initJobs() {
        LOG.log(Level.INFO, "Initializing Quartz Jobs, starting search for jobs.");
        Set<Class<? extends Job>> jobs = ClasspathWebUtil.instance().subTypesOf(Job.class, "nl.sodeso");
        LOG.log(Level.INFO, "Found '" + jobs.size() + "' jobs.");

        try {
            for (Class<? extends Job> job : jobs) {
                if (!Modifier.isAbstract(job.getModifiers()) && !Modifier.isInterface(job.getModifiers())) {
                    QuartzJob quartzJob = job.getAnnotation(QuartzJob.class);
                    if (quartzJob != null) {
                        LOG.log(Level.INFO, String.format("Add or replacing job '%s'.", quartzJob.name()));
                        addOrReplaceJob(quartzJob.name(), quartzJob.cron(), job);
                    } else {
                        LOG.log(Level.INFO, String.format("Quartz job '%s' found, however, the job did not specify a QuartzJob annotation so this job will be ignored.", job.getSimpleName()));
                    }
                } else {
                    LOG.log(Level.INFO, String.format("Quartz job '%s' found, however, the job is either an interface or is an abstract class so this job will be ignored.", job.getSimpleName()));
                }
            }
        } catch (SchedulerException e) {
            LOG.log(Level.SEVERE, "Failed to create or replace job.", e);
        }
    }

    /**
     * Adds or replaces an existing job.
     *
     * @param jobName the name of the job.
     * @param cronExpression the cron expression.
     * @param jobClass the job class.
     *
     * @throws SchedulerException thrown when an error occurred during the process of creating or replacing a job.
     */
    private void addOrReplaceJob(String jobName, String cronExpression, Class<? extends Job> jobClass) throws SchedulerException {
        boolean exists = false, outdated = false;

        List<? extends Trigger> triggersOfJob = scheduler.getTriggersOfJob(new JobKey(jobName));
        for (Trigger trigger : triggersOfJob) {
            exists = true;

            if (trigger instanceof CronTrigger) {
                CronTrigger cronTrigger = (CronTrigger) trigger;
                String cronExpr = cronTrigger.getCronExpression();

                if (!cronExpr.equals(cronExpression)) {
                    outdated = true;
                    break;
                }
            }
        }

        JobBuilder jobBuilder = newJob(jobClass);

        // Check if there is any JobData
        JobDataMap jobDataMap = getJobDataMap(jobName);
        if (jobDataMap != null && !jobDataMap.isEmpty()) {
            jobBuilder.usingJobData(jobDataMap);
        }
        JobDetail jobDetail = jobBuilder.withIdentity(new JobKey(jobName)).build();

        if (!exists) {
            scheduler.scheduleJob(jobDetail, newTrigger().forJob(jobDetail).withSchedule(
                    CronScheduleBuilder.cronSchedule(cronExpression)).withIdentity(jobName + "_TRIGGER").build());

            LOG.info("Added new job '" + jobName + "'.");
        } else if (outdated) {
            Date firstTriggerDate = scheduler.rescheduleJob(new TriggerKey(jobName + "_TRIGGER"), newTrigger().forJob(jobDetail).withSchedule(
                    CronScheduleBuilder.cronSchedule(cronExpression)).withIdentity(jobName + "_TRIGGER").build());
            if (firstTriggerDate == null) {
                throw new SchedulerException(String.format("Error replacing existing job with key %s_TRIGGER", jobName));
            }

            LOG.info(String.format("Replaced existing job '%s' due to cron expression change.", jobName));
        }

    }

    /**
     * This method should be implemented to return the job data map that is used by the job with
     * the specified name. If there is no job data then simply return <code>null</code> or an empty map.
     *
     * @param name the name of the job for which the job data is requested.
     * @return A map containing job data or an empty map or <code>null</code> when there is no job data.
     */
    public abstract JobDataMap getJobDataMap(String name);

    /**
     * {@inheritDoc}
     */
    public void destroy() {
        if (scheduler != null) {
            try {
                scheduler.shutdown();
            } catch (SchedulerException e) {
                LOG.log(Level.SEVERE, "Error shutting down Quartz scheduler.", e);
            }
        }
    }

}
