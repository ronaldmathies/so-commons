package nl.sodeso.commons.quartz;

import nl.sodeso.commons.web.initialization.AbstractWebInitContext;
import nl.sodeso.commons.web.initialization.annotation.WebInitContext;
import org.quartz.*;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Abstract implementation of a servlet context initialization for initializing a quartz
 * engine.
 *
 * @author Ronald Mathies
 */
@WebInitContext(priority = 0)
public abstract class AbstractQuartzSchedulerContextListener extends AbstractWebInitContext {

    private static final Logger LOG = Logger.getLogger(AbstractQuartzSchedulerContextListener.class.getName());

    private QuartzInitializer quartzInitializer = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(ServletContextEvent event) {
        LOG.log(Level.INFO, "Initializing Quartz Scheduler Context Listener.");

        quartzInitializer = new QuartzInitializer() {
            @Override
            public JobDataMap getJobDataMap(String name) {
                return AbstractQuartzSchedulerContextListener.this.getJobDataMap(name);
            }
        };

        quartzInitializer.initialize();

        LOG.log(Level.INFO, "Quartz Scheduler Context Listener initialized.");

    }

    /**
     * Should return the job data that is applicable for the job with the specified name.
     *
     * Please note that the JOB data should be serializable, otherwise it will not be stored
     * in the database.
     *
     * @param name the name of the job.
     * @return the job data that will be used for the job.
     */
    public abstract JobDataMap getJobDataMap(String name);

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy(ServletContextEvent event) {
        LOG.log(Level.INFO, "Destroying Quartz Scheduler Context Listener.");

        quartzInitializer.destroy();

        LOG.log(Level.INFO, "Quartz Scheduler Context Listener Destroyed.");

    }

}
