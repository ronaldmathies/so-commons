package nl.sodeso.commons.quartz.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Can be used on a quartz job to specify the name of the quartz job
 * and the cron expression of the job.
 *
 * @author Ronald Mathies
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface QuartzJob {

    /**
     * Name of the quartz job.
     * @return the name of the quartz job.
     */
    String name();
    String cron();
}
