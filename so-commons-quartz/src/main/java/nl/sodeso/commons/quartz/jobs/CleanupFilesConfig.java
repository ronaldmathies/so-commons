package nl.sodeso.commons.quartz.jobs;

import java.io.Serializable;

/**
 * @author Ronald Mathies
 */
public class CleanupFilesConfig implements Serializable {

    private Long expirationSinceCreated;
    private Long expirationSinceModified;
    private Long expirationSinceLastAccess;

    private String folder;
    private String expression;

    public CleanupFilesConfig() {}

    public Long getExpirationSinceCreated() {
        return expirationSinceCreated;
    }

    public void setExpirationSinceCreated(Long expirationSinceCreated) {
        this.expirationSinceCreated = expirationSinceCreated;
    }

    public Long getExpirationSinceModified() {
        return expirationSinceModified;
    }

    public void setExpirationSinceModified(Long expirationSinceModified) {
        this.expirationSinceModified = expirationSinceModified;
    }

    public Long getExpirationSinceLastAccess() {
        return expirationSinceLastAccess;
    }

    public void setExpirationSinceLastAccess(Long expirationSinceLastAccess) {
        this.expirationSinceLastAccess = expirationSinceLastAccess;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }
}
