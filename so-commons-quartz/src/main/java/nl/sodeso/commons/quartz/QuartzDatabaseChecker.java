package nl.sodeso.commons.quartz;

import nl.sodeso.persistence.jdbc.Provider;
import nl.sodeso.persistence.jdbc.SqlScriptExecutor;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.utils.DBConnectionManager;

import javax.annotation.Nonnull;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Checks if the quartz tables exist within the database that is configured for the specified
 * scheduler. If the tables don't exist then it will create the tables, otherwise it will
 * check the version of quartz that was used to create the tables, when it is out-dated it will
 * automatically re-create the tables based on the new version of quartz.
 *
 * @author Ronald Mathies
 */
public class QuartzDatabaseChecker {

    private static final Logger LOG = Logger.getLogger(QuartzDatabaseChecker.class.getName());

    private static final String QRTZ_VERSION_QUERY = "SELECT VERSION FROM QRTZ_VERSION";

    /**
     * Creates / replaces the Quartz tables when the Quartz Scheduler supports
     * persistence storage of jobs..
     *
     * @throws SchedulerException
     */
    public static void createOrReplaceTables(@Nonnull Scheduler scheduler) throws SchedulerException {
        if (!scheduler.getMetaData().isJobStoreSupportsPersistence()) {
            LOG.log(Level.INFO, "Quartz Scheduler does not support persistence storage of jobs, skipping datavbase checks.");
            return;
        }

        try {
            LOG.log(Level.INFO, "Starting process for creating Quartz tables.");

            Connection connection = DBConnectionManager.getInstance().getConnection("datasource");
            if (!checkQuartzTablesUpToDate(scheduler, connection)) {
                LOG.log(Level.INFO, "Executing sqlScript for creating tables.");

                InputStream inputStream = null;
                Provider provider = Provider.getProvider(connection.getMetaData().getDatabaseProductName());
                if (Provider.H2.equals(provider)) {
                    inputStream = AbstractQuartzSchedulerContextListener.class.getResourceAsStream("/scripts/tables-h2.sql");
                } else if (Provider.MySQL.equals(provider)) {
                    inputStream = AbstractQuartzSchedulerContextListener.class.getResourceAsStream("/scripts/tables-mysql.sql");
                } else if (Provider.DB2_AIX64.equals(provider)) {
                    inputStream = AbstractQuartzSchedulerContextListener.class.getResourceAsStream("/scripts/tables-db2-v95.sql");
                }

                if (inputStream != null) {
                    SqlScriptExecutor.execute(connection, inputStream);
                } else {
                    LOG.log(Level.SEVERE, "Failed to create tables, no script found for provider '%s'.", provider != null ? provider.getProvider() : "<unknown>");
                }
            } else {
                LOG.log(Level.INFO, "Tables already exist, skipping this step.");
            }
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, "Problem while getting connection to datasource in use by Quartz.");
        }
    }

    private static boolean checkQuartzTablesUpToDate(@Nonnull Scheduler scheduler, @Nonnull Connection connection) throws SQLException, SchedulerException {
        LOG.log(Level.INFO, "Checking if Quartz tables are present.");
        ResultSet tables = connection.getMetaData().getTables(null, null, "QRTZ_VERSION", null);
        if (tables.next()) {
            LOG.log(Level.INFO, "Quartz tables are present, checking version.");

            ResultSet versionRs = connection.createStatement()
                    .executeQuery(QRTZ_VERSION_QUERY);

            if (versionRs.next()) {
                String version = versionRs.getString("VERSION");
                if (!scheduler.getMetaData().getVersion().equals(version)) {
                    LOG.log(Level.INFO, "Quartz tables are not up-to-date.");
                    return false;
                }

                LOG.log(Level.INFO, "Quartz tables are up-to-date.");
                return true;
            } else {
                LOG.log(Level.INFO, "Quartz version not present in QRTZ_VERSION table.");
                return false;
            }
        } else {
            LOG.log(Level.INFO, "Quartz tables not present.");

            return false;
        }
    }

}
