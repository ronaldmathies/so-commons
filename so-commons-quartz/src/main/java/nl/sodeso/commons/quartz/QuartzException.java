package nl.sodeso.commons.quartz;

/**
 * TODO: Unnecessary class?
 *
 * @author Ronald Mathies
 */
public class QuartzException extends Exception {

    public QuartzException() {
    }

    public QuartzException(String message) {
        super(message);
    }

    public QuartzException(String message, Throwable cause) {
        super(message, cause);
    }

    public QuartzException(Throwable cause) {
        super(cause);
    }

    public QuartzException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
