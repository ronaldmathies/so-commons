DROP TABLE QRTZ_FIRED_TRIGGERS;
DROP TABLE QRTZ_PAUSED_TRIGGER_GRPS;
DROP TABLE QRTZ_SCHEDULER_STATE;
DROP TABLE QRTZ_LOCKS;
DROP TABLE QRTZ_SIMPLE_TRIGGERS;
DROP TABLE QRTZ_SIMPROP_TRIGGERS;
DROP TABLE QRTZ_CRON_TRIGGERS;
DROP TABLE QRTZ_TRIGGERS;
DROP TABLE QRTZ_JOB_DETAILS;
DROP TABLE QRTZ_CALENDARS;
DROP TABLE QRTZ_BLOB_TRIGGERS;
DROP TABLE QRTZ_VERSION;

CREATE TABLE QRTZ_VERSION (
  VERSION varchar(32) NOT NULL
);

CREATE TABLE qrtz_job_details (
  sched_name varchar(120) NOT NULL,
  job_name varchar(80) NOT NULL,
  job_group varchar(80) NOT NULL,
  description varchar(120),
  job_class_name varchar(128) NOT NULL,
  is_durable integer NOT NULL,
  is_nonconcurrent integer NOT NULL,
  is_update_data integer NOT NULL,
  requests_recovery integer NOT NULL,
  job_data blob(2000),
  PRIMARY KEY (sched_name, job_name, job_group)
);

CREATE TABLE qrtz_triggers (
  sched_name varchar(120) NOT NULL,
  trigger_name varchar(80) NOT NULL,
  trigger_group varchar(80) NOT NULL,
  job_name varchar(80) NOT NULL,
  job_group varchar(80) NOT NULL,
  description varchar(120),
  next_fire_time bigint,
  prev_fire_time bigint,
  priority integer,
  trigger_state varchar(16) NOT NULL,
  trigger_type varchar(8) NOT NULL,
  start_time bigint NOT NULL,
  end_time bigint,
  calendar_name varchar(80),
  misfire_instr smallint,
  job_data blob(2000),
  PRIMARY KEY (sched_name, trigger_name, trigger_group),
  FOREIGN KEY (sched_name, job_name, job_group) REFERENCES qrtz_job_details (sched_name, job_name, job_group)
);

CREATE TABLE qrtz_simple_triggers (
  sched_name varchar(120) NOT NULL,
  trigger_name varchar(80) NOT NULL,
  trigger_group varchar(80) NOT NULL,
  repeat_count bigint NOT NULL,
  repeat_interval bigint NOT NULL,
  times_triggered bigint NOT NULL,
  PRIMARY KEY (sched_name, trigger_name, trigger_group),
  FOREIGN KEY (sched_name, trigger_name, trigger_group) REFERENCES qrtz_triggers (sched_name, trigger_name, trigger_group)
);

CREATE TABLE qrtz_cron_triggers (
  sched_name varchar(120) NOT NULL,
  trigger_name varchar(80) NOT NULL,
  trigger_group varchar(80) NOT NULL,
  cron_expression varchar(120) NOT NULL,
  time_zone_id varchar(80),
  PRIMARY KEY (sched_name, trigger_name, trigger_group),
  FOREIGN KEY (sched_name, trigger_name, trigger_group) REFERENCES qrtz_triggers (sched_name, trigger_name, trigger_group)
);

CREATE TABLE qrtz_simprop_triggers (
  sched_name varchar(120) NOT NULL,
  TRIGGER_NAME varchar(200) NOT NULL,
  TRIGGER_GROUP varchar(200) NOT NULL,
  STR_PROP_1 varchar(512),
  STR_PROP_2 varchar(512),
  STR_PROP_3 varchar(512),
  INT_PROP_1 int,
  INT_PROP_2 int,
  LONG_PROP_1 bigint,
  LONG_PROP_2 bigint,
  DEC_PROP_1 numeric(13, 4),
  DEC_PROP_2 numeric(13, 4),
  BOOL_PROP_1 varchar(1),
  BOOL_PROP_2 varchar(1),
  PRIMARY KEY (sched_name, TRIGGER_NAME, TRIGGER_GROUP),
  FOREIGN KEY (sched_name, TRIGGER_NAME, TRIGGER_GROUP)
  REFERENCES QRTZ_TRIGGERS (sched_name, TRIGGER_NAME, TRIGGER_GROUP)
);

CREATE TABLE qrtz_blob_triggers (
  sched_name varchar(120) NOT NULL,
  trigger_name varchar(80) NOT NULL,
  trigger_group varchar(80) NOT NULL,
  blob_data blob(2000),
  PRIMARY KEY (sched_name, trigger_name, trigger_group),
  FOREIGN KEY (sched_name, trigger_name, trigger_group) REFERENCES qrtz_triggers (sched_name, trigger_name, trigger_group)
);

CREATE TABLE qrtz_calendars (
  sched_name varchar(120) NOT NULL,
  calendar_name varchar(80) NOT NULL,
  calendar blob(2000) NOT NULL,
  PRIMARY KEY (calendar_name)
);

CREATE TABLE qrtz_fired_triggers (
  sched_name varchar(120) NOT NULL,
  entry_id varchar(95) NOT NULL,
  trigger_name varchar(80) NOT NULL,
  trigger_group varchar(80) NOT NULL,
  instance_name varchar(80) NOT NULL,
  fired_time bigint NOT NULL,
  sched_time bigint NOT NULL,
  priority integer NOT NULL,
  state varchar(16) NOT NULL,
  job_name varchar(80),
  job_group varchar(80),
  is_nonconcurrent integer,
  requests_recovery integer,
  PRIMARY KEY (sched_name, entry_id)
);

CREATE TABLE qrtz_paused_trigger_grps (
  sched_name varchar(120) NOT NULL,
  trigger_group varchar(80) NOT NULL,
  PRIMARY KEY (sched_name, trigger_group)
);

CREATE TABLE qrtz_scheduler_state (
  sched_name varchar(120) NOT NULL,
  instance_name varchar(80) NOT NULL,
  last_checkin_time bigint NOT NULL,
  checkin_interval bigint NOT NULL,
  PRIMARY KEY (sched_name, instance_name)
);

CREATE TABLE qrtz_locks (
  sched_name varchar(120) NOT NULL,
  lock_name varchar(40) NOT NULL,
  PRIMARY KEY (sched_name, lock_name)
);

INSERT INTO QRTZ_VERSION (VERSION)
  VALUES ('2.2.1');

COMMIT;